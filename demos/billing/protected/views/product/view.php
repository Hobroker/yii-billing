<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs = array(
    'Products' => array('index'),
    $model->nameprod,
);
?>
<div class="container-fluid">
    <div class="row">
        <h2><?php echo $model->idcateg0->namecateg . " " . $model->nameprod; ?></h2>
    </div>
    <div class="row">
        <h4><?= "<b>Manufacturer:</b> " . $model->idmanufacturer0->name ?></h4>
    </div>
    <div class="row">
        <h4><b>Price:</b>
            <?php if (Product::productSale($model->idprod) != 0): ?>
                <s class="text-danger"><?php echo $model->priceprod . Product::$currency; ?></s>
                <?php echo Product::productPriceWithSale($model->idprod) . Product::$currency; ?>
            <?php else: ?>
                <?php echo $model->priceprod . Product::$currency; ?>
            <?php endif; ?>
        </h4>
    </div>
    <?php if (Product::productSale($model->idprod) != 0): ?>
        <div class="row">
            <h4><b>Sale until:</b> <?= IHtml::idate(Product::productSaleUntil($model->idprod)) ?></h4>
        </div>
    <?php endif; ?>
</div>