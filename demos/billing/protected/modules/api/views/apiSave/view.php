<?php
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 5/4/16
 * Time: 10:27 AM
 */
$this->menu = array(
    array('label' => 'Index', 'icon' => 'list', 'url' => array("Api/apiSave/index")),
    array('label' => 'Create', 'icon' => 'plus', 'url' => array("Api/apiSave/create")),
    array('label' => 'Update', 'icon' => 'edit', 'url' => array("Api/apiSave/update", "id" => $model->id)),
);
?>
<?php
$jsn = json_encode(json_decode($model->params), JSON_PRETTY_PRINT);
if (strtolower($model->action) == 'putfile')
    $jsn = '0=' . $jsn;
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'module',
        'model',
        'action',
        'url',
        [
            'name' => 'params',
            'value' => "<pre>$jsn</pre>",
            'type' => 'raw'
        ]
    ),
)); ?>