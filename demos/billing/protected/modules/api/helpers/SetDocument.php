<?php

/**
 * Created by PhpStorm.
 * User: urechean
 * Date: 27.05.2015
 * Time: 09:03
 */
class SetDocument
{

    public static function uploadFile($model_name)
    {
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {
                        $model = array();
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        $document_id = null;
                        if (isset($model["use_as"]) && $model["use_as"] != "") {
                            switch ($model["use_as"]) {
                                case "file":
                                    $filePath = Storage::model()->findByPk(Storage::API_STORAGE_ID)->path . time() . '_' . rand() . '.' . pathinfo("temp." . $model["format"], PATHINFO_EXTENSION);
                                    $ifp = fopen($filePath, "wb");
                                    fwrite($ifp, base64_decode($model["image"]));
                                    fclose($ifp);

                                    if (is_file($filePath)) {
                                        try {
                                            $document_id = Document::create($model_name, $filePath);
                                            $arr = SetDocument::saveDocument($filePath, $document_id, $model_name, $model);
                                        } catch (Exception $ex) {
                                            Response::sendResponse(500,
                                                ObjectEncoded::getObjectEncoded(
                                                    'Document',
                                                    array(
                                                        "status" => "ERROR",
                                                        "message" => $ex->getMessage()
                                                    )
                                                )
                                            );
                                        }

                                    } else
                                        Response::sendResponse(500,
                                            ObjectEncoded::getObjectEncoded(
                                                'Document',
                                                array(
                                                    "status" => "ERROR",
                                                    "message" => "File doesn't exist !"
                                                )
                                            )
                                        );
                                    break;
                                case "special":
                                    $file_name = 'file.png';
                                    $special_file = Yii::getPathOfAlias('application.modules.Documents.data') . DIRECTORY_SEPARATOR . $file_name;
                                    if (is_file($special_file)) {
                                        $filePath = Storage::model()->findByPk(Storage::API_STORAGE_ID)->path . time() . '_' . rand() . '.' . pathinfo($special_file, PATHINFO_EXTENSION);

                                        HelpersStorage::MoveFile($special_file, $filePath);
                                        $document_id = Document::create($model_name, $filePath);

                                        $arr = SetDocument::saveDocument($filePath, $document_id, $model_name, $model);
                                    }
                                    break;
                                case "template_pdf":
                                    $arr = SetDocument::saveDocumentByPdf($model_name, $model);
                                    break;
                                default:
                                    Response::sendResponse(500,
                                        ObjectEncoded::getObjectEncoded(
                                            'Document',
                                            array(
                                                "status" => "ERROR",
                                                "message" => 'Type is missing !'
                                            )
                                        )
                                    );
                                    break;
                            }

                            if (is_numeric($document_id)) {
                                if (isset($model["bound"]) && is_numeric($model["bound"])) {
                                    SetDocument::saveBoundDocument($document_id, $model["bound"]);
                                }
                                if (isset($model["task_id"]) && is_numeric($model["task_id"])) {
                                    SetDocument::saveTaskDocument($document_id, $model["task_id"]);
                                }
                            }
                        } else
                            Response::sendResponse(203,
                                ObjectEncoded::getObjectEncoded(
                                    'Document',
                                    array(
                                        "status" => "ERROR",
                                        "message" => "Parameter <b>use_as</b> is missing !"
                                    )
                                )
                            );
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function saveDocument($filePath, $document_id, $model_name, $elements)
    {
        @unlink($filePath);
        HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($document_id));
        $instance = new $model_name;
        $instance->document_id = $document_id;

        $table = Yii::app()->getDb()->getSchema()->getTable($instance->tableName());
        $columnNames = $table->getColumnNames();

        foreach ($elements as $key_model => $element)
            if (in_array($key_model, $columnNames))
                $instance->$key_model = $element;
        $instance->save();
        SetDocument::updateDocument($document_id, $instance->id);
        return array("id" => $instance->id, "document_id" => $document_id);
    }

    public static function updateDocument($document_id, $instance)
    {
        $user_api = SetDocument::getUserApi();
        Document::model()->updateByPk($document_id, array("instance_id" => $instance, "create_user_id" => $user_api->id, "create_datetime" => date('Y-m-d H:i:s')));
    }

    public static function getUserApi()
    {
        if (!isset($_GET['login']))
            Response::sendResponse(203,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => "Parameter <b>login</b> is missing !"
                    )
                )
            );
        else
            $login = strtolower($_GET['login']);
        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), true);
        $user_api = User::model()->find($criteria);
        return $user_api;
    }

    public static function saveDocumentByPdf($model_name, $elements)
    {
        $model = new $model_name();
        $docType = DocumentType::model()->findByAttributes(array('instance_model_name' => $model_name));
        $template_link = DocumentsTypesTemplatesPdfLink::model()->findByAttributes(array('type_id' => $docType->id));
        $template = DocumentsTemplatesPdf::model()->findByPk($template_link->template_id);

        $storage = Storage::model()->findByPk(Storage::TEMPLATES_STORAGE_ID);
        $temp = Storage::model()->findByPk(Storage::APP_STORAGE_ID);
        $tempPath = $temp->path . $template->saved_name;
        $templatePath = $storage->path . $template->saved_name;
        $rez = HelpersStorage::StreamCopyFile($templatePath, $tempPath);
        if ($rez) {
            $table = Yii::app()->getDb()->getSchema()->getTable($model->tableName());
            $columnNames = $table->getColumnNames();
            foreach ($elements as $key_model => $element)
                if (in_array($key_model, $columnNames))
                    $model->$key_model = $element;

            //document_id, sursa, autoritatea, nr_nota, nume_analist, subiect_tip, nr_contului, fabula

            $model->document_id = HelperTemplates::replaceTokensPdf($template->file_content, $model);
            if (isset($model->document_id) && !is_numeric($model->document_id)) {
                $pdf = Yii::createComponent('application.modules.Documents.extensions.tcpdf.ETcPdf', 'P', 'mm', 'A4', true, 'ISO-8859-1', false);

                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->SetFont('dejavusans', 'N', 10);

                $pdf->SetPrintHeader(false);
                $pdf->SetPrintFooter(false);

                $pdf->AddPage();
                $pdf_data = str_replace('&nbsp;', ' ', $model->document_id);

                $pdf->writeHTML($pdf_data, true, false, false, false, '');
                $filePath = Storage::model()->findByPk(Storage::API_STORAGE_ID)->path . time() . '_' . rand() . '.' . pathinfo("template.pdf", PATHINFO_EXTENSION);
                $pdf->Output($filePath, "F");
                $model->document_id = SetDocument::remoteCreateDocument($model_name, $filePath);
            }

            if ($model->document_id && $model->save()) {
                $mdName = $model_name . 'History';
                HelpersDocumentHistory::HistoryCreate($model->document_id, new $mdName);
                try {
                    HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($model->document_id));

                } catch (Exception $ex) {
                    Response::sendResponse(104,
                        ObjectEncoded::getObjectEncoded(
                            'Document',
                            array(
                                "status" => "ERROR",
                                "message" => 'nu a fost posibil MoveFileToOperativ'
                            )
                        )
                    );
                }
                HelpersDocumentHistory::HistoryMoveToOperativ($model->document_id, new $mdName);
                return array("id" => $model->id, "document_id" => $model->document_id);
            } else
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Generate error'
                        )
                    )
                );
        }

    }

    public static function remoteCreateDocument($instance, $file)
    {
        $model = new Document;
        $tryType = DocumentType::model()->findByAttributes(array('instance_model_name' => $instance));
        $user_api = SetDocument::getUserApi();
        if ($tryType) {
            $model->type_id = $tryType->id;
            $model->files = CFile::set($file);
            $model->file = $model->files->realpath;
            $model->create_user_id = $user_api->id;
            if ($model->save()) {
                $newfilepath = $model->id . DIRECTORY_SEPARATOR . $model->file;
                HelpersStorage::CheckAndCreateDir(HelpersStorage::GetTempPath() . DIRECTORY_SEPARATOR . $model->id);
                HelpersStorage::MoveFile($file, HelpersStorage::GetTempPath() . DIRECTORY_SEPARATOR . $newfilepath);
                HelpersStorage::RemoveFile($file);
                Document::model()->updateByPk($model->id, array('file' => $newfilepath));
                return $model->id;
            }
        }
        return false;
    }

    public static function saveBoundDocument($document_id, $instance_id)
    {
        Bound::model()->newRecord($instance_id, $document_id);
        Bound::model()->newRecord($document_id, $instance_id);
    }

    public static function saveTaskDocument($document_id, $task_id)
    {
        $user_api = SetDocument::getUserApi();
        $taskAttached = new TaskAttached;
        $taskAttached->task_id = $task_id;
        $taskAttached->document_id = $document_id;
        $taskAttached->user_id = $user_api->id;
        $taskAttached->create_time = date('Y-m-d H:i:s');
        $taskAttached->save();
    }

    /* public static function setData($model_name)
     {
         $arr = array("Error"=>"No");
         $body = file_get_contents("php://input");
         $content_type = false;
         if(isset($_SERVER['CONTENT_TYPE'])) {
             $content_type = $_SERVER['CONTENT_TYPE'];
         }
         switch($content_type) {
             case "application/json":
                 $body_params = json_decode($body);

                 if(isset($body_params) && !empty($body_params))
                     foreach($body_params as $key=>$body_param)
                     {//die(var_dump($body_param));
                         $model = new $model_name;
                         foreach($body_param as $param_name => $param_value){
                             $model[$param_name] = $param_value;
                         }
                         $arr = array("id"=>SetDocument::saveInModel($model));
                     }
                 break;
             default:
                 Response::sendResponse(500,
                     ObjectEncoded::getObjectEncoded(
                         'Document',
                         array(
                             "status" => "ERROR",
                             "message" =>'Content type <b>'.$content_type.'</b> is not accept'
                         )
                     )
                 );
                 break;
         }
         return $arr;
     }*/

    public static function setDataByPk($model_name, $id)
    {
        //aici trebuie sa se returneze versiunea aplicatiei
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {//die(var_dump($body_param));
                        $model = $model_name::model()->findByPK($id);
                        if (!RightsList::checkDocumentRight($model->document_id, 2)) // 2 is right to write doc
                            Response::sendResponse(403);
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        SetDocument::saveInModel($model);
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function saveInModel($model)
    {
        // Try to save the model
        try {
            if (!$model->save()) {
                // Errors occurred
                $msg = "Error";
                foreach ($model->errors as $attribute => $attr_errors) {
                    $msg .= "Attribute: $attribute";
                    foreach ($attr_errors as $attr_error)
                        $msg .= $attr_error;
                }
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => $msg
                        )
                    )
                );
            }
        } catch (Exception $ex) {
            Response::sendResponse(500,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => $ex->getMessage()
                    )
                )
            );
        }
    }

}