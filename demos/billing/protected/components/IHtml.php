<?php

class IHtml extends CHtml
{
    public static $bs_inputClasses = ['class' => 'form-control'];
    public static $bs_BtnClasses = ['class' => 'btn btn-primary btn-block'];
    public static $requiredCss = '';
    public static $afterRequiredLabel = ' <span class="text-danger">*</span>';

    public static function btnGroup($items)
    {
        $result = "<div class='btn-group' role='group'>";
        foreach ($items as $item) {
            if (Yii::app()->controller->id . DIRECTORY_SEPARATOR . Yii::app()->controller->action->id != $item['url'][0])
                $result .= IHtml::link($item['label'], $item['url'],
                    [
                        'class' => 'btn btn-default'
                    ]);
        }
        $result .= "</div>";
        return $result;
    }

    public static function activeLabelEx($model, $attribute, $htmlOptions = array())
    {
        $realAttribute = $attribute;
        self::resolveName($model, $attribute); // strip off square brackets if any
        if (!isset($htmlOptions['required']))
            $htmlOptions['required'] = $model->isAttributeRequired($attribute);
        return self::activeLabel($model, $realAttribute, $htmlOptions);
    }

    public static function label($label, $for, $htmlOptions = array())
    {
        if ($for === false)
            unset($htmlOptions['for']);
        else
            $htmlOptions['for'] = $for;
        if (isset($htmlOptions['required'])) {
            if ($htmlOptions['required']) {
                if (isset($htmlOptions['class']))
                    $htmlOptions['class'] .= ' ' . self::$requiredCss;
                else
                    $htmlOptions['class'] = self::$requiredCss;
                $label = self::$beforeRequiredLabel . $label . self::$afterRequiredLabel;
            }
            unset($htmlOptions['required']);
        }
        return self::tag('label', $htmlOptions, $label);
    }

    public static function activeLabel($model, $attribute, $htmlOptions = array())
    {
        $inputName = self::resolveName($model, $attribute);
        if (isset($htmlOptions['for'])) {
            $for = $htmlOptions['for'];
            unset($htmlOptions['for']);
        } else
            $for = self::getIdByName($inputName);
        if (isset($htmlOptions['label'])) {
            if (($label = $htmlOptions['label']) === false)
                return '';
            unset($htmlOptions['label']);
        } else
            $label = $model->getAttributeLabel($attribute);
        if ($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);
        return self::label($label, $for, $htmlOptions);
    }

    public static function textField($name, $value = '', $htmlOptions = array())
    {
        self::clientChange('change', $htmlOptions);
        return self::inputField('text', $name, $value, array_merge(IHtml::$bs_inputClasses, $htmlOptions));
    }

    public static function dropDownList($name, $select, $data, $htmlOptions = array())
    {
        $htmlOptions['name'] = $name;

        if (!isset($htmlOptions['id']))
            $htmlOptions['id'] = self::getIdByName($name);
        elseif ($htmlOptions['id'] === false)
            unset($htmlOptions['id']);

        self::clientChange('change', $htmlOptions);
        $options = "\n" . self::listOptions($select, $data, $htmlOptions);
        $hidden = '';

        if (!empty($htmlOptions['multiple'])) {
            if (substr($htmlOptions['name'], -2) !== '[]')
                $htmlOptions['name'] .= '[]';

            if (isset($htmlOptions['unselectValue'])) {
                $hiddenOptions = isset($htmlOptions['id']) ? array('id' => self::ID_PREFIX . $htmlOptions['id']) : array('id' => false);
                $hidden = self::hiddenField(substr($htmlOptions['name'], 0, -2), $htmlOptions['unselectValue'], $hiddenOptions);
                unset($htmlOptions['unselectValue']);
            }
        }
        // add a hidden field so that if the option is not selected, it still submits a value
        return $hidden . self::tag('select', array_merge(IHtml::$bs_inputClasses, $htmlOptions), $options);
    }

    public static function submitButton($label = 'submit', $htmlOptions = array())
    {
        $htmlOptions['type'] = 'submit';
        return self::button($label, array_merge(IHtml::$bs_BtnClasses, $htmlOptions));
    }

    public static function textArea($name, $value = '', $htmlOptions = array())
    {
        $htmlOptions = array_merge(IHtml::$bs_inputClasses, $htmlOptions);
        $htmlOptions['name'] = $name;
        if (!isset($htmlOptions['id']))
            $htmlOptions['id'] = self::getIdByName($name);
        elseif ($htmlOptions['id'] === false)
            unset($htmlOptions['id']);
        self::clientChange('change', $htmlOptions);
        return self::tag('textarea', $htmlOptions, isset($htmlOptions['encode']) && !$htmlOptions['encode'] ? $value : self::encode($value));
    }

    public static function idate($what)
    {
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($what, 'yyyy-MM-dd'), 'medium', null);
    }

    public static function controllerExists($id)
    {
        $i = ucfirst($id);
        $controllerPath = Yii::app()->getControllerPath();
        return file_exists($controllerPath . DIRECTORY_SEPARATOR . $i . "Controller.php");
    }

    public static function getActions($controller, $path)
    {
        $controller .= 'Controller.php';
        $actions = array();
        //die(var_dump($controller));
        if (file_exists($path . DIRECTORY_SEPARATOR . $controller)) {
            $source = file_get_contents($path . DIRECTORY_SEPARATOR . $controller);
            preg_match_all('/function (?:(actions)|action(\w+))\(/i', $source, $matches, PREG_SET_ORDER);

            foreach ($matches as $match) {
                if (!empty($match[1])) {
                    $actionsMethod = '';
                    $braces = 0;
                    $pos = stripos($source, 'function actions()') + strlen('function actions()');

                    while (($c = $source[++$pos]) !== '{') ;
                    do {
                        $c = $source[$pos++];
                        if ($c === '{')
                            $braces++;
                        elseif ($c === '}')
                            $braces--;
                        $actionsMethod .= $c;
                    } while ($braces > 0);

                    preg_match_all('/([\'"])(\w+)\1.*?class/si', $actionsMethod, $classes, PREG_SET_ORDER);

                    foreach ($classes as $class)
                        $actions[] = (object)array('id' => ucfirst($class[2]));
                } else
                    $actions[] = (object)array('id' => ucfirst($match[2]));
            }//foreach
        }//file_exists
        return $actions;
    }
}