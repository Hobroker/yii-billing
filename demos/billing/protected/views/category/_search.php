<?php
/* @var $this CategoryController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idcateg'); ?>
		<?php echo $form->textField($model,'idcateg'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'namecateg'); ?>
		<?php echo $form->textField($model,'namecateg',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->