<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Set model data</h3>
                    <p>Importă date într-un model</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Tipsubiect", "data": [{"name":"test"},
                            {"name":"test2"}]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - atributele într-un array
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"id":"32", "insert":true}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele pentru inserare
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Coloana specificată în data nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"OK","response":[{"insert":false,"error":"Check model structure!"}]}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putData
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Tipsubiect", "data": [{"name":"test"},
                                    {"name":"test2"}]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Put PDF document</h3>
                    <p>Inserare document PDF</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Documents", "model": "Qwe", "data": {"name":"test"}, "template": "TestApi2"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - datele pentru import
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Parametri opționali:</p>
                    <ol>
                        <li>
                            <i>template</i> - template-ul ce trebuie folosit
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"save":true,"template":"TestApi2","id":"23"}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele noi (data)
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Coloana specificată în data nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putPdf
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Documents", "model": "Qwe", "data": {"name":"test"}, "template":
                                    "TestApi2"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Put DOCX document</h3>
                    <p>Inserare document DOCX</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Documents", "model": "Asd", "data": {"name":"test"}, "template": "TestApi2"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - datele pentru import
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Parametri opționali:</p>
                    <ol>
                        <li>
                            <i>template</i> - template-ul ce trebuie folosit
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"save":true,"template":"TestApi2","id":"23"}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele noi (data)
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Coloana specificată în data nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putDoc
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Documents", "model": "Asd", "data": {"name":"test"}, "template":
                                    "TestApi2"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Put File document</h3>
                    <p>Inserare document File</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            0=[{"module": "Documents", "model": "Qwe", "data": {"name":"test"}, "template": "TestApi2"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - datele pentru import
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"save":true,"template":"TestApi2","id":"23"}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele noi (data)
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Coloana specificată în data nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putFile
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    0=[{"module": "Documents", "model": "Qwe", "data": {"name":"test"}}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Put Special document</h3>
                    <p>Inserare document Special</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Documents", "model": "Qwe", "data": {"name":"test"}, "template": "TestApi2"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - datele pentru import
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"save":true,"template":"TestApi2","id":"23"}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele noi (data)
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Coloana specificată în data nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putSpecial
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Documents", "model": "Qwe", "data": {"name":"test"}}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Update model data by pk</h3>
                    <p>Actualizare date într-un model după cheia primară</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module":"Clasificator", "model":"Tipsubiect", "data":[{"pk":11, "name":"test1_update"},
                            {"pk":12, "name":"test2_update"}]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>data</i> - datele noi într-un array, unul dintre atribute trebuie să fie <i>pk</i>, ce
                            specifică cheia primară
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"id":"32", "insert":true}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificată cheia primară în array-ul data. Vor fi actualizate doar datele in array ce
                        au cheia primară specificată
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-warning">
                                    {"status":"OK","response":[{"update":false,"error":"Pk not specified!"}]}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată este incorectă
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Invalid Primary Key `12q`!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată este corectă, dar nu este găsită în modul. Vor fi returnate doar
                        datele cu cheia primară validă
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"OK","response":[{"error":"Invalid Primary Key `234`"}]}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/putData
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Tipsubiect", "data": [{"name":"test"},
                                    {"name":"test2"}]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Update model data by attributes</h3>
                    <p>Actualizare date într-un model după atribute</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="put"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Tipsubiect", s"data": [{"name":"new"} "criteria":
                            [{"column":"name", "value":"test", "condition":"="}]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>criteria</i> - atributele
                            <ul>
                                <li>
                                    <i>column</i> – numele coloanei
                                </li>
                                <li>
                                    <i>value</i> – valoarea cu care se compară <i>column</i>
                                </li>
                                <li>
                                    <i>condition</i> – condiția de comparare. Spre exemplu like, =, >=, ...
                                </li>
                            </ul>
                        </li>
                        <li>
                            <i>data</i> - datele noi
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"id":"32", "insert":true}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate datele noi (data)
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-warning">
                                    {"status":"ERROR","message":"Data not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate atributele
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Attributes not specified!"}
                                </div>
                            </div>
                        </div>
                        <ul>
                            <li>
                                Nu este specificată coloana (column) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Column not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată valoarea (value) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Value not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată condiția (condition) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Condition not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Coloana specificată în atribute (column) nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/updateDataByAttributes
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Tipsubiect", "data": [{"name":"new"}
                                    "criteria":
                                    [{"column":"name", "value":"test", "condition":"="}]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Delete model data by pk</h3>
                    <p>Șterge date dintr-un model după cheia primară</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="delete"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Magic", "pk": [3,5]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>pk</i> - cheile primare într-un array
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"id":"32", "delete":true}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată este incorectă
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Invalid Primary Key `12q`!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată este corectă, dar nu este găsită în modul. Vor fi returnate doar
                        datele cu cheia primară validă.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"OK","response":[{"error":"Invalid Primary Key `234`"}]}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/deleteDataByPk
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Magic", "pk": [3,5]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Delete model data by attributes</h3>
                    <p>Șterge date dintr-un model după atribute</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="delete"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Magic", "pk": [3,5]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>criteria</i> - atributele
                            <ul>
                                <li>
                                    <i>column</i> – numele coloanei
                                </li>
                                <li>
                                    <i>value</i> – valoarea cu care se compară <i>column</i>
                                </li>
                                <li>
                                    <i>condition</i> – condiția de comparare. Spre exemplu like, =, >=, ...
                                </li>
                            </ul>
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"id":"32", "delete":true}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate atributele
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Attributes not specified!"}
                                </div>
                            </div>
                        </div>
                        <ul>
                            <li>
                                Nu este specificată coloana (column) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Column not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată valoarea (value) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Value not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată condiția (condition) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Condition not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/deleteDataByAttributes
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Tipsubiect", "criteria": [{"column":"name", "value":"test", "condition":"="}]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>