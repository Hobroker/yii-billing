--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.7
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authassignment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authassignment (
    itemname character varying(64) NOT NULL,
    userid character varying(64) NOT NULL,
    bizrule text,
    data text
);


ALTER TABLE authassignment OWNER TO postgres;

--
-- Name: authitem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authitem (
    name character varying(64) NOT NULL,
    type integer NOT NULL,
    description text,
    bizrule text,
    data text
);


ALTER TABLE authitem OWNER TO postgres;

--
-- Name: authitemchild; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authitemchild (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


ALTER TABLE authitemchild OWNER TO postgres;

--
-- Name: tbl_manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_manufacturers (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE tbl_manufacturers OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE manufacturers_id_seq OWNED BY tbl_manufacturers.id;


--
-- Name: tbl_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_roles (
    "idRole" integer NOT NULL,
    "nameRole" character varying(50),
    permissions character varying(50)
);


ALTER TABLE tbl_roles OWNER TO postgres;

--
-- Name: roles_idrole_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_idrole_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_idrole_seq OWNER TO postgres;

--
-- Name: roles_idrole_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_idrole_seq OWNED BY tbl_roles."idRole";


--
-- Name: tbl_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_categories (
    idcateg integer NOT NULL,
    namecateg character varying(100)
);


ALTER TABLE tbl_categories OWNER TO postgres;

--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_category_idcateg_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_category_idcateg_seq OWNER TO postgres;

--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_category_idcateg_seq OWNED BY tbl_categories.idcateg;


--
-- Name: tbl_orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_orders (
    idcpi integer NOT NULL,
    iduser integer,
    idprod integer,
    date date,
    idinv integer
);


ALTER TABLE tbl_orders OWNER TO postgres;

--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_client_prod_inv_idcpi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_client_prod_inv_idcpi_seq OWNER TO postgres;

--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_client_prod_inv_idcpi_seq OWNED BY tbl_orders.idcpi;


--
-- Name: tbl_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_comment (
    id integer NOT NULL,
    content text NOT NULL,
    status integer NOT NULL,
    create_time integer,
    author character varying(128) NOT NULL,
    email character varying(128) NOT NULL,
    url character varying(128),
    post_id integer NOT NULL
);


ALTER TABLE tbl_comment OWNER TO postgres;

--
-- Name: tbl_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_comment_id_seq OWNER TO postgres;

--
-- Name: tbl_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_comment_id_seq OWNED BY tbl_comment.id;


--
-- Name: tbl_invoices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_invoices (
    idinv integer NOT NULL,
    aprobat character(1)
);


ALTER TABLE tbl_invoices OWNER TO postgres;

--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_invoices_idinv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_invoices_idinv_seq OWNER TO postgres;

--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_invoices_idinv_seq OWNED BY tbl_invoices.idinv;


--
-- Name: tbl_lookup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_lookup (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    code integer NOT NULL,
    type character varying(128) NOT NULL,
    "position" integer NOT NULL
);


ALTER TABLE tbl_lookup OWNER TO postgres;

--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_lookup_id_seq OWNER TO postgres;

--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_lookup_id_seq OWNED BY tbl_lookup.id;


--
-- Name: tbl_organisations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_organisations (
    "idOrg" integer NOT NULL,
    name character varying(100),
    address character varying(200)
);


ALTER TABLE tbl_organisations OWNER TO postgres;

--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_organisations_idorg_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_organisations_idorg_seq OWNER TO postgres;

--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_organisations_idorg_seq OWNED BY tbl_organisations."idOrg";


--
-- Name: tbl_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_post (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    content text NOT NULL,
    tags text,
    status integer NOT NULL,
    create_time integer,
    update_time integer,
    author_id integer NOT NULL
);


ALTER TABLE tbl_post OWNER TO postgres;

--
-- Name: tbl_post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_post_id_seq OWNER TO postgres;

--
-- Name: tbl_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_post_id_seq OWNED BY tbl_post.id;


--
-- Name: tbl_prod_sale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_prod_sale (
    idps integer NOT NULL,
    idprod integer,
    idsale integer
);


ALTER TABLE tbl_prod_sale OWNER TO postgres;

--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_prodsale_idps_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_prodsale_idps_seq OWNER TO postgres;

--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_prodsale_idps_seq OWNED BY tbl_prod_sale.idps;


--
-- Name: tbl_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_products (
    idprod integer NOT NULL,
    idcateg integer,
    nameprod character varying(100),
    priceprod numeric(9,2),
    idmanufacturer integer
);


ALTER TABLE tbl_products OWNER TO postgres;

--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_products_idprod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_products_idprod_seq OWNER TO postgres;

--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_products_idprod_seq OWNED BY tbl_products.idprod;


--
-- Name: tbl_sales; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_sales (
    idsale integer NOT NULL,
    valuesale integer,
    startdate date,
    enddate date
);


ALTER TABLE tbl_sales OWNER TO postgres;

--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_sales_idsale_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_sales_idsale_seq OWNER TO postgres;

--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_sales_idsale_seq OWNED BY tbl_sales.idsale;


--
-- Name: tbl_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_tag (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    frequency integer DEFAULT 1
);


ALTER TABLE tbl_tag OWNER TO postgres;

--
-- Name: tbl_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_tag_id_seq OWNER TO postgres;

--
-- Name: tbl_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_tag_id_seq OWNED BY tbl_tag.id;


--
-- Name: tbl_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_user (
    id integer NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(128) NOT NULL,
    email character varying(128) NOT NULL,
    profile text,
    fname character varying(128),
    lname character varying(128),
    tel character varying(32),
    idorg integer
);


ALTER TABLE tbl_user OWNER TO postgres;

--
-- Name: tbl_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_user_id_seq OWNER TO postgres;

--
-- Name: tbl_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_user_id_seq OWNED BY tbl_user.id;


--
-- Name: idcateg; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_categories ALTER COLUMN idcateg SET DEFAULT nextval('tbl_category_idcateg_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment ALTER COLUMN id SET DEFAULT nextval('tbl_comment_id_seq'::regclass);


--
-- Name: idinv; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_invoices ALTER COLUMN idinv SET DEFAULT nextval('tbl_invoices_idinv_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_lookup ALTER COLUMN id SET DEFAULT nextval('tbl_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_manufacturers ALTER COLUMN id SET DEFAULT nextval('manufacturers_id_seq'::regclass);


--
-- Name: idcpi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders ALTER COLUMN idcpi SET DEFAULT nextval('tbl_client_prod_inv_idcpi_seq'::regclass);


--
-- Name: idOrg; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations ALTER COLUMN "idOrg" SET DEFAULT nextval('tbl_organisations_idorg_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post ALTER COLUMN id SET DEFAULT nextval('tbl_post_id_seq'::regclass);


--
-- Name: idps; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale ALTER COLUMN idps SET DEFAULT nextval('tbl_prodsale_idps_seq'::regclass);


--
-- Name: idprod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products ALTER COLUMN idprod SET DEFAULT nextval('tbl_products_idprod_seq'::regclass);


--
-- Name: idRole; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_roles ALTER COLUMN "idRole" SET DEFAULT nextval('roles_idrole_seq'::regclass);


--
-- Name: idsale; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_sales ALTER COLUMN idsale SET DEFAULT nextval('tbl_sales_idsale_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_tag ALTER COLUMN id SET DEFAULT nextval('tbl_tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user ALTER COLUMN id SET DEFAULT nextval('tbl_user_id_seq'::regclass);


--
-- Data for Name: authassignment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authassignment VALUES ('admin', '8', NULL, 'N;');


--
-- Data for Name: authitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authitem VALUES ('createCategory', 0, 'create a category', NULL, 'N;');
INSERT INTO authitem VALUES ('deleteCategory', 0, 'Delete a product category', NULL, 'N;');
INSERT INTO authitem VALUES ('admin', 2, '', NULL, 'N;');


--
-- Data for Name: authitemchild; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authitemchild VALUES ('admin', 'createCategory');
INSERT INTO authitemchild VALUES ('admin', 'deleteCategory');


--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 6, true);


--
-- Name: roles_idrole_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_idrole_seq', 1, true);


--
-- Data for Name: tbl_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_categories VALUES (1, 'Laptop');
INSERT INTO tbl_categories VALUES (3, 'Smartphone');
INSERT INTO tbl_categories VALUES (4, 'Mouse');
INSERT INTO tbl_categories VALUES (5, 'Keyboard');
INSERT INTO tbl_categories VALUES (6, 'Power Bank');
INSERT INTO tbl_categories VALUES (7, 'Hard Disk');
INSERT INTO tbl_categories VALUES (8, 'Tablet');


--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_category_idcateg_seq', 8, true);


--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_client_prod_inv_idcpi_seq', 8, true);


--
-- Data for Name: tbl_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_comment VALUES (1, 'This is a test comment.', 2, 1230952187, 'Tester', 'tester@example.com', NULL, 2);


--
-- Name: tbl_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_comment_id_seq', 1, true);


--
-- Data for Name: tbl_invoices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_invoices VALUES (176, '1');
INSERT INTO tbl_invoices VALUES (192, '1');
INSERT INTO tbl_invoices VALUES (193, '1');


--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_invoices_idinv_seq', 193, true);


--
-- Data for Name: tbl_lookup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_lookup VALUES (1, 'Draft', 1, 'PostStatus', 1);
INSERT INTO tbl_lookup VALUES (2, 'Published', 2, 'PostStatus', 2);
INSERT INTO tbl_lookup VALUES (3, 'Archived', 3, 'PostStatus', 3);
INSERT INTO tbl_lookup VALUES (4, 'Pending Approval', 1, 'CommentStatus', 1);
INSERT INTO tbl_lookup VALUES (5, 'Approved', 2, 'CommentStatus', 2);


--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_lookup_id_seq', 5, true);


--
-- Data for Name: tbl_manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_manufacturers VALUES (1, 'LG');
INSERT INTO tbl_manufacturers VALUES (2, 'Sony');
INSERT INTO tbl_manufacturers VALUES (3, 'Asus');
INSERT INTO tbl_manufacturers VALUES (4, 'Acer');
INSERT INTO tbl_manufacturers VALUES (5, 'Dell');
INSERT INTO tbl_manufacturers VALUES (6, 'Samsung');


--
-- Data for Name: tbl_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_orders VALUES (3, 8, 1, NULL, 192);
INSERT INTO tbl_orders VALUES (7, 8, 1, '2016-04-07', 193);
INSERT INTO tbl_orders VALUES (8, 8, 1, '2016-04-07', 193);
INSERT INTO tbl_orders VALUES (6, 1, 1, '2016-04-06', 176);
INSERT INTO tbl_orders VALUES (2, 1, 2, NULL, 176);


--
-- Data for Name: tbl_organisations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_organisations VALUES (1, 'Qaz1', 'str. Nr 1');


--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_organisations_idorg_seq', 3, true);


--
-- Data for Name: tbl_post; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_post VALUES (1, 'Welcome!', 'This blog system is developed using Yii. It is meant to demonstrate how to use Yii to build a complete real-world application. Complete source code may be found in the Yii releases.

Feel free to try this system by writing new posts and posting comments.', 'yii, blog', 2, 1230952187, 1230952187, 1);
INSERT INTO tbl_post VALUES (2, 'A Test Post', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'test', 2, 1230952187, 1230952187, 1);


--
-- Name: tbl_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_post_id_seq', 2, true);


--
-- Data for Name: tbl_prod_sale; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_prod_sale VALUES (3, 1, 3);


--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_prodsale_idps_seq', 3, true);


--
-- Data for Name: tbl_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_products VALUES (1, 4, 'WT415', 526.00, 3);
INSERT INTO tbl_products VALUES (2, 3, 'Xperia Z5', 20100.00, 2);
INSERT INTO tbl_products VALUES (3, 7, 'Spinpoint M8', 2000.00, 6);


--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_products_idprod_seq', 3, true);


--
-- Data for Name: tbl_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_roles VALUES (1, 'Administrator', '1111');


--
-- Data for Name: tbl_sales; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_sales VALUES (3, 15, '2016-04-01', '2016-04-21');
INSERT INTO tbl_sales VALUES (2, 50, '2016-04-02', '2016-04-09');


--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_sales_idsale_seq', 3, true);


--
-- Data for Name: tbl_tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_tag VALUES (1, 'yii', 1);
INSERT INTO tbl_tag VALUES (2, 'blog', 1);
INSERT INTO tbl_tag VALUES (3, 'test', 1);


--
-- Name: tbl_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_tag_id_seq', 3, true);


--
-- Data for Name: tbl_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_user VALUES (1, 'demo1', '$2a$10$JTJf6/XqC94rrOtzuF397OHa4mbmZrVTBOQCmYD9U.obZRUut4BoC', 'webmaster@example.com', '', 'Igor', 'Leahu', '+37378672301', 1);
INSERT INTO tbl_user VALUES (8, 'magic', '$2y$13$HqWQ5NriaUsuIRbpC94HDO7F1l4WS9Jbrh9V/RPVJrMaCMlHCLw0G', 'igor.leahu24@gmail.com', '', 'John', 'Doe', '+373556775124', 1);


--
-- Name: tbl_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_user_id_seq', 8, true);


--
-- Name: authassignment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authassignment
    ADD CONSTRAINT authassignment_pkey PRIMARY KEY (itemname, userid);


--
-- Name: authitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitem
    ADD CONSTRAINT authitem_pkey PRIMARY KEY (name);


--
-- Name: authitemchild_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_pkey PRIMARY KEY (parent, child);


--
-- Name: manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: pk_organisations; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations
    ADD CONSTRAINT pk_organisations PRIMARY KEY ("idOrg");


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY ("idRole");


--
-- Name: tbl_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_categories
    ADD CONSTRAINT tbl_category_pkey PRIMARY KEY (idcateg);


--
-- Name: tbl_client_prod_inv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_client_prod_inv_pkey PRIMARY KEY (idcpi);


--
-- Name: tbl_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment
    ADD CONSTRAINT tbl_comment_pkey PRIMARY KEY (id);


--
-- Name: tbl_invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_invoices
    ADD CONSTRAINT tbl_invoices_pkey PRIMARY KEY (idinv);


--
-- Name: tbl_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_lookup
    ADD CONSTRAINT tbl_lookup_pkey PRIMARY KEY (id);


--
-- Name: tbl_organisations_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations
    ADD CONSTRAINT tbl_organisations_name_key UNIQUE (name);


--
-- Name: tbl_post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post
    ADD CONSTRAINT tbl_post_pkey PRIMARY KEY (id);


--
-- Name: tbl_prodsale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_pkey PRIMARY KEY (idps);


--
-- Name: tbl_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_products_pkey PRIMARY KEY (idprod);


--
-- Name: tbl_sales_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_sales
    ADD CONSTRAINT tbl_sales_pkey PRIMARY KEY (idsale);


--
-- Name: tbl_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_tag
    ADD CONSTRAINT tbl_tag_pkey PRIMARY KEY (id);


--
-- Name: tbl_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user
    ADD CONSTRAINT tbl_user_pkey PRIMARY KEY (id);


--
-- Name: authassignment_itemname_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authassignment
    ADD CONSTRAINT authassignment_itemname_fkey FOREIGN KEY (itemname) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authitemchild_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_child_fkey FOREIGN KEY (child) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authitemchild_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_parent_fkey FOREIGN KEY (parent) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tbl_client_prod_inv_iduser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_client_prod_inv_iduser_fkey FOREIGN KEY (iduser) REFERENCES tbl_user(id);


--
-- Name: tbl_comment_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment
    ADD CONSTRAINT tbl_comment_post_id_fkey FOREIGN KEY (post_id) REFERENCES tbl_post(id);


--
-- Name: tbl_manufacturer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_manufacturer_fkey FOREIGN KEY (idmanufacturer) REFERENCES tbl_manufacturers(id);


--
-- Name: tbl_orders_idinv_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_orders_idinv_fkey FOREIGN KEY (idinv) REFERENCES tbl_invoices(idinv);


--
-- Name: tbl_orders_idprod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_orders_idprod_fkey FOREIGN KEY (idprod) REFERENCES tbl_products(idprod);


--
-- Name: tbl_post_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post
    ADD CONSTRAINT tbl_post_author_id_fkey FOREIGN KEY (author_id) REFERENCES tbl_user(id);


--
-- Name: tbl_prodsale_idprod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_idprod_fkey FOREIGN KEY (idprod) REFERENCES tbl_products(idprod);


--
-- Name: tbl_prodsale_idsale_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_idsale_fkey FOREIGN KEY (idsale) REFERENCES tbl_sales(idsale);


--
-- Name: tbl_products_idcateg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_products_idcateg_fkey FOREIGN KEY (idcateg) REFERENCES tbl_categories(idcateg);


--
-- Name: tbl_user_idorg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user
    ADD CONSTRAINT tbl_user_idorg_fkey FOREIGN KEY (idorg) REFERENCES tbl_organisations("idOrg");


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

