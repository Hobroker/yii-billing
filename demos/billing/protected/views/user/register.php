<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs = array(
    'Register',
);
?>


<div class="col-md-10 col-md-offset-1">
    <h2>Register</h2>

    <?php $form = $this->beginWidget('IActiveForm', array(
        'id' => 'user-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <div class="col-md-12">
            <p class="note">Fields with <span class="text-danger">*</span> are required.</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php echo $form->errorSummary($model); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'fname'); ?>
                <?php echo $form->textField($model, 'fname', array('size' => 50, 'maxlength' => 50)); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'lname'); ?>
                <?php echo $form->textField($model, 'lname', array('size' => 50, 'maxlength' => 50)); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'email'); ?>
                <?php echo $form->textField($model, 'email', array('size' => 60, 'maxlength' => 128)); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'username'); ?>
                <?php echo $form->textField($model, 'username', array('size' => 60, 'maxlength' => 128)); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'password'); ?>
                <?php echo $form->passwordField($model, 'password', array('size' => 60, 'maxlength' => 128)); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'idorg'); ?>
                <?= CHtml::activeDropDownList($model, 'idorg',
                    CHtml::listData(Organisations::model()->findAll(), 'idOrg', 'name'),
                    [
                        'class' => 'form-control',
                        'prompt' => 'Select an organisation'
                    ]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'tel'); ?>
                <?php echo $form->textField($model, 'tel'); ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'profile'); ?>
                <?php echo $form->textArea($model, 'profile', array('form-groups' => 6, 'rows' => 3)); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?php echo $form->submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div>