<?php $this->beginContent('/layouts/main'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <?php
                if (Yii::app()->user->checkAccess('admin'))
                    echo IHtml::btnGroup($this->menu);
                ?>
            </div>
            <div class="col-md-9">
                <?php echo $content; ?>
            </div>
            <div class="col-md-3 pdt-sidebar">
                <h3>Categories</h3>
                <?php
                $this->widget(
                    'IMenu', array(
                        'items' => Categories::getMenuItems(),
                        'encodeLabel' => false
                    )
                );
                ?>

                <h3>Manufacturers</h3>
                <?php
                $this->widget(
                    'IMenu', array(
                        'items' => Manufacturer::getMenuItems(),
                        'encodeLabel' => false
                    )
                );
                ?>
            </div>
        </div>
    </div>
<?php $this->endContent(); ?>