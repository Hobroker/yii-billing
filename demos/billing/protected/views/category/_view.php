<?php
/* @var $this CategoryController */
/* @var $data Categories */
?>

<div class="col-md-4 col-sm-6">
    <h4>
        <a href="<?= Yii::app()->createUrl('category/' . $data->idcateg); ?>"
           class="thumbnail well no-underline">
            <?= CHtml::encode($data->namecateg) ?>
            <br>
            <small>
                <?=Categories::getNrProductsByCategory($data->idcateg) . (Categories::getNrProductsByCategory($data->idcateg)==1?" product":" products")?>
            </small>
        </a>
    </h4>
</div>