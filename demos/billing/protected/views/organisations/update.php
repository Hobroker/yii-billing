<?php
/* @var $this OrganisationsController */
/* @var $model Organisations */

$this->breadcrumbs=array(
	'Organisations'=>array('index'),
	$model->name=>array('view','id'=>$model->idOrg),
	'Update',
);

$this->menu=array(
	array('label'=>'List Organisations', 'url'=>array('index')),
	array('label'=>'Create Organisations', 'url'=>array('create')),
	array('label'=>'View Organisations', 'url'=>array('view', 'id'=>$model->idOrg)),
	array('label'=>'Manage Organisations', 'url'=>array('admin')),
);
?>

<h2>Update Organisations <?php echo $model->idOrg; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>