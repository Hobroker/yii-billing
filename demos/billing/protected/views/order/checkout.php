<?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Orders',
);

$this->menu = array(
    array('label' => 'Create Order', 'url' => array('create')),
    array('label' => 'Manage Order', 'url' => array('admin')),
);
?>
<h2>Checkout</h2>


<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_minimal',
)); ?>
<div class="col-md-12">
    <div class="pull-right">
        <p>
            Total: <?= Product::productSumByUser(); ?> MDL
        </p>
        <?= CHtml::link('Finish', ['order/finish'], ['class' => 'btn btn-success pull-right']); ?>
    </div>
</div>