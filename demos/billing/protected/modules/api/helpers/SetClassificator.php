<?php

/**
 * Created by PhpStorm.
 * User: urechean
 * Date: 27.05.2015
 * Time: 09:03
 */
class SetClassificator
{

    public static function setData($model_name)
    {
        $arr = array();
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {
                        $model = new $model_name;
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        $arr[] = array("id" => SetClassificator::saveInModel($model));
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function saveInModel($model)
    {
        // Try to save the model
        try {
            if (!$model->save()) {
                // Errors occurred
                $msg = "Error";
                foreach ($model->errors as $attribute => $attr_errors) {
                    $msg .= "Attribute: $attribute";
                    foreach ($attr_errors as $attr_error)
                        $msg .= $attr_error;
                }
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => $msg
                        )
                    )
                );
                return $model->id;
            } else {
                return $model->id;
            }
        } catch (Exception $ex) {
            Response::sendResponse(500,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => $ex->getMessage()
                    )
                )
            );
        }
    }

    public static function setDataByPk($model_name, $id)
    {
        //aici trebuie sa se returneze versiunea aplicatiei
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {//die(var_dump($body_param));
                        $model = $model_name::model()->findByPK($id);
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        SetClassificator::saveInModel($model);
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

}