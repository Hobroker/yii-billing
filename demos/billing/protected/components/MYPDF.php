<?php

/**
 * User: hobroker
 * Date: 4/15/16
 * Time: 12:04 PM
 */
require_once('protected/extensions/tcpdf/tcpdf.php');
Yii::import('application.vendors.*');
//require_once('../extensions/tcpdf/tcpdf.php');
class MYPDF extends TCPDF
{
    // Colored table
    public function ColoredTable($header, $data)
    {
        $this->SetFillColor(66, 139, 202);
        $this->SetTextColor(255);
        $this->SetDrawColor(66,139,202);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        $w = array(55, 42, 43, 15, 25);
        $num_headers = count($header);
        for ($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;
        foreach ($data as $row) {
            $this->Cell($w[0], 6, $row[0], 'LR', 0, 'L', $fill);
            $this->Cell($w[1], 6, $row[1], 'LR', 0, 'L', $fill);
            $this->Cell($w[2], 6, $row[2], 'LR', 0, 'L', $fill);
            $this->Cell($w[3], 6, $row[3], 'LR', 0, 'R', $fill);
            $this->Cell($w[4], 6, $row[4], 'LR', 0, 'R', $fill);
            $this->Ln();
            $fill = !$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}