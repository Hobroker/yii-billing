<?php
$user = User::model()->findByPk($_GET['id']);
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/7/16
 * Time: 2:02 PM
 */
$this->breadcrumbs = array(
    'I-RBAC' => array('index'),
    'Assign role',
);
?>
<h2>Assign role to <b><?= $user->username ?></b></h2>

<div class="container-fluid">
    <div class="row">
        <?php $form = $this->beginWidget('CActiveForm'); ?>
        <div class="col-md-6">
            <div class="form-group">
                <?= IHtml::label('All roles', 'allroles') ?>
                <?php
                $this->widget('zii.widgets.grid.CGridView',
                    [
                        'afterAjaxUpdate' => 'function(){ selectRowsEvent(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => [
                            [
                                'class' => 'ICheckBoxColumn',
                                'selectableRows' => 2,
                                'checkBoxHtmlOptions' => [
                                    'class' => 'row-checkbox',
                                ],
                            ],
                            [
                                'name' => 'name',
                                'type' => 'raw',
                                'value' => 'CHtml::link("$data->name", ["irbac/role/$data->name"])',
                                'htmlOptions' => [
                                    'class' => 'select-name'
                                ]
                            ],
                            'description',
                        ],
                        'enablePagination' => true,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover table-condensed selectable',
                        'pager' => [
                            'class' => 'ILinkPager',
                        ],
                        'htmlOptions' => [
                            'class' => 'table-responsive'
                        ],
                        'pagerCssClass' => 'default',
                    ])
                ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <?= IHtml::label('Assigned roles', 'myroles') ?>
                <?php
                $auth = Yii::app()->authManager;
                $roles = $auth->getRoles($user->id);
                echo "<select id='rolesTo' multiple='multiple' name='roles[]' class='form-control' style='height: 350px'>";
                foreach ($roles as $role) {
                    echo "<option value='$role->name' ";
                    echo ">$role->name</option>'";
                }
                echo "</select>";
                ?>
            </div>
        </div>

        <div class="col-md-6">
            
        </div>

        <div class="col-md-6">
           <div class="row">
               <div class="col-md-3">
                   <?php
                   echo IHtml::button('Remove', [
                       'class' => 'btn btn-danger btn-block',
                       'onclick' => "removeRole()"
                   ]);
                   ?>
               </div>
               <div class="col-md-3 col-md-offset-6">
                   <div class="form-group">
                       <?= CHtml::link('Go to role', ['irbac/task/'], [
                           'class' => 'btn btn-default btn-block roleRedirect',
                           'source' => 'To'
                       ]) ?>
                   </div>
               </div>
           </div>
        </div>

        <div class="col-md-4 buttons">
            <?php echo IHtml::submitButton('Update', [
                'name' => 'assign',
                'onclick' => 'assignSubmit()'
            ]); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>