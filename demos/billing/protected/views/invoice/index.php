<?php
/* @var $this InvoiceController */
/* @var $dataProvider CActiveDataProvider */
$msg = "";
if (isset($_GET['sent'])) {
    if ($_GET['sent'] == '1')
        $msg = "<h4 class='text-success'>Email sent successfully!</h4>";
    elseif ($_GET['sent'] == '0')
        $msg = "<h4 class='text-danger'>Email wasn't sent!</h4>";
}
$this->breadcrumbs = array(
    'Invoices',
);

$this->menu = array(
    array('label' => 'Create Invoice', 'url' => array('create')),
    array('label' => 'Manage Invoice', 'url' => array('admin')),
);
?>
<h1>Invoices</h1>
<?= $msg ?>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_view',
)); ?>
