<?php

/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 5/4/16
 * Time: 9:33 AM
 */
class ApiSaveController extends Controller
{
    public $layout = 'main';

    public function filters()
    {
        return [
            'accessControl',
            'postOnly + delete',
        ];
    }

    public function actionCreate()
    {
        if (isset($_POST['Api'])) {
            $model = new ApiSave();
            $model->attributes = $_POST['Api'];
            $model->params = preg_replace('/\s+/', '', $model->params);
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render("create");
    }

    public function actionIndex()
    {
        $this->render("index");
    }

    public function actionAdmin()
    {
        self::actionIndex();
    }

    public function actionView($id)
    {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        if (isset($_POST['Api'])) {
            $model->attributes = $_POST['Api'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }
        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = ApiSave::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

}