<?php
/* @var $this SaleController */
/* @var $data Sales */
?>
<div class="col-md-4 col-sm-6">
    <h4>
        <a href="<?= Yii::app()->createUrl('sale/' . $data->idsale); ?>"
           class="thumbnail well no-underline">
            <?php echo CHtml::encode($data->valuesale) . Sales::$unit; ?>
            <br>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('startdate')); ?>:</b>
                <?php echo IHtml::idate($data->startdate); ?>
            </small>
            <br>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('enddate')); ?>:</b>
                <?php echo IHtml::idate($data->enddate); ?>
            </small>
        </a>
    </h4>
</div>
