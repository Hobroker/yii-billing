<?php
/* @var $this CategoryController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Categories', 'url'=>array('index')),
	array('label'=>'Manage Categories', 'url'=>array('admin')),
);
?>

<h2>Create Category</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>