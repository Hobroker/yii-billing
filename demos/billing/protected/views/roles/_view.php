<?php
/* @var $this RolesController */
/* @var $data Roles */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idRole')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idRole), array('view', 'id'=>$data->idRole)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nameRole')); ?>:</b>
	<?php echo CHtml::encode($data->nameRole); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permissions')); ?>:</b>
	<?php echo CHtml::encode($data->permissions); ?>
	<br />


</div>