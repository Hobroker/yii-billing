<?php
/* @var $this CategoryController */
/* @var $model Categories */

$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->idcateg=>array('view','id'=>$model->idcateg),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categories', 'url'=>array('index')),
	array('label'=>'Create Categories', 'url'=>array('create')),
	array('label'=>'View Categories', 'url'=>array('view', 'id'=>$model->idcateg)),
	array('label'=>'Manage Categories', 'url'=>array('admin')),
);
?>

<h2>Update Categories <?php echo $model->idcateg; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>