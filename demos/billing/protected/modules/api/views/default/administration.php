d<!--div class="jumbotron">
    <h1>Import Export</h1>
    <p>An module bla bla bla...</p>
</div-->

<?php
/*$this->menu=array(
    array('label'=>'Administrare modul', 'icon'=>'flag', 'url'=>array("/{$this->module->id}/default/administration")),
    array('label'=>Yii::t('t_views','Configuraţii'), 'url'=>array("/{$this->module->id}/config/admin")),
    array('label'=>Yii::t('t_views','Generare Acţiuni Diferenţial'), 'url'=>array("/{$this->module->id}/default/generateDiffActions")),
    array('label'=>Yii::t('t_views','Regenerare Acţiuni'), 'url'=>array("/DynamicMenu/default/generateAllActions")),
    array('label'=>Yii::t('t_views','Administrează Acţiuni'),'icon'=>'list', 'url'=>array("/{$this->module->id}/default/manageAllActions")),
);*/
?>


<!--div class="jumbotron">
    <h1>DynamicMenu generator</h1>
    <p>An module bla bla bla...</p>
</div-->


<?php $this->breadcrumbs = array(
    $this->module->id
); ?>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Mai</span> <span>2015</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-play"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Urechean Emil</a></span>
                </div>
                <div class="timeline-content">
                    <h3>REST Api</h3>
                    <p>Modulul dat reprezintă server-ul REST Api al aplicației.</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Actiunile principale ale Modulului Api</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <div class="col-md-6">
                                <?php $this->widget('application.components.widgets.usertheme.AvantTiles', array(
                                    'type' => 'shortcut',
                                    'color' => 'info',
                                    'icon' => 'download',
                                    'footer' => 'Exportare Date',
                                    'href' => Yii::app()->createUrl("/{$this->module->id}/default/export")
                                )); ?>
                            </div>
                            <div class="col-md-6">
                                <?php $this->widget('application.components.widgets.usertheme.AvantTiles', array(
                                    'type' => 'shortcut',
                                    'color' => 'inverse',
                                    'icon' => 'upload',
                                    'footer' => 'Importare Date',
                                    'href' => Yii::app()->createUrl("/{$this->module->id}/default/import")
                                )); ?>
                            </div>

                            <div class="col-md-6">
                                <?php $this->widget('application.components.widgets.usertheme.AvantTiles', array(
                                    'type' => 'shortcut',
                                    'color' => 'danger',
                                    'icon' => 'list',
                                    'footer' => 'Saved APIs',
                                    'href' => Yii::app()->createUrl("/{$this->module->id}/apiSave/create")
                                )); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>