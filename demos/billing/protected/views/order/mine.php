<?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'My cart',
);

$this->menu = array(
    array('label' => 'Create Order', 'url' => array('create')),
    array('label' => 'Manage Order', 'url' => array('admin')),
);
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h2>My cart</h2>
        </div>
    </div>

    <?php if (Product::productSumByUser() > 0): ?>
        <h4>Total: <?= Product::productSumByUser(); ?> MDL</h4>
        <div class="row">
            <?php $this->widget('zii.widgets.CListView', array(
                'dataProvider' => $dataProvider,
                'itemView' => '_view',
            )); ?>
        </div>
        <?= CHtml::button('Checkout',
            [
                'submit' => ['order/checkout'],
                'class' => 'btn btn-success pull-right'
            ]
        ); ?>
    <?php else: ?>
        <p>Is empty</p>
    <?php endif; ?>

</div>