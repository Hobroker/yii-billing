<?php
/* @var $this ProdsaleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Prod Sales',
);

$this->menu = array(
    array('label' => 'Create Product Sales', 'url' => array('create')),
    array('label' => 'Manage Product Sales', 'url' => array('admin')),
);
?>

<h2>Product Sales</h2>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('Sales'),
    'itemView' => '_view',
    'summaryText' => ''
)); ?>
