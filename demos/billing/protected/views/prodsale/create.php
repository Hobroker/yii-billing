<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */

$this->breadcrumbs = array(
    'Product Sales' => array('index'),
    'Create',
);

$this->menu = array(
    array('label' => 'List Product Sales', 'url' => array('index')),
    array('label' => 'Manage Product Sales', 'url' => array('admin')),
);
?>

    <h2>Create Product Sale</h2>

<?php $this->renderPartial('_form', array('model' => $model)); ?>