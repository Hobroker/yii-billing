<?php
$module_name = basename(dirname(dirname(__FILE__)));

return array(
    'aliases' => array(
        'Api' => 'application.modules.' . $module_name
    ),
    'import' => array(
        'application.modules.' . $module_name . '.*',
        'application.modules.' . $module_name . '.models.*',
        'application.modules.' . $module_name . '.helpers.*',
    ),

    'modules' => array(
        $module_name => array(
            'front_tiles' => array(
                'icon' => 'unsorted',
                'color' => 'sky',
            )
        ),
    ),
    'params' => array(
        'folders' => array(
            'application.modules.Api.tmp' => '0777',
        )
    )
);
?>
