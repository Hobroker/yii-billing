<?php
/* @var $this CategoryController */
/* @var $model Categories */

$this->breadcrumbs = array(
    'Categories' => array('index'),
    $model->namecateg,
);

$this->menu = array(
    array('label' => 'List Categories', 'url' => array('index')),
    array('label' => 'Create Categories', 'url' => array('create')),
    array('label' => 'Update Categories', 'url' => array('update', 'id' => $model->idcateg)),
    array('label' => 'Delete Categories', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idcateg), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Categories', 'url' => array('admin')),
);
?>

<h2><?php echo $model->namecateg; ?></h2>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('Product', [
        'criteria' => [
            'condition' => 'idcateg=:idcateg',
            'params' => [
                ':idcateg' => $model->idcateg
            ],
            'order' => 'nameprod ASC'
        ]
    ]),
    'itemView' => '../product/_view',
    'htmlOptions' => [
        'class' => 'row'
    ],
    'summaryCssClass' => 'container-fluid text-right'
)); ?>
