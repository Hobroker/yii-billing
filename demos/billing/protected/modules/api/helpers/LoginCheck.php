<?php

class LoginCheck
{
    // {{{ _checkAuth
    /**
     * Checks if a request is authorized
     *
     * @access private
     * @return void
     */
    public static function checkAuth()
    {
//        $_SERVER['PHP_AUTH_USER'] = null;
        if (!isset($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="API Authentication"');
            header('HTTP/1.0 401 Unauthorized');
            die("Not authorized!");
        } else {
            $login = strtolower($_SERVER['PHP_AUTH_USER']);
            $password = $_SERVER['PHP_AUTH_PW'];
        }
        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), false);
        $user = User::model()->find($criteria);
        if (is_null($user)) {
            self::destroyAuth();
        }
        //var_dump($user);
        if (isset($user) && !is_null($user) && $user->status_id != 0) {
            if (isset($user->ad_username) && !is_null($user->ad_username) && !empty($user->ad_username)) {
                $uldap_relation = UserLdapRelation::model()->findByAttributes(array('user_id' => $user->id));
//die("DS");
                if ($uldap_relation === null) {
                    Response::sendResponse(401);
                } else {
                    $ldap = LdapSettings::model()->findByPk($uldap_relation->ldap_setting_id);

                    if ($ldap === null) {
                        Response::sendResponse(401);
                    } else {
                        $dc_first = explode('.', $ldap->ldap_dc)[0];
                        if (!LoginCheck::bindLdap($ldap->ldap_host, $ldap->ldap_port, $user->ad_username, $password, $dc_first)) {
                            Response::sendResponse(401);
                        } else {
                            $identity = new  AdUserIdentity($user->ad_username, $password);
                            $identity->authenticate();
                            if (!$identity->errorCode) {
                                Yii::app()->user->login($identity, 3600 * 24 * 7);
                                Yii::app()->user->setState('username', $user->username);
                            }

                        }
                    }
                }
            } else {
//                die("DSA");
                if (!$user->validatePassword($password)) {
//                    die("DSA");
                    Response::sendResponse(401);
                } else {
//                    die("DSA");
                    $loginForm = new LoginForm;
                    $loginForm->username = $user->username;
                    $loginForm->password = $password;
                    $loginForm->loginMethod = 1;
                    $loginForm->login();
                }
            }
        } else
            self::destroyAuth();
        if (strpos(Yii::app()->request->pathinfo, '/') !== false || strlen(Yii::app()->request->pathinfo) <= 43)
            $pathinfo = Yii::app()->request->pathinfo;
        else {
            $pathinfo = Yii::app()->getSecurityManager()->decrypt(base64_decode(strtr(Yii::app()->request->pathinfo, '-_,', '+/=')));
        }
        $arr_pathinfo = explode("/", $pathinfo);
        if (isset(Yii::app()->modules['User']['modules']['Rbam'])) {     // return true;

            $module_name_rbam = '';
            if (isset($arr_pathinfo[0])): $module_name_rbam = $arr_pathinfo[0] . ':'; endif;
//die(var_dump($module_name_rbam . ucfirst($arr_pathinfo[1]) . ':' . ucfirst($arr_pathinfo[2])));
//            die(var_dump(Yii::app()->user->checkAccess($module_name_rbam . ucfirst($arr_pathinfo[1]) . ':' . ucfirst($arr_pathinfo[2]))));
//            if (!Yii::app()->user->checkAccess($module_name_rbam . ucfirst($arr_pathinfo[1]) . ':' . ucfirst($arr_pathinfo[2])))
//                Response::sendResponse(403);
        }

    }

    private static function bindLdap($host, $port, $username, $password, $dc)
    {
        if (!strpos($username, '\\'))
            $username = $dc . '\\' . $username;

        try {

            // connect to ldap server
            $ldapconn = @ldap_connect($host, $port)
            or die("Could not connect to LDAP server.");

            // Set some ldap options for talking to
            ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);

            try {
                if ($ldapconn) {

                    // binding to ldap server
                    $ldapbind = @ldap_bind($ldapconn, $username, $password);

                    // verify binding
                    if ($ldapbind) {
                        //echo "LDAP bind successful...\n";
                        return true;
                    } else {
                        //echo "LDAP bind failed...\n";
                        return false;
                    }

                }
            } catch (ErrorException $ex) {
                echo Message("Mesaj :" . $ex->getMessage());
            }
        } catch (ErrorException $ex) {
            echo Message("Mesaj :" . $ex->getMessage());
        }
    }

    public static function checkUser()
    {
//        die(var_dump($_SERVER['PHP_AUTH_USER']));
        if (!isset($_SERVER['PHP_AUTH_USER']) || is_null($_SERVER['PHP_AUTH_USER'])) {
            header('WWW-Authenticate: Basic realm="Authentication"');
            header('HTTP/1.0 401 Unauthorized');
            die("Not authorized!");
        }
        $login = strtolower($_SERVER['PHP_AUTH_USER']);
        $password = $_SERVER['PHP_AUTH_PW'];

        $criteria = new CDbCriteria;

        $criteria->compare('upper(username)', strtoupper($login), true);
        $user = User::model()->find($criteria);
        if (is_null($user)) {
            self::destroyAuth();
        }
        if (isset($user) && !is_null($user) && $user->status_id != 0) {
            if (isset($user->ad_username) && !is_null($user->ad_username) && !empty($user->ad_username)) {
                $uldap_relation = UserLdapRelation::model()->findByAttributes(array('user_id' => $user->id));

                if ($uldap_relation === null) {
                    Response::sendResponse(401);
                } else {
                    $ldap = LdapSettings::model()->findByPk($uldap_relation->ldap_setting_id);

                    if ($ldap === null) {
                        Response::sendResponse(401);
                    } else {
                        $dc_first = explode('.', $ldap->ldap_dc)[0];
                        if (!LoginCheck::bindLdap($ldap->ldap_host, $ldap->ldap_port, $user->ad_username, $password, $dc_first)) {
                            Response::sendResponse(401);
                        } else {
                            $identity = new  AdUserIdentity($user->ad_username, $password);
                            $identity->authenticate();
                            if (!$identity->errorCode) {
                                Yii::app()->user->login($identity, 24);
                                $arr = array();
                                $arr["id"] = $user->id;
                                $arr["status"] = "true";
                                $arr["name_surname"] = $user->profile->firstname . " " . $user->profile->lastname;
                                $arr["last_login"] = date("m.d.y H:i");
                                return $arr;
                            }
                        }
                    }
                }
            } else
                if (!$user->validatePassword($password)) {
                    Response::sendResponse(401);
                } else {
                    $loginForm = new LoginForm;
                    $loginForm->username = $login;
                    $loginForm->password = $password;
                    $loginForm->loginMethod = 1;
                    $loginForm->login();
                    $arr = array();
                    $arr["id"] = (string)$user->id;
                    $arr["status"] = "true";
                    $arr["name_surname"] = $user->profile->firstname . " " . $user->profile->lastname;
                    $arr["last_login"] = date("m.d.y H:i");
                    return $arr;
                }
        }
        self::destroyAuth();
    }

    private static function destroyAuth()
    {
        $_SERVER['PHP_AUTH_USER'] = null;
        MyHelper::send404("Unauthorised");
    }

    /*
     * @return bool if bind ldap
     */

    public static function userList()
    {
        $users = User::model()->findAll();
        $arr = array();
        foreach ($users as $user) {
            $arr1 = array();
            $arr1["id"] = $user->id;
            $arr1["name_surname"] = $user->profile->firstname . " " . $user->profile->lastname;
            $arr[] = $arr1;
        }
        return $arr;
    }
}