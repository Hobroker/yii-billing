<?php
$auth = Yii::app()->authManager;
$this->breadcrumbs = array(
    'I-RBAC',
);
?>
<div class="container-fluid">
    <div class="row">
        <!--<div class="col-md-4">
            <h2>Operations
                <small><br><? /*= CHtml::link('Add manually', ['irbac/operation']) */ ?></small>
                <small><? /*= CHtml::link('Add automatically', ['irbac/operation/refresh']) */ ?></small>
            </h2>
            <ul class="list-group">
                <?php
        /*                $roles = $auth->getOperations();
                        foreach ($roles as $operation) {
                            echo '<li class="list-group-item">' . $operation->name . '</li>';
                        }
                        */ ?>
            </ul>
        </div>-->
        <div class="col-md-4">
            <h2>
                Tasks
                <small><?= CHtml::link('Add', ['irbac/task']) ?></small>
            </h2>
            <ul class="list-group">
                <?php
                $tasks = $auth->getTasks();
                foreach ($tasks as $task) {
                    echo CHtml::link($task->name, ["irbac/task/$task->name"], ['class' => 'list-group-item']);
                }
                ?>
            </ul>
        </div>
        <div class="col-md-4">
            <h2>Roles
                <small><?= CHtml::link('Add', ['irbac/role']) ?></small>
            </h2>
            <ul class="list-group">
                <?php
                $roles = $auth->getRoles();
                foreach ($roles as $role) {
                    echo CHtml::link($role->name, ["irbac/role/$role->name"], ['class' => 'list-group-item']);
                }
                ?>
            </ul>
        </div>
        <div class="col-md-4">
            <h2>Assignments</h2>
            <ul class="list-group">
                <?php
                $users = User::model()->findAll();
                foreach ($users as $user) {
                    $arr = [];
                    $assignments = Authassignment::model()->findAll([
                        'condition' => 'userid=:id',
                        'params' => [
                            'id' => $user->id
                        ]
                    ]);
                    foreach ($assignments as $assignment) {
                        $arr[] = $assignment->itemname;
                    }
                    $allRoles = implode(', ', $arr);
                    echo CHtml::link("<b>$user->username</b>: $allRoles", ['irbac/user/' . $user->id], ['class' => 'list-group-item']);
                }
                ?>
            </ul>
        </div>
    </div>
</div>