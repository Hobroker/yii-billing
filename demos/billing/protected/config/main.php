<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Yii Billing',
    'theme' => 'bootstrap',

    // preloading 'log' component
    'preload' => array('log'),

    // autoloading model and component classes
    'import' => array(
        'application.models.*',
        'application.components.*',
    ),

    'modules' => array(
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            // 'user'=>
            'ipFilters' => ['*'],
            'password' => 'admin',
            'generatorPaths' => ['application.gii']
        ),
        'api'
    ),

    'defaultController' => 'post',

    // application components
    'components' => array(
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'db' => array(
            'tablePrefix' => 'tbl_',
            'connectionString' => 'pgsql:host=localhost;port=5432;dbname=billing;',
            'username' => 'postgres',
            'password' => 'postgres',
            'charset' => 'UTF8',
        ),
        'authManager' => array(
            'class' => 'CDbAuthManager',
            'connectionID' => 'db',
            'itemTable' => 'authitem',
            'itemChildTable' => 'authitemchild',
            'assignmentTable' => 'authassignment',
            'defaultRoles' => array('Guest'),
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'urlManager' => array(
            'urlFormat' => 'path',
            'rules' => array(
                '' => 'product/index',
                'category/<id:\d+>' => 'category/view',
                'product/<id:\d+>' => 'product/view',
                'manufacturer/<id:\d+>' => 'manufacturer/view',
                'invoice/<id:\d+>' => 'invoice/view',
                'login' => 'site/login',
                'register' => 'user/register',
                'organisations/<id:\d+>' => 'organisations/view',
                'sale/<id:\d+>' => 'sale/view',
                'prodsale/<id:\d+>' => 'prodsale/view',
                'user/<id:\d+>' => 'user/view',
                'irbac/user/<id:\d+>' => 'irbac/user',
                'irbac/role/<name:.*?>' => 'irbac/role',
                'irbac/task/<name:.*?>' => 'irbac/task',
                'irbac/operation/<name:.*?>' => 'irbac/operation',
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'posts/<tag:.*?>' => 'post/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'gii' => 'gii',
                'gii/<controller:\w+>' => 'gii/<controller>',
                'gii/<controller:\w+>/<action:\w+>' => 'gii/<controller>/<action>',
            ),
            'showScriptName' => false
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),
        'mail' => array(
            'class' => 'ext.yii-mail.YiiMailMessage',
            'transportType' => 'smtp',
            'viewPath' => 'application.views.mail',
            'logging' => true,
            'dryRun' => false,
            'transportOptions' => array(
                'host' => 'smtp.gmail.com',
                'username' => 'stajor.it@gmail.com',
                'password' => '123qweASDZXC',
                'port' => '25',
            ),
        ),
    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => require(dirname(__FILE__) . '/params.php'),

);