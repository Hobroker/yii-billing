<?php

/**
 * Created by PhpStorm.
 * User: urechean
 * Date: 27.05.2015
 * Time: 09:03
 */
class TaskHelperApi
{
    public static function getAllTask()
    {
        if (!isset($_GET['login']))
            Response::sendResponse(203,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => "Parameter <b>login</b> is missing !"
                    )
                )
            );
        else
            $login = strtolower($_GET['login']);

        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), true);
        $user_api = User::model()->find($criteria);

        $criteria = new CDbCriteria(array('order' => 'start_time DESC'));
        $criteria->condition = "t.to = '$user_api->id' OR t.from = '$user_api->id'";
        $in = array(Task::TASK_STATUS_ACCEPTED, Task::TASK_STATUS_UNREADED, Task::TASK_STATUS_READED);
        $criteria->addInCondition('status_id', $in);
        $models = Task::model()->findAll($criteria);

        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr[] = $arr_elements;
        };
        return $arr;
    }

    public static function getAllTaskDocumentsByPk($id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "t.task_id = $id";
        $models = TaskAttached::model()->findAll($criteria);
        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr_elements["document_name"] = Document::model()->findByPk($arr_elements["document_id"])->title . "." .
                Document::model()->findByPk($arr_elements["document_id"])->fileFormat->extension;
            $arr_elements["model_type"] = Document::model()->findByPk($arr_elements["document_id"])->type->instance_model_name;
            $arr[] = $arr_elements;
        }
        return $arr;
    }

    public static function getAllTaskDocuments()
    {
        $models = TaskAttached::model()->findAll();
        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr_elements["document_name"] = Document::model()->findByPk($arr_elements["document_id"])->title . "." .
                Document::model()->findByPk($arr_elements["document_id"])->fileFormat->extension;
            $arr_elements["model_type"] = Document::model()->findByPk($arr_elements["document_id"])->type->instance_model_name;
            $arr[] = $arr_elements;
        }
        return $arr;
        /*foreach($models as $model)
        {
            $arr_elements = array();
            $arr_elements["id"] = $model->id;
            $arr_elements["create_time"] = $model->create_time;
            $arr_elements["create_user_id"] = $model->user_id;
            $arr_elements["task_id"] = $model->task_id;
            $doc = Document::model()->findByPk($model->document_id);
            $arr_elements["document_link"] = HelpersStorage::GetOperativPath($doc->documentType->docCategory->storage).DIRECTORY_SEPARATOR.
                $doc->type->storageRoute->title.DIRECTORY_SEPARATOR.$doc->file;
            $arr[] = $arr_elements;
        }
        die(var_dump($arr));*/
    }

    public static function getTaskDocumentByPk($id)
    {
        $model = Document::model()->findByPk($id);

        if (isset($model)) {
            $doc = HelpersStorage::GetOperativPath($model->documentType->docCategory->storage) . DIRECTORY_SEPARATOR .
                $model->type->storageRoute->title . DIRECTORY_SEPARATOR . $model->file;

            if (file_exists($doc)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($doc));
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($doc));
                readfile($doc);
                exit;
            }
        } else {
            Response::sendResponse(401);
        }
    }

    public static function getCommitDataByPk($id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "t.task_id = $id";
        $models = TaskComments::model()->findAll($criteria);

        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr[] = $arr_elements;
        };
        return $arr;
    }

    public static function setData()
    {
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {//die(var_dump($body_param));
                        $model = new Task;
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        $arr = array("id" => TaskHelperApi::saveInModel($model));
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function saveInModel($model)
    {
        // Try to save the model
        try {
            if (!$model->save()) {
                // Errors occurred
                $msg = "Error";
                foreach ($model->errors as $attribute => $attr_errors) {
                    $msg .= "Attribute: $attribute";
                    foreach ($attr_errors as $attr_error)
                        $msg .= $attr_error;
                }
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => $msg
                        )
                    )
                );
            }
            return $model->id;
        } catch (Exception $ex) {
            Response::sendResponse(500,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => $ex->getMessage()
                    )
                )
            );
        }
    }

    public static function setCommitData()
    {
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {//die(var_dump($body_param));
                        $model = new TaskComments();
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        $arr = array("id" => TaskHelperApi::saveInModel($model));
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function setDataByPk($id)
    {
        //aici trebuie sa se returneze versiunea aplicatiei
        $arr = array("Error" => "No");
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    foreach ($body_params as $key => $body_param) {//die(var_dump($body_param));
                        $model = Task::model()->findByPK($id);
                        foreach ($body_param as $param_name => $param_value) {
                            $model[$param_name] = $param_value;
                        }
                        TaskHelperApi::saveInModel($model);
                    }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        return $arr;
    }

    public static function getDataByAttributes()
    {
        if (!isset($_GET['login']))
            Response::sendResponse(203,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => "Parameter <b>login</b> is missing !"
                    )
                )
            );
        else
            $login = strtolower($_GET['login']);

        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), true);
        $user_api = User::model()->find($criteria);

        $criteria = new CDbCriteria;

        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    $key_val = 97;
                foreach ($body_params as $key => $body_param) {
                    $elements = array();

                    foreach ($body_param as $param_name => $param_value) {
                        $elements[$param_name] = $param_value;
                    }

                    $criteria->addCondition("t." . $elements["column"] . " " . $elements["condition"] . " :" . chr($key_val));
                    $criteria->addCondition("t.to = '$user_api->id' OR t.from = '$user_api->id'");
                    $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $elements["value"]));
                    $in = array(Task::TASK_STATUS_ACCEPTED, Task::TASK_STATUS_UNREADED, Task::TASK_STATUS_READED);
                    $criteria->addInCondition('status_id', $in);
                    $key_val = $key_val + 1;
                }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }

        $models = Task::model()->findAll($criteria);
        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr[] = $arr_elements;
        }
        return $arr;
    }
}