<?php

/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/12/16
 * Time: 10:59 PM
 */
class ILinkPager extends CLinkPager
{
    const CSS_SELECTED_PAGE = 'active';
    public $selectedPageCssClass = self::CSS_SELECTED_PAGE;
    public $htmlOptions = [
        'class' => 'pagination pagination-center center-block'
    ];
    public $firstPageLabel = "<span aria-hidden=\"true\">&laquo;</span>";
    public $lastPageLabel = "<span aria-hidden=\"true\">&raquo;</span>";
    public $nextPageLabel = "<span aria-hidden=\"true\">&gt;</span>";
    public $prevPageLabel = "<span aria-hidden=\"true\">&lt;</span>";
    public $maxButtonCount = 15;
    public $header = '';
}