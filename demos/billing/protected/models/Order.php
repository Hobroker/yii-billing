<?php

/**
 * This is the model class for table "{{orders}}".
 *
 * The followings are the available columns in table '{{orders}}':
 * @property integer $idcpi
 * @property integer $iduser
 * @property integer $idprod
 * @property string $date
 * @property integer $idinv
 *
 * The followings are the available model relations:
 * @property User $iduser0
 * @property Products $idprod0
 * @property Invoices $idinv0
 */
class Order extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{orders}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('iduser, idprod, idinv', 'numerical', 'integerOnly' => true),
            array('date', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idcpi, iduser, idprod, date, idinv', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'iduser0' => array(self::BELONGS_TO, 'User', 'iduser'),
            'idprod0' => array(self::BELONGS_TO, 'Product', 'idprod'),
            'idinv0' => array(self::BELONGS_TO, 'Invoices', 'idinv'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idcpi' => 'ID',
            'iduser' => 'User',
            'idprod' => 'Product',
            'date' => 'Date',
            'idinv' => 'Invoice',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idcpi', $this->idcpi);
        $criteria->compare('iduser', $this->iduser);
        $criteria->compare('idprod', $this->idprod);
        $criteria->compare('date', $this->date, true);
        $criteria->compare('idinv', $this->idinv);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function userInvoices()
    {
        $products = Order::model()->findAllByAttributes(array('iduser' => Yii::app()->User->id));
        $count = [];
        foreach ($products as $product) {
            if (isset($product->idinv) && !in_array($product->idinv, $count))
                $count[] = $product->idinv;
        }
        return count($count);
    }

    public static function getNrCartProducts()
    {
        return Order::model()->count(
            'iduser=:iduser AND idinv IS NULL',
            [
                ':iduser' => Yii::app()->User->id
            ]
        );
    }


    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Order the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
