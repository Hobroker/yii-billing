<?php

/**
 * This is the model class for table "{{invoices}}".
 *
 * The followings are the available columns in table '{{invoices}}':
 * @property integer $idinv
 * @property string $aprobat
 *
 * The followings are the available model relations:
 * @property Orders[] $orders
 */
class Invoice extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{invoices}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('aprobat', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idinv, aprobat', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'orders' => array(self::HAS_MANY, 'Order', 'idinv'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idinv' => 'ID',
            'aprobat' => 'Aprobat',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idinv', $this->idinv);
        $criteria->compare('aprobat', $this->aprobat, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getUserIDByInvoice($id)
    {
        $order = Order::model()->findByAttributes(['idinv' => $id]);
        if ($order != null)
            return $order->iduser;
        return 1;
    }

    public static function getTotalPriceByInvoice($id)
    {
        $orders = Order::model()->findAllByAttributes(['idinv' => $id]);
        $total = 0;
        foreach ($orders as $order) {
            $product = Product::model()->findByPk($order->idprod);
            $total += Product::productPriceWithSale($product->idprod);
        }
        return $total;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Invoice the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getInvoiceHTML($id)
    {
        return file_get_contents(Yii::app()->createAbsoluteUrl('invoice/export', ['id' => $id]));
    }

    public static function getInvoiceExport($id)
    {
        $data = "";
        $idinv = 199;
        $total = 0;
        $orders = Order::model()->findAll([
            'condition' => 'idinv=:idinv',
            'params' => [
                ':idinv' => $idinv
            ]
        ]);

        $data .= "
<table border='1'>
    <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Manufacturer</th>
        <th>Sale</th>
        <th>Price</th>
    </tr>
        ";
        foreach ($orders as $order) {
            $product = Product::model()->findByPk($order->idprod);
            $total += $product->priceprod;
            $data .= "<tr>";
            $data .= "<td>$product->nameprod</td>";
            $data .= "<td>" . $product->idcateg0->namecateg . "</td>";
            $data .= "<td>" . $product->idmanufacturer0->name . "</td>";

            $sale = Product::productSale($product->idprod);

            $data .= "<td>" . ($sale == 0 ? '' : $sale . Sales::$unit) . "</td>";
            $data .= "<td>" . Product::productPriceWithSale($product->idprod) . Product::$currency . "</td>";
            $data .= "</tr>";
        }
        $data .= "
</table>
    <p align='right'>
        Total: $total " . Product::$currency . "
    </p>

        ";
        return $data;
    }

    public static function getInvoiceData($idinv)
    {
        $orders = Order::model()->findAll([
            'condition' => 'idinv=:idinv',
            'params' => [
                ':idinv' => $idinv
            ]
        ]);
        $data = [];
        foreach ($orders as $order) {
            $product = Product::model()->findByPk($order->idprod);
            $sale = Product::productSale($product->idprod);
            $data[] = [
                $product->nameprod,
                $product->idcateg0->namecateg,
                $product->idmanufacturer0->name,
                ($sale == 0 ? '' : $sale . Sales::$unit),
                Product::productPriceWithSale($product->idprod) . Product::$currency
            ];
        }
        return $data;
    }

    public static function getUserByInvoice($idinv)
    {
        return Order::model()->find([
            'condition' => 't.idinv=:idinv',
            'params' => [
                ':idinv' => $idinv
            ]
        ])->iduser0;
    }

    public static function isApproved($id)
    {
        if (Invoice::model()->findByAttributes(["idinv" => $id])->aprobat == 1)
            return true;
        return false;
    }
    
    public static function generatePDF($id) {
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Igor Leahu');
        $pdf->SetTitle("Invoice #$id");
        $pdf->SetSubject('Order summary');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');
        $user = Invoice::getUserByInvoice($id);
        $pdf->setHeaderData(null, null, "Yii Billing", $user->fname . " " . $user->lname);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->setHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->setFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('helvetica', '', 12);
        $pdf->AddPage();
        $header = ['Name', 'Category', 'Manufacturer', 'Sale', 'Price'];
        $data = Invoice::getInvoiceData($id);
        $pdf->writeHTML("<h1>Invoice #$id</h1><br>", true, 0, true, true, 'C');
        $pdf->ColoredTable($header, $data);
        $pdf->Ln();
        $pdf->writeHTML("<h2>Total: " . Invoice::getTotalPriceByInvoice($id) . Product::$currency . "</h2><br>", true, 0, true, true, 'R');
        
        return $pdf;
    }
}
