<?php
/* @var $this InvoiceController */
/* @var $data Invoice */
$user = Invoice::getUserByInvoice($data->idinv);
?>

<div class="col-md-6">
    <div class="thumbnail well">
        <?php if (Invoice::isApproved($data->idinv)): ?>
            <i>Invoice approved</i>
        <?php else: ?>
            <?= CHtml::link("Approve",
                [
                    'invoice/approve',
                    'id' => $data->idinv
                ], [
                    'class' => 'btn btn-primary pull-right btn-sm',
                    'confirm' => 'Approve this invoice?'
                ]) ?>
            <i>Invoice is not approved yet!</i>
        <?php endif; ?>
        <br/>
        <b><?php echo CHtml::encode($data->getAttributeLabel('idinv')); ?>:</b>
        <?php echo CHtml::link(CHtml::encode($data->idinv), array('view', 'id' => $data->idinv)); ?>
        <br/>

        <b>Client:</b>
        <?= $user->fname . " " . $user->lname; ?>
        <br/>

        <b>Email:</b>
        <?= $user->email ?>
        <br/>

        <b>Total price:</b>
        <?php echo Invoice::getTotalPriceByInvoice($data->idinv); ?> MDL
        <br>

        <?php echo CHtml::link("Export to PDF", ['export', 'id' => $data->idinv]); ?>

    </div>
</div>