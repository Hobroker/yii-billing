<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */

$this->breadcrumbs = array(
    'Product Sales' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List Product Sales', 'url' => array('index')),
    array('label' => 'Create Product Sales', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prod-sale-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Product Sales</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
        &lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php $this->renderPartial('_search', array(
        'model' => $model,
    )); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'prod-sale-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
//        'idps',
        [
            'name' => 'idprod',
            'value' => '$data->idprod0->idcateg0->namecateg . " " . $data->idprod0->idmanufacturer0->name . " " . $data->idprod0->nameprod'
        ],
        [
            'name' => 'idsale',
            'value' => '$data->idsale0->valuesale."%"'
        ],
        [
            'name' => 'idsale',
            'value' => 'CHtml::encode(Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->idsale0->startdate, \'yyyy-MM-dd\'),\'medium\',null))',
            'header'=>'Start date'
        ],
        [
            'name' => 'idsale',
            'value' => 'CHtml::encode(Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->idsale0->enddate, \'yyyy-MM-dd\'),\'medium\',null))',
            'header'=>'End date'
        ],
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>
