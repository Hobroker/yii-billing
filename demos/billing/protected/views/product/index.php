<?php
/* @var $this ProductController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Products',
);

$this->menu = array(
    ['label' => 'Create Product', 'url' => array('create')],
    ['label' => 'Manage Product', 'url' => array('admin')],
)
?>

<p>
    <?= isset($_GET['new']) ? 'The product was added to cart! <br> You can continue shopping or go to ' . CHtml::link('checkout', ['order/mine']) : "" ?>
</p>

<h2>All products</h2>

<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,    
    'itemView' => '_view',
    'htmlOptions' => [
        'class' => 'row'
    ],
    'summaryCssClass' => 'container-fluid text-right'
)); ?>
