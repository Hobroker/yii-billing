<?php

/**
 * This is the model class for table "cl_test_cl_api".
 *
 * The followings are the available columns in table 'cl_test_cl_api':
 * @property string $id
 * @property string $cimp1
 * @property integer $cimp2
 * @property string $cimp3
 * @property string $cimp4
 * @property double $cimp5
 * @property string $cimp8
 * @property string $cimp9
 * @property string $cimp10
 * @property string $cimp11
 * @property integer $cimp12
 * @property string $cimp13
 * @property string $cimp14
 * @property string $cimp15
 * @property string $create_user_id
 * @property string $create_datetime
 * @property string $update_user_id
 * @property string $update_datetime
 */
class TestClApi extends GeneralClasificator
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return TestClApi the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'cl_test_cl_api';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('cimp2, cimp12', 'numerical', 'integerOnly' => true),
            array('cimp5', 'numerical'),
            array('cimp1', 'length', 'max' => 150),
            array('cimp9, cimp11', 'length', 'max' => 100),
            array('cimp3, cimp4, cimp8, cimp10, cimp13, cimp14, cimp15, create_user_id, create_datetime, update_user_id, update_datetime', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, cimp1, cimp2, cimp3, cimp4, cimp5, cimp8, cimp9, cimp10, cimp11, cimp12, cimp13, cimp14, cimp15, create_user_id, create_datetime, update_user_id, update_datetime', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'cimp1' => 'Cimp1',
            'cimp2' => 'Cimp2',
            'cimp3' => 'Cimp3',
            'cimp4' => 'Cimp4',
            'cimp5' => 'Cimp5',
            'cimp8' => 'Cimp8',
            'cimp9' => 'Cimp9',
            'cimp10' => 'Cimp10',
            'cimp11' => 'Cimp11',
            'cimp12' => 'Cimp12',
            'cimp13' => 'Cimp13',
            'cimp14' => 'Cimp14',
            'cimp15' => 'Cimp15',
            'create_user_id' => 'Create User',
            'create_datetime' => 'Create Datetime',
            'update_user_id' => 'Update User',
            'update_datetime' => 'Update Datetime',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('cimp1', $this->cimp1, true);
        $criteria->compare('cimp2', $this->cimp2);
        $criteria->compare('cimp3', $this->cimp3, true);
        $criteria->compare('cimp4', $this->cimp4, true);
        $criteria->compare('cimp5', $this->cimp5);
        $criteria->compare('cimp8', $this->cimp8, true);
        $criteria->compare('cimp9', $this->cimp9, true);
        $criteria->compare('cimp10', $this->cimp10, true);
        $criteria->compare('cimp11', $this->cimp11, true);
        $criteria->compare('cimp12', $this->cimp12);
        $criteria->compare('cimp13', $this->cimp13, true);
        $criteria->compare('cimp14', $this->cimp14, true);
        $criteria->compare('cimp15', $this->cimp15, true);
        $criteria->compare('create_user_id', $this->create_user_id, true);
        $criteria->compare('create_datetime', $this->create_datetime, true);
        $criteria->compare('update_user_id', $this->update_user_id, true);
        $criteria->compare('update_datetime', $this->update_datetime, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function formAttributes()
    {

        return array(
            'cimp15' => array('type' => 'DateTime', 'data_type' => 'DATETIME', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp14' => array('type' => 'DateTime', 'data_type' => 'DATE', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp10' => array('type' => 'DropDownListBox', 'data' => 'FormDatacimp10', 'from' => 'func', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp13' => array('type' => 'CheckBoxList', 'data' => 'FormDatacimp13', 'from' => 'func', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp9' => array('type' => 'DropDownList', 'data' => 'FormDatacimp9', 'from' => 'func', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp11' => array('type' => 'RadioButtonList', 'data' => 'FormDatacimp11', 'from' => 'func', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp5' => array('type' => 'NumberField', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp12' => array('type' => 'CheckBox', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp8' => array('type' => 'TextArea', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp3' => array('type' => 'NumberField', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp4' => array('type' => 'NumberField', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp1' => array('type' => 'TextField', 'visible' => 'public', 'tab' => 'Tab Principal',),
            'cimp2' => array('type' => 'NumberField', 'visible' => 'public', 'tab' => 'Tab Principal',),
        );
    }

    public function getFilters($key = null)
    {
        if (!is_null($key)) {
            $array_filter = array();

            if (array_key_exists($key, $array_filter))
                return $array_filter[$key];
            else return null;
        }
        return null;
    }

    public function getFormDatacimp9()
    {

        return array("test" => "test", "test1" => "test1",);
    }

    public function getFormDatacimp11()
    {

        return array("test1" => "test1", "test2" => "test1",);
    }

    public function getFormDatacimp13()
    {

        return array("test1" => "test1", "test2" => "test2",);
    }

    public function beforeValidate()
    {
        if (is_array($this->cimp13)) $this->cimp13 = implode(",", $this->cimp13);
        if (is_array($this->cimp10)) $this->cimp10 = implode(",", $this->cimp10);
        return parent::beforeValidate();
    }

    public function getFormDatacimp10()
    {

        return array("test1" => "test1", "test2" => "test2",);
    }
}