<?php
/* @var $this ProdsaleController */
/* @var $data ProdSale */
?>
<div class="col-md-12">
    <h2>
        <a href="<?= Yii::app()->createUrl('sale/' . $data->idsale); ?>">
            <?= $data->valuesale . Sales::$unit; ?>
            <small>
                <?=IHtml::idate($data->startdate) . " - " . IHtml::idate($data->enddate)?>
            </small>
        </a>
    </h2>
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => new CActiveDataProvider('Product', [
            'criteria' => [
                'condition' => '"prodSales".idsale=:idsale',
                'params'=>[
                    ':idsale'=>$data->idsale
                ],
                'with' => [
                    'prodSales'
                ],
                "together" => true,
                'order' => 'nameprod ASC'
            ]
        ]),
        'itemView' => '../product/_view',
        'htmlOptions' => [
            'class' => 'row'
        ],
        'summaryCssClass' => 'container-fluid text-right',
        'summaryText'=>''
    )); ?>
</div>