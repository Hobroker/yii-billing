<?php

class IrbacController extends Controller
{
//    public $layout = '//layouts/column2';

    public function actionRole()
    {
        $auth = Yii::app()->authManager;
        $error = "<span class='text-danger'>*</span>";
        $data = [];
        if (isset($_GET['name']) && !Authitem::itemExists($_GET['name']))
            $this->redirect(['irbac/index']);
        if (isset($_POST['create'])) {
            if (Authitem::itemExists($_POST['name'])) {
                $error = "<span class='text-danger'>An item with this name already exists!</span>";
                $data = $_POST;
            } else {
                $tasks = isset($_POST['tasks']) ? $_POST['tasks'] : [];
                $role = $auth->createRole($_POST['name'], $_POST['description']);
                var_dump($tasks);
                foreach ($tasks as $task) {
                    $role->addChild($task);
                }
                $this->redirect(['irbac/role/' . $_POST['name']]);
            }
        } elseif (isset($_POST['update'])) {
            $selectedTasks = isset($_POST['tasks']) ? $_POST['tasks'] : [];
            $authTasks = $auth->getItemChildren($_GET['name']);
            $assignedTasks = [];
            foreach ($authTasks as $authTask) {
                $assignedTasks[] = $authTask->name;
            }
            foreach ($assignedTasks as $assignedTask) {
                if (!in_array($assignedTask, $selectedTasks)) {
                    Authitemchild::model()->find([
                        'condition' => 'parent=:parent AND child=:child',
                        'params' => [
                            ':parent' => $_GET['name'],
                            ':child' => $assignedTask
                        ]
                    ])->delete();
                }
            }
            foreach ($selectedTasks as $operation) {
                $dbItem = Authitemchild::model()->find([
                    'condition' => 'parent=:parent AND child=:child',
                    'params' => [
                        ':parent' => $_GET['name'],
                        ':child' => $operation
                    ]
                ]);
                if (!is_null($dbItem)) continue;
                $item = new Authitemchild();
                $item->parent = $_GET['name'];
                $item->child = $operation;
                $item->save();
            }
        }
        $model = new Authitem('search');
        $model->unsetAttributes();
        if (isset($_GET['Authitem'])) {
            $model->attributes = $_GET['Authitem'];
        }
        $model->type = 1;
        $this->render('role', [
            'data' => $data,
            'error' => $error,
            'model' => $model
        ]);
    }

    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionUser()
    {
        if (isset($_POST['assign'])) {
            $selectedRoles = isset($_POST['roles']) ? $_POST['roles'] : [];
            $auth = Yii::app()->authManager;
            $allRoles = $auth->getRoles();
            foreach ($allRoles as $role) {
                if (!is_null(Authassignment::model()->find([
                        'condition' => 'itemname=:itemname AND userid=:userid',
                        'params' => [
                            ':itemname' => $role->name,
                            ':userid' => $_GET['id']
                        ]
                    ])) && !in_array($role->name, $selectedRoles)
                )
                    $auth->revoke($role->name, $_GET['id']);
            }
            foreach ($selectedRoles as $role) {
                if (is_null(Authassignment::model()->find([
                    'condition' => 'itemname=:itemname AND userid=:userid',
                    'params' => [
                        ':itemname' => $role,
                        ':userid' => $_GET['id']
                    ]
                ])))
                    $auth->assign($role, $_GET['id']);
            }
        }
        $model = new Authitem('search');
        $model->unsetAttributes();
        if (isset($_GET['Authitem'])) {
            $model->attributes = $_GET['Authitem'];
        }
        $model->type = 2;
        if (isset($_GET['id']))
            $this->render('user', [
                'model' => $model
            ]);
    }

    public function actionTask()
    {
        Authitem::refreshOperations();
        $error = "<span class='text-danger'>*</span>";
        $data = [];
        if (isset($_GET['name']) && !Authitem::itemExists($_GET['name']))
            $this->redirect(['irbac/index']);
        if (isset($_POST['create'])) {
            $auth = Yii::app()->authManager;
            if (Authitem::itemExists($_POST['name'])) {
                $error = "<span class='text-danger'>A task with this name already exists!</span>";
                $data = $_POST;
            } else {
                $operations = isset($_POST['operations']) ? $_POST['operations'] : [];
                $role = $auth->createTask($_POST['name'], $_POST['description'], $_POST['bizrule']);
                foreach ($operations as $operation) {
                    $role->addChild($operation);
                }
                $this->redirect(['irbac/task/' . $_POST['name']]);
            }
        } elseif (isset($_POST['update'])) {
            $auth = Yii::app()->authManager;
            $item = Authitem::model()->findByPk($_GET['name']);
            $item->bizrule = $_POST['bizrule'];
            $item->save();
            $selectedOperations = isset($_POST['operations']) ? $_POST['operations'] : [];
            $authOperations = $auth->getItemChildren($_GET['name']);
            $assignedOperations = [];
            foreach ($authOperations as $authOperation) {
                $assignedOperations[] = $authOperation->name;
            }
            foreach ($assignedOperations as $assignedOperation) {
                if (!in_array($assignedOperation, $selectedOperations)) {
                    Authitemchild::model()->find([
                        'condition' => 'parent=:parent AND child=:child',
                        'params' => [
                            ':parent' => $_GET['name'],
                            ':child' => $assignedOperation
                        ]
                    ])->delete();
                }
            }
            foreach ($selectedOperations as $operation) {
                $dbItem = Authitemchild::model()->find([
                    'condition' => 'parent=:parent AND child=:child',
                    'params' => [
                        ':parent' => $_GET['name'],
                        ':child' => $operation
                    ]
                ]);
                if (!is_null($dbItem)) continue;
                $item = new Authitemchild();
                $item->parent = $_GET['name'];
                $item->child = $operation;
                $item->save();
            }
        }
        $model = new Authitem('search');
        $model->unsetAttributes();
        if (isset($_GET['Authitem'])) {
            $model->attributes = $_GET['Authitem'];
        }
        $model->type = 0;
        $this->render('task', [
            'error' => $error,
            'data' => $data,
            'model' => $model
        ]);
    }

    public static function addMenu()
    {
        return IHtml::btnGroup([
            ['label' => 'Tasks', 'url' => ['irbac/task']],
            ['label' => 'Roles', 'url' => ['irbac/role']],
            ['label' => 'Assignments', 'url' => ['irbac/user']],
        ]);
    }
}
