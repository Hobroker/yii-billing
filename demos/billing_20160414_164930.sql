--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.7
-- Dumped by pg_dump version 9.5.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authassignment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authassignment (
    itemname character varying(64) NOT NULL,
    userid character varying(64) NOT NULL,
    bizrule text,
    data text
);


ALTER TABLE authassignment OWNER TO postgres;

--
-- Name: authitem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authitem (
    name character varying(64) NOT NULL,
    type integer NOT NULL,
    description text,
    bizrule text,
    data text
);


ALTER TABLE authitem OWNER TO postgres;

--
-- Name: authitemchild; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE authitemchild (
    parent character varying(64) NOT NULL,
    child character varying(64) NOT NULL
);


ALTER TABLE authitemchild OWNER TO postgres;

--
-- Name: tbl_manufacturers; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_manufacturers (
    id integer NOT NULL,
    name character varying(100)
);


ALTER TABLE tbl_manufacturers OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE manufacturers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE manufacturers_id_seq OWNER TO postgres;

--
-- Name: manufacturers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE manufacturers_id_seq OWNED BY tbl_manufacturers.id;


--
-- Name: tbl_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_roles (
    "idRole" integer NOT NULL,
    "nameRole" character varying(50),
    permissions character varying(50)
);


ALTER TABLE tbl_roles OWNER TO postgres;

--
-- Name: roles_idrole_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE roles_idrole_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_idrole_seq OWNER TO postgres;

--
-- Name: roles_idrole_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE roles_idrole_seq OWNED BY tbl_roles."idRole";


--
-- Name: tbl_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_categories (
    idcateg integer NOT NULL,
    namecateg character varying(100)
);


ALTER TABLE tbl_categories OWNER TO postgres;

--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_category_idcateg_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_category_idcateg_seq OWNER TO postgres;

--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_category_idcateg_seq OWNED BY tbl_categories.idcateg;


--
-- Name: tbl_orders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_orders (
    idcpi integer NOT NULL,
    iduser integer,
    idprod integer,
    date date,
    idinv integer
);


ALTER TABLE tbl_orders OWNER TO postgres;

--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_client_prod_inv_idcpi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_client_prod_inv_idcpi_seq OWNER TO postgres;

--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_client_prod_inv_idcpi_seq OWNED BY tbl_orders.idcpi;


--
-- Name: tbl_comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_comment (
    id integer NOT NULL,
    content text NOT NULL,
    status integer NOT NULL,
    create_time integer,
    author character varying(128) NOT NULL,
    email character varying(128) NOT NULL,
    url character varying(128),
    post_id integer NOT NULL
);


ALTER TABLE tbl_comment OWNER TO postgres;

--
-- Name: tbl_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_comment_id_seq OWNER TO postgres;

--
-- Name: tbl_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_comment_id_seq OWNED BY tbl_comment.id;


--
-- Name: tbl_invoices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_invoices (
    idinv integer NOT NULL,
    aprobat character(1)
);


ALTER TABLE tbl_invoices OWNER TO postgres;

--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_invoices_idinv_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_invoices_idinv_seq OWNER TO postgres;

--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_invoices_idinv_seq OWNED BY tbl_invoices.idinv;


--
-- Name: tbl_lookup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_lookup (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    code integer NOT NULL,
    type character varying(128) NOT NULL,
    "position" integer NOT NULL
);


ALTER TABLE tbl_lookup OWNER TO postgres;

--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_lookup_id_seq OWNER TO postgres;

--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_lookup_id_seq OWNED BY tbl_lookup.id;


--
-- Name: tbl_organisations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_organisations (
    "idOrg" integer NOT NULL,
    name character varying(100),
    address character varying(200)
);


ALTER TABLE tbl_organisations OWNER TO postgres;

--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_organisations_idorg_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_organisations_idorg_seq OWNER TO postgres;

--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_organisations_idorg_seq OWNED BY tbl_organisations."idOrg";


--
-- Name: tbl_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_post (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    content text NOT NULL,
    tags text,
    status integer NOT NULL,
    create_time integer,
    update_time integer,
    author_id integer NOT NULL
);


ALTER TABLE tbl_post OWNER TO postgres;

--
-- Name: tbl_post_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_post_id_seq OWNER TO postgres;

--
-- Name: tbl_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_post_id_seq OWNED BY tbl_post.id;


--
-- Name: tbl_prod_sale; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_prod_sale (
    idps integer NOT NULL,
    idprod integer,
    idsale integer
);


ALTER TABLE tbl_prod_sale OWNER TO postgres;

--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_prodsale_idps_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_prodsale_idps_seq OWNER TO postgres;

--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_prodsale_idps_seq OWNED BY tbl_prod_sale.idps;


--
-- Name: tbl_products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_products (
    idprod integer NOT NULL,
    idcateg integer,
    nameprod character varying(100),
    priceprod numeric(9,2),
    idmanufacturer integer
);


ALTER TABLE tbl_products OWNER TO postgres;

--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_products_idprod_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_products_idprod_seq OWNER TO postgres;

--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_products_idprod_seq OWNED BY tbl_products.idprod;


--
-- Name: tbl_sales; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_sales (
    idsale integer NOT NULL,
    valuesale integer,
    startdate date,
    enddate date
);


ALTER TABLE tbl_sales OWNER TO postgres;

--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_sales_idsale_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_sales_idsale_seq OWNER TO postgres;

--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_sales_idsale_seq OWNED BY tbl_sales.idsale;


--
-- Name: tbl_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_tag (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    frequency integer DEFAULT 1
);


ALTER TABLE tbl_tag OWNER TO postgres;

--
-- Name: tbl_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_tag_id_seq OWNER TO postgres;

--
-- Name: tbl_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_tag_id_seq OWNED BY tbl_tag.id;


--
-- Name: tbl_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE tbl_user (
    id integer NOT NULL,
    username character varying(128) NOT NULL,
    password character varying(128) NOT NULL,
    email character varying(128) NOT NULL,
    profile text,
    fname character varying(128),
    lname character varying(128),
    tel character varying(32),
    idorg integer
);


ALTER TABLE tbl_user OWNER TO postgres;

--
-- Name: tbl_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tbl_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tbl_user_id_seq OWNER TO postgres;

--
-- Name: tbl_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tbl_user_id_seq OWNED BY tbl_user.id;


--
-- Name: idcateg; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_categories ALTER COLUMN idcateg SET DEFAULT nextval('tbl_category_idcateg_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment ALTER COLUMN id SET DEFAULT nextval('tbl_comment_id_seq'::regclass);


--
-- Name: idinv; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_invoices ALTER COLUMN idinv SET DEFAULT nextval('tbl_invoices_idinv_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_lookup ALTER COLUMN id SET DEFAULT nextval('tbl_lookup_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_manufacturers ALTER COLUMN id SET DEFAULT nextval('manufacturers_id_seq'::regclass);


--
-- Name: idcpi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders ALTER COLUMN idcpi SET DEFAULT nextval('tbl_client_prod_inv_idcpi_seq'::regclass);


--
-- Name: idOrg; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations ALTER COLUMN "idOrg" SET DEFAULT nextval('tbl_organisations_idorg_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post ALTER COLUMN id SET DEFAULT nextval('tbl_post_id_seq'::regclass);


--
-- Name: idps; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale ALTER COLUMN idps SET DEFAULT nextval('tbl_prodsale_idps_seq'::regclass);


--
-- Name: idprod; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products ALTER COLUMN idprod SET DEFAULT nextval('tbl_products_idprod_seq'::regclass);


--
-- Name: idRole; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_roles ALTER COLUMN "idRole" SET DEFAULT nextval('roles_idrole_seq'::regclass);


--
-- Name: idsale; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_sales ALTER COLUMN idsale SET DEFAULT nextval('tbl_sales_idsale_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_tag ALTER COLUMN id SET DEFAULT nextval('tbl_tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user ALTER COLUMN id SET DEFAULT nextval('tbl_user_id_seq'::regclass);


--
-- Data for Name: authassignment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authassignment VALUES ('manager', '8', NULL, 'N;');
INSERT INTO authassignment VALUES ('client', '16', NULL, 'N;');
INSERT INTO authassignment VALUES ('god', '8', NULL, 'N;');
INSERT INTO authassignment VALUES ('client', '1', NULL, 'N;');
INSERT INTO authassignment VALUES ('god', '16', NULL, 'N;');
INSERT INTO authassignment VALUES ('manager', '16', NULL, 'N;');
INSERT INTO authassignment VALUES ('client', '17', NULL, 'N;');
INSERT INTO authassignment VALUES ('admin', '8', NULL, 'N;');


--
-- Data for Name: authitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authitem VALUES ('admin', 2, '', NULL, 'N;');
INSERT INTO authitem VALUES ('manager', 2, '', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:View', 0, 'Category Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:Create', 0, 'Category Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:Update', 0, 'Category Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:Delete', 0, 'Category Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:Index', 0, 'Category Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Category:Admin', 0, 'Category Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Comment:Update', 0, 'Comment Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Comment:Delete', 0, 'Comment Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Comment:Index', 0, 'Comment Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Comment:Approve', 0, 'Comment Controller, Approve Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:View', 0, 'Invoice Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Create', 0, 'Invoice Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Update', 0, 'Invoice Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Delete', 0, 'Invoice Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Index', 0, 'Invoice Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Mine', 0, 'Invoice Controller, Mine Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Admin', 0, 'Invoice Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Invoice:Approve', 0, 'Invoice Controller, Approve Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Create', 0, 'Irbac Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Operation', 0, 'Irbac Controller, Operation Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Role', 0, 'Irbac Controller, Role Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Users', 0, 'Irbac Controller, Users Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Index', 0, 'Irbac Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:User', 0, 'Irbac Controller, User Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:View', 0, 'Manufacturer Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:Create', 0, 'Manufacturer Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:Update', 0, 'Manufacturer Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:Delete', 0, 'Manufacturer Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:Index', 0, 'Manufacturer Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Manufacturer:Admin', 0, 'Manufacturer Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:View', 0, 'Order Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Create', 0, 'Order Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Update', 0, 'Order Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Delete', 0, 'Order Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Remove', 0, 'Order Controller, Remove Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Index', 0, 'Order Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Mine', 0, 'Order Controller, Mine Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Admin', 0, 'Order Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Checkout', 0, 'Order Controller, Checkout Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Order:Finish', 0, 'Order Controller, Finish Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:View', 0, 'Organisations Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:Create', 0, 'Organisations Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:Update', 0, 'Organisations Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:Delete', 0, 'Organisations Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:Index', 0, 'Organisations Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Organisations:Admin', 0, 'Organisations Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:View', 0, 'Post Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:Create', 0, 'Post Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:Update', 0, 'Post Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:Delete', 0, 'Post Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:Index', 0, 'Post Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:Admin', 0, 'Post Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Post:SuggestTags', 0, 'Post Controller, SuggestTags Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:View', 0, 'Prodsale Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:Create', 0, 'Prodsale Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:Update', 0, 'Prodsale Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:Delete', 0, 'Prodsale Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:Index', 0, 'Prodsale Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Prodsale:Admin', 0, 'Prodsale Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:View', 0, 'Product Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Create', 0, 'Product Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Update', 0, 'Product Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Delete', 0, 'Product Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Index', 0, 'Product Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Admin', 0, 'Product Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Product:Buy', 0, 'Product Controller, Buy Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:View', 0, 'Roles Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:Create', 0, 'Roles Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:Update', 0, 'Roles Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:Delete', 0, 'Roles Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:Index', 0, 'Roles Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Roles:Admin', 0, 'Roles Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:View', 0, 'Sale Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:Create', 0, 'Sale Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:Update', 0, 'Sale Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:Delete', 0, 'Sale Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:Index', 0, 'Sale Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Sale:Admin', 0, 'Sale Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Captcha', 0, 'Site Controller, Captcha Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:CCaptchaAction', 0, 'Site Controller, CCaptchaAction Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Error', 0, 'Site Controller, Error Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Contact', 0, 'Site Controller, Contact Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Login', 0, 'Site Controller, Login Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Logout', 0, 'Site Controller, Logout Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Test', 0, 'Site Controller, Test Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Site:Index', 0, 'Site Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:View', 0, 'User Controller, View Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Create', 0, 'User Controller, Create Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Register', 0, 'User Controller, Register Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Update', 0, 'User Controller, Update Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Delete', 0, 'User Controller, Delete Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Index', 0, 'User Controller, Index Action', NULL, 'N;');
INSERT INTO authitem VALUES ('User:Admin', 0, 'User Controller, Admin Action', NULL, 'N;');
INSERT INTO authitem VALUES ('Irbac:Task', 0, 'Irbac Controller, Task Action', NULL, 'N;');
INSERT INTO authitem VALUES ('rbacManager', 1, '', NULL, 'N;');
INSERT INTO authitem VALUES ('Guest', 2, '', NULL, 'N;');
INSERT INTO authitem VALUES ('client', 2, '', NULL, 'N;');
INSERT INTO authitem VALUES ('test', 1, 'description', '', 'N;');
INSERT INTO authitem VALUES ('everything', 1, '', '', 'N;');
INSERT INTO authitem VALUES ('god', 2, '', NULL, 'N;');
INSERT INTO authitem VALUES ('guestTask', 1, 'description', 'return Yii::app()->user->isGuest;', 'N;');
INSERT INTO authitem VALUES ('clientTask', 1, '', '', 'N;');
INSERT INTO authitem VALUES ('managerTask', 1, '', '', 'N;');


--
-- Data for Name: authitemchild; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO authitemchild VALUES ('clientTask', 'Order:Create');
INSERT INTO authitemchild VALUES ('clientTask', 'Order:Remove');
INSERT INTO authitemchild VALUES ('clientTask', 'Order:Checkout');
INSERT INTO authitemchild VALUES ('everything', 'Category:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Category:Create');
INSERT INTO authitemchild VALUES ('everything', 'Category:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Category:Index');
INSERT INTO authitemchild VALUES ('everything', 'Category:Update');
INSERT INTO authitemchild VALUES ('everything', 'Category:View');
INSERT INTO authitemchild VALUES ('everything', 'Comment:Approve');
INSERT INTO authitemchild VALUES ('everything', 'Comment:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Comment:Index');
INSERT INTO authitemchild VALUES ('everything', 'Comment:Update');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Approve');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Create');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Index');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Mine');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:Update');
INSERT INTO authitemchild VALUES ('everything', 'Invoice:View');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Create');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Index');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Operation');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Role');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Task');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:User');
INSERT INTO authitemchild VALUES ('everything', 'Irbac:Users');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:Create');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:Index');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:Update');
INSERT INTO authitemchild VALUES ('everything', 'Manufacturer:View');
INSERT INTO authitemchild VALUES ('everything', 'Order:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Order:Checkout');
INSERT INTO authitemchild VALUES ('everything', 'Order:Create');
INSERT INTO authitemchild VALUES ('everything', 'Order:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Order:Finish');
INSERT INTO authitemchild VALUES ('everything', 'Order:Index');
INSERT INTO authitemchild VALUES ('everything', 'Order:Mine');
INSERT INTO authitemchild VALUES ('everything', 'Order:Remove');
INSERT INTO authitemchild VALUES ('everything', 'Order:Update');
INSERT INTO authitemchild VALUES ('everything', 'Order:View');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:Admin');
INSERT INTO authitemchild VALUES ('guestTask', 'Product:Index');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:Create');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:Index');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:Update');
INSERT INTO authitemchild VALUES ('everything', 'Organisations:View');
INSERT INTO authitemchild VALUES ('Guest', 'guestTask');
INSERT INTO authitemchild VALUES ('everything', 'Post:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Post:Create');
INSERT INTO authitemchild VALUES ('everything', 'Post:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Post:Index');
INSERT INTO authitemchild VALUES ('everything', 'Post:SuggestTags');
INSERT INTO authitemchild VALUES ('everything', 'Post:Update');
INSERT INTO authitemchild VALUES ('everything', 'Post:View');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:Create');
INSERT INTO authitemchild VALUES ('clientTask', 'Category:Index');
INSERT INTO authitemchild VALUES ('clientTask', 'Category:View');
INSERT INTO authitemchild VALUES ('clientTask', 'Invoice:Index');
INSERT INTO authitemchild VALUES ('clientTask', 'Invoice:Create');
INSERT INTO authitemchild VALUES ('clientTask', 'Invoice:Mine');
INSERT INTO authitemchild VALUES ('clientTask', 'Invoice:View');
INSERT INTO authitemchild VALUES ('clientTask', 'Manufacturer:Index');
INSERT INTO authitemchild VALUES ('clientTask', 'Manufacturer:View');
INSERT INTO authitemchild VALUES ('client', 'clientTask');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Create');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Index');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Operation');
INSERT INTO authitemchild VALUES ('clientTask', 'Order:Mine');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:Index');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:Update');
INSERT INTO authitemchild VALUES ('everything', 'Prodsale:View');
INSERT INTO authitemchild VALUES ('everything', 'Product:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Product:Buy');
INSERT INTO authitemchild VALUES ('everything', 'Product:Create');
INSERT INTO authitemchild VALUES ('everything', 'Product:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Product:Index');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Role');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Task');
INSERT INTO authitemchild VALUES ('everything', 'Product:Update');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:User');
INSERT INTO authitemchild VALUES ('rbacManager', 'Irbac:Users');
INSERT INTO authitemchild VALUES ('everything', 'Product:View');
INSERT INTO authitemchild VALUES ('everything', 'Roles:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Roles:Create');
INSERT INTO authitemchild VALUES ('everything', 'Roles:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Roles:Index');
INSERT INTO authitemchild VALUES ('everything', 'Roles:Update');
INSERT INTO authitemchild VALUES ('everything', 'Roles:View');
INSERT INTO authitemchild VALUES ('everything', 'Sale:Admin');
INSERT INTO authitemchild VALUES ('everything', 'Sale:Create');
INSERT INTO authitemchild VALUES ('everything', 'Sale:Delete');
INSERT INTO authitemchild VALUES ('everything', 'Sale:Index');
INSERT INTO authitemchild VALUES ('everything', 'Sale:Update');
INSERT INTO authitemchild VALUES ('everything', 'Sale:View');
INSERT INTO authitemchild VALUES ('everything', 'Site:Captcha');
INSERT INTO authitemchild VALUES ('everything', 'Site:CCaptchaAction');
INSERT INTO authitemchild VALUES ('everything', 'Site:Contact');
INSERT INTO authitemchild VALUES ('everything', 'Site:Error');
INSERT INTO authitemchild VALUES ('everything', 'Site:Index');
INSERT INTO authitemchild VALUES ('everything', 'Site:Login');
INSERT INTO authitemchild VALUES ('everything', 'Site:Logout');
INSERT INTO authitemchild VALUES ('everything', 'Site:Test');
INSERT INTO authitemchild VALUES ('everything', 'User:Admin');
INSERT INTO authitemchild VALUES ('everything', 'User:Create');
INSERT INTO authitemchild VALUES ('everything', 'User:Delete');
INSERT INTO authitemchild VALUES ('everything', 'User:Index');
INSERT INTO authitemchild VALUES ('everything', 'User:Register');
INSERT INTO authitemchild VALUES ('everything', 'User:Update');
INSERT INTO authitemchild VALUES ('everything', 'User:View');
INSERT INTO authitemchild VALUES ('god', 'everything');
INSERT INTO authitemchild VALUES ('guestTask', 'User:Register');
INSERT INTO authitemchild VALUES ('clientTask', 'Product:Index');
INSERT INTO authitemchild VALUES ('clientTask', 'Product:Buy');
INSERT INTO authitemchild VALUES ('clientTask', 'Product:View');
INSERT INTO authitemchild VALUES ('clientTask', 'Order:Finish');
INSERT INTO authitemchild VALUES ('clientTask', 'Order:View');
INSERT INTO authitemchild VALUES ('managerTask', 'Invoice:Approve');
INSERT INTO authitemchild VALUES ('manager', 'clientTask');
INSERT INTO authitemchild VALUES ('manager', 'managerTask');
INSERT INTO authitemchild VALUES ('admin', 'everything');


--
-- Name: manufacturers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('manufacturers_id_seq', 6, true);


--
-- Name: roles_idrole_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('roles_idrole_seq', 1, true);


--
-- Data for Name: tbl_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_categories VALUES (1, 'Laptop');
INSERT INTO tbl_categories VALUES (3, 'Smartphone');
INSERT INTO tbl_categories VALUES (4, 'Mouse');
INSERT INTO tbl_categories VALUES (5, 'Keyboard');
INSERT INTO tbl_categories VALUES (6, 'Power Bank');
INSERT INTO tbl_categories VALUES (7, 'Hard Disk');
INSERT INTO tbl_categories VALUES (8, 'Tablet');
INSERT INTO tbl_categories VALUES (9, 'new');


--
-- Name: tbl_category_idcateg_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_category_idcateg_seq', 9, true);


--
-- Name: tbl_client_prod_inv_idcpi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_client_prod_inv_idcpi_seq', 20, true);


--
-- Data for Name: tbl_comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_comment VALUES (1, 'This is a test comment.', 2, 1230952187, 'Tester', 'tester@example.com', NULL, 2);


--
-- Name: tbl_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_comment_id_seq', 1, true);


--
-- Data for Name: tbl_invoices; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_invoices VALUES (176, '1');
INSERT INTO tbl_invoices VALUES (192, '1');
INSERT INTO tbl_invoices VALUES (193, '1');
INSERT INTO tbl_invoices VALUES (194, '1');
INSERT INTO tbl_invoices VALUES (195, '0');
INSERT INTO tbl_invoices VALUES (197, '1');
INSERT INTO tbl_invoices VALUES (196, '1');
INSERT INTO tbl_invoices VALUES (198, '1');


--
-- Name: tbl_invoices_idinv_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_invoices_idinv_seq', 198, true);


--
-- Data for Name: tbl_lookup; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_lookup VALUES (1, 'Draft', 1, 'PostStatus', 1);
INSERT INTO tbl_lookup VALUES (2, 'Published', 2, 'PostStatus', 2);
INSERT INTO tbl_lookup VALUES (3, 'Archived', 3, 'PostStatus', 3);
INSERT INTO tbl_lookup VALUES (4, 'Pending Approval', 1, 'CommentStatus', 1);
INSERT INTO tbl_lookup VALUES (5, 'Approved', 2, 'CommentStatus', 2);


--
-- Name: tbl_lookup_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_lookup_id_seq', 5, true);


--
-- Data for Name: tbl_manufacturers; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_manufacturers VALUES (1, 'LG');
INSERT INTO tbl_manufacturers VALUES (2, 'Sony');
INSERT INTO tbl_manufacturers VALUES (3, 'Asus');
INSERT INTO tbl_manufacturers VALUES (4, 'Acer');
INSERT INTO tbl_manufacturers VALUES (5, 'Dell');
INSERT INTO tbl_manufacturers VALUES (6, 'Samsung');


--
-- Data for Name: tbl_orders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_orders VALUES (3, 8, 1, NULL, 192);
INSERT INTO tbl_orders VALUES (7, 8, 1, '2016-04-07', 193);
INSERT INTO tbl_orders VALUES (8, 8, 1, '2016-04-07', 193);
INSERT INTO tbl_orders VALUES (11, 8, 2, '2016-04-09', 194);
INSERT INTO tbl_orders VALUES (12, 8, 4, '2016-04-09', 194);
INSERT INTO tbl_orders VALUES (13, 8, 2, '2016-04-14', 195);
INSERT INTO tbl_orders VALUES (6, 1, 1, '2016-04-06', 176);
INSERT INTO tbl_orders VALUES (2, 1, 2, NULL, 176);


--
-- Data for Name: tbl_organisations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_organisations VALUES (1, 'Qaz1', 'str. Nr 1');


--
-- Name: tbl_organisations_idorg_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_organisations_idorg_seq', 3, true);


--
-- Data for Name: tbl_post; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_post VALUES (1, 'Welcome!', 'This blog system is developed using Yii. It is meant to demonstrate how to use Yii to build a complete real-world application. Complete source code may be found in the Yii releases.

Feel free to try this system by writing new posts and posting comments.', 'yii, blog', 2, 1230952187, 1230952187, 1);
INSERT INTO tbl_post VALUES (2, 'A Test Post', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'test', 2, 1230952187, 1230952187, 1);


--
-- Name: tbl_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_post_id_seq', 2, true);


--
-- Data for Name: tbl_prod_sale; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_prod_sale VALUES (3, 1, 3);
INSERT INTO tbl_prod_sale VALUES (4, 2, 2);
INSERT INTO tbl_prod_sale VALUES (5, 3, 3);


--
-- Name: tbl_prodsale_idps_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_prodsale_idps_seq', 5, true);


--
-- Data for Name: tbl_products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_products VALUES (1, 4, 'WT415', 526.00, 3);
INSERT INTO tbl_products VALUES (2, 3, 'Xperia Z5', 20100.00, 2);
INSERT INTO tbl_products VALUES (3, 7, 'Spinpoint M8', 2000.00, 6);
INSERT INTO tbl_products VALUES (4, 4, 'ASD 666', 16000.00, 3);


--
-- Name: tbl_products_idprod_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_products_idprod_seq', 4, true);


--
-- Data for Name: tbl_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_roles VALUES (1, 'Administrator', '1111');


--
-- Data for Name: tbl_sales; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_sales VALUES (3, 15, '2016-04-01', '2016-04-21');
INSERT INTO tbl_sales VALUES (2, 50, '2016-04-02', '2016-04-09');


--
-- Name: tbl_sales_idsale_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_sales_idsale_seq', 3, true);


--
-- Data for Name: tbl_tag; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_tag VALUES (1, 'yii', 1);
INSERT INTO tbl_tag VALUES (2, 'blog', 1);
INSERT INTO tbl_tag VALUES (3, 'test', 1);


--
-- Name: tbl_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_tag_id_seq', 3, true);


--
-- Data for Name: tbl_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tbl_user VALUES (1, 'demo1', '$2a$10$JTJf6/XqC94rrOtzuF397OHa4mbmZrVTBOQCmYD9U.obZRUut4BoC', 'webmaster@example.com', '', 'Igor', 'Leahu', '+37378672301', 1);
INSERT INTO tbl_user VALUES (8, 'magic', '$2y$13$HqWQ5NriaUsuIRbpC94HDO7F1l4WS9Jbrh9V/RPVJrMaCMlHCLw0G', 'igor.leahu24@gmail.com', '', 'John', 'Doe', '+373556775124', 1);
INSERT INTO tbl_user VALUES (17, 'daemon', '$2y$13$OnvYtObsBwYG90nazp7pGuP//B6gAR3j1T/1yqNYjjR.EyF1b01ka', 'igor.leahu24@gmail.com', '', 'Igor', 'Leahu', '+37378672301', 1);


--
-- Name: tbl_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tbl_user_id_seq', 17, true);


--
-- Name: authassignment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authassignment
    ADD CONSTRAINT authassignment_pkey PRIMARY KEY (itemname, userid);


--
-- Name: authitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitem
    ADD CONSTRAINT authitem_pkey PRIMARY KEY (name);


--
-- Name: authitemchild_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_pkey PRIMARY KEY (parent, child);


--
-- Name: manufacturers_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_manufacturers
    ADD CONSTRAINT manufacturers_pkey PRIMARY KEY (id);


--
-- Name: pk_organisations; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations
    ADD CONSTRAINT pk_organisations PRIMARY KEY ("idOrg");


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY ("idRole");


--
-- Name: tbl_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_categories
    ADD CONSTRAINT tbl_category_pkey PRIMARY KEY (idcateg);


--
-- Name: tbl_client_prod_inv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_client_prod_inv_pkey PRIMARY KEY (idcpi);


--
-- Name: tbl_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment
    ADD CONSTRAINT tbl_comment_pkey PRIMARY KEY (id);


--
-- Name: tbl_invoices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_invoices
    ADD CONSTRAINT tbl_invoices_pkey PRIMARY KEY (idinv);


--
-- Name: tbl_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_lookup
    ADD CONSTRAINT tbl_lookup_pkey PRIMARY KEY (id);


--
-- Name: tbl_organisations_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_organisations
    ADD CONSTRAINT tbl_organisations_name_key UNIQUE (name);


--
-- Name: tbl_post_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post
    ADD CONSTRAINT tbl_post_pkey PRIMARY KEY (id);


--
-- Name: tbl_prodsale_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_pkey PRIMARY KEY (idps);


--
-- Name: tbl_products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_products_pkey PRIMARY KEY (idprod);


--
-- Name: tbl_sales_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_sales
    ADD CONSTRAINT tbl_sales_pkey PRIMARY KEY (idsale);


--
-- Name: tbl_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_tag
    ADD CONSTRAINT tbl_tag_pkey PRIMARY KEY (id);


--
-- Name: tbl_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user
    ADD CONSTRAINT tbl_user_pkey PRIMARY KEY (id);


--
-- Name: authassignment_itemname_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authassignment
    ADD CONSTRAINT authassignment_itemname_fkey FOREIGN KEY (itemname) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authitemchild_child_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_child_fkey FOREIGN KEY (child) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: authitemchild_parent_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY authitemchild
    ADD CONSTRAINT authitemchild_parent_fkey FOREIGN KEY (parent) REFERENCES authitem(name) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tbl_client_prod_inv_iduser_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_client_prod_inv_iduser_fkey FOREIGN KEY (iduser) REFERENCES tbl_user(id);


--
-- Name: tbl_comment_post_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_comment
    ADD CONSTRAINT tbl_comment_post_id_fkey FOREIGN KEY (post_id) REFERENCES tbl_post(id);


--
-- Name: tbl_manufacturer_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_manufacturer_fkey FOREIGN KEY (idmanufacturer) REFERENCES tbl_manufacturers(id);


--
-- Name: tbl_orders_idinv_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_orders_idinv_fkey FOREIGN KEY (idinv) REFERENCES tbl_invoices(idinv);


--
-- Name: tbl_orders_idprod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_orders
    ADD CONSTRAINT tbl_orders_idprod_fkey FOREIGN KEY (idprod) REFERENCES tbl_products(idprod);


--
-- Name: tbl_post_author_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_post
    ADD CONSTRAINT tbl_post_author_id_fkey FOREIGN KEY (author_id) REFERENCES tbl_user(id);


--
-- Name: tbl_prodsale_idprod_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_idprod_fkey FOREIGN KEY (idprod) REFERENCES tbl_products(idprod);


--
-- Name: tbl_prodsale_idsale_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_prod_sale
    ADD CONSTRAINT tbl_prodsale_idsale_fkey FOREIGN KEY (idsale) REFERENCES tbl_sales(idsale);


--
-- Name: tbl_products_idcateg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_products
    ADD CONSTRAINT tbl_products_idcateg_fkey FOREIGN KEY (idcateg) REFERENCES tbl_categories(idcateg);


--
-- Name: tbl_user_idorg_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tbl_user
    ADD CONSTRAINT tbl_user_idorg_fkey FOREIGN KEY (idorg) REFERENCES tbl_organisations("idOrg");


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

