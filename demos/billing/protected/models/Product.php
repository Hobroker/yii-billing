<?php

/**
 * This is the model class for table "{{products}}".
 *
 * The followings are the available columns in table '{{products}}':
 * @property integer $idprod
 * @property integer $idcateg
 * @property string $nameprod
 * @property string $priceprod
 * @property integer $idmanufacturer
 *
 * The followings are the available model relations:
 * @property Categories $idcateg0
 * @property Manufacturers $idmanufacturer0
 * @property ProdSale[] $prodSales
 */
class Product extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public static $currency = " MDL";

    public function tableName()
    {
        return '{{products}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idcateg, idmanufacturer', 'numerical', 'integerOnly' => true),
            array('nameprod', 'length', 'max' => 100),
            array('priceprod', 'length', 'max' => 9),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idprod, idcateg, nameprod, priceprod, idmanufacturer', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idcateg0' => array(self::BELONGS_TO, 'Categories', 'idcateg'),
            'idmanufacturer0' => array(self::BELONGS_TO, 'Manufacturer', 'idmanufacturer'),
            'prodSales' => array(self::HAS_MANY, 'ProdSale', 'idprod'),
            'orders' => array(self::HAS_MANY, 'Order', 'idprod'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idprod' => 'ID',
            'idcateg' => 'Category',
            'nameprod' => 'Name',
            'priceprod' => 'Price',
            'idmanufacturer' => 'Manufacturer',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idprod', $this->idprod);
        $criteria->compare('idcateg', $this->idcateg);
        $criteria->compare('nameprod', $this->nameprod, true);
        $criteria->compare('priceprod', $this->priceprod, true);
        $criteria->compare('idmanufacturer', $this->idmanufacturer);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Product the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function productSumByUser()
    {
        $orders = Order::model()->findAll([
            'condition' => 'iduser=:iduser AND idinv IS NULL',
            'params' => [':iduser' => Yii::app()->User->id]
        ]);
        $sum = 0;
        foreach ($orders as $order) {
            $initialPrice = $order->idprod0->priceprod;
            $productSale = Product::model()->productSale($order->idprod);
            $sum += $initialPrice - ($initialPrice * ($productSale / 100));
        }
        return $sum;
    }

    public static function productSale($id)
    {
        $prodsales = ProdSale::model()->find([
            'condition' => 'idprod=:idprod',
            'params' => [
                ':idprod' => $id
            ]
        ]);
        if (!is_null($prodsales))
            return $prodsales->idsale0->valuesale;
        return 0;
    }

    public static function productPriceWithSale($id)
    {
        $initialPrice = Product::model()->findByPk($id)->priceprod;
        $productSale = Product::model()->productSale($id);
        return $initialPrice - ($initialPrice * ($productSale / 100));
    }

    public static function productSaleUntil($id)
    {
        $prodsale = ProdSale::model()->find([
                'condition' => 'idprod=:idprod',
                'params' => [
                    ':idprod' => $id
                ]
            ]
        );
        return $prodsale->idsale0->startdate;
    }

    public static function productManufacturer($id)
    {
        return Manufacturer::model()->findByPk(Product::model()->findByPk($id)->idmanufacturer)->name;
    }

    public static function getProductsByCategoy($id)
    {
        $products = Product::model()->findAll([
            'condition' => 'idcateg=:idcateg',
            'params' => [':idcateg' => $id]
        ]);
        return $products;
    }
}
