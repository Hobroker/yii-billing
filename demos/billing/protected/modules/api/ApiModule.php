<?php

class ApiModule extends CWebModule
{
    //public $applicationLayout = 'application.views.layouts.main';
    public $applicationLayout = 'main';

    /**
     * @property string The base URL used to access RBAM.
     * If NULL (default) the baseUrl is: '/parentModule1Id/…/parentModuleNId/rbam'.
     * Do not append any slash character to the URL.
     */
    public $baseUrl;
    /**
     * @property string The base script URL for all module resources (e.g. javascript,
     * CSS file, images).
     * If NULL (default) the integrated module resources (which are published as
     * assets) are used.
     */
    public $baseAssetsUrl;
    /**
     * @property string The URL of the CSS file used by this module.
     * If NULL (default) the integrated CSS file is used.
     * If === FALSE a CSS file must be explicitly included, e.g. in the layout.
     */
    public $cssFile;

    /**
     * @property boolean Set TRUE to enable development mode.
     * In development mode assets (e.g. JavaScript and CSS files) are published on
     * each access and options to initialise (if RbamModule::initialise is not
     * empty) and generate authorisation data are shown.
     */
    public $development = true;
    public $front_tiles = array();
    private $_cs;
    private $_assetsUrl;

    public function init()
    {
        //Yii::app()->theme = 'user_theme';
        if (strpos(Yii::app()->request->pathinfo, '/') !== false || strlen(Yii::app()->request->pathinfo) <= 43)
            $pathinfo = Yii::app()->request->pathinfo;
        else {
            $pathinfo = Yii::app()->getSecurityManager()->decrypt(base64_decode(strtr(Yii::app()->request->pathinfo, '-_,', '+/=')));
        }
        if (preg_match('#ImportExport#isu', $pathinfo)) {
            $this->_setBaseUrl();
            $this->_publishModuleAssets();
        }
    }

    private function _setBaseUrl()
    {
        if (empty($this->baseUrl)) {
            $this->baseUrl = '';
            $m = $this;
            do {
                $this->baseUrl = '/' . $m->getId() . $this->baseUrl;
                $m = $m->getParentModule();
            } while (!is_null($m));
        }
        if (substr($this->baseUrl, -1) === '/') $this->baseUrl = substr($this->baseUrl, 0, -1);
    }

    /*public function getAssetsUrl()
    {
        if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('ImportExport') );
        return $this->_assetsUrl;
    }*/

    private function _publishModuleAssets()
    {
        if (is_null($this->_cs))
            $this->_cs = Yii::app()->getClientScript();

        if (!is_string($this->baseAssetsUrl)) {
            // Republish if in development mode
            $this->baseAssetsUrl = ($this->development ?
                Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('Api.assets'), false, -1, true) :
                Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('Api.assets'))
            );
        }
        //$this->_cs->registerCssFile($this->baseAssetsUrl . '/css/screen.css');
        //$this->_cs->registerCssFile($this->baseAssetsUrl . '/css/print.css');

        //$this->_cs->registerCssFile($this->baseAssetsUrl . '/css/main.css');
        $this->_cs->registerCssFile($this->baseAssetsUrl . '/css/form.css');
        $this->_cs->registerCssFile($this->baseAssetsUrl . '/css/custom.css');
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }
}
