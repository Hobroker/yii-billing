<?php
/* @var $this OrganisationsController */
/* @var $data Organisations */
?>

<div class="col-md-4 col-sm-6">
    <h4>
        <a href="<?= Yii::app()->createUrl('organisations/' . $data->idOrg); ?>"
           class="thumbnail well no-underline">
            <?= CHtml::encode($data->name) ?>
            <br>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
                <?= CHtml::encode($data->address) ?>
            </small>
        </a>
    </h4>
</div>