<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idps'); ?>
		<?php echo $form->textField($model,'idps'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idprod'); ?>
		<?php echo $form->textField($model,'idprod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idsale'); ?>
		<?php echo $form->textField($model,'idsale'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->