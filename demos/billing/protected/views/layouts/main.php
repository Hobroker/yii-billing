<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- blueprint CSS framework -->
    <!--    <link rel="stylesheet" type="text/css" href="-->
    <?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection"/>-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <!--    <link rel="stylesheet" type="text/css" href="-->
    <?php //echo Yii::app()->request->baseUrl; ?><!--/css/main.css"/>-->
    <!--    <link rel="stylesheet" type="text/css" href="-->
    <?php //echo Yii::app()->request->baseUrl; ?><!--/css/form.css"/>-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/magic.css"/>

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/bootstrap.min.css">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    <?php
    Yii::app()->clientScript->registerCoreScript('jquery');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/bootstrap.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/magic.js', CClientScript::POS_END);
    ?>
</head>

<body>

<div class="container">
    <div id="header">

    </div>

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navtgl"
                        aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?= CHtml::link(Yii::app()->name, Yii::app()->baseUrl, ['class' => 'navbar-brand']); ?>
            </div>
            <div class="collapse navbar-collapse" id="navtgl">
                <?php $this->widget('zii.widgets.CMenu', array(
                    'items' => array(
                        ['label' => 'Home', 'url' => array('product/index')],
                        ['label' => 'Contact', 'url' => array('site/contact')],
                    ),
                    'htmlOptions' => [
                        'class' => 'nav navbar-nav'
                    ]
                )); ?>
                <?php $this->widget('zii.widgets.CMenu', array(
                    'items' => [
                        ['label' => "Test", 'url' => ['site/test'], 'visible' => Yii::app()->user->checkAccess('admin')],
                        ['label' => "Admin <span class='caret'></span>", 'url' => '#',
                            'linkOptions' => [
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown',
                            ],
                            'visible' => Yii::app()->user->checkAccess('admin'),
                            'itemOptions' => [
                                'class' => 'dropdown'
                            ],
                            'items' => array_merge([
                                ['label' => 'Products', 'url' => ['product/index']],
                                ['label' => 'Organisations', 'url' => ['organisations/index']],
                                ['label' => 'Users', 'url' => ['user/index']],
                                ['label' => 'Categories', 'url' => ['category/index']],
                                ['label' => 'Manufacturers', 'url' => ['manufacturer/index']],
                                ['label' => 'Sales', 'url' => ['sale/index']],
                                ['label' => 'Product Sales', 'url' => ['prodsale/index']],
                                ['label' => 'Orders', 'url' => ['order/index']],
                                ['label' => 'Invoices', 'url' => ['invoice/index']],
                            ], [
                                ['label' => '<li role="separator" class="divider"></li>'],
                                ['label' => 'I-RBAC', 'url' => ['irbac/index']],
                                ['label' => 'Gii', 'url' => ['/gii']],
                            ]),
                        ],
                        ['label' => 'Login', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest],
                        ['label' => 'Register', 'url' => array('user/register'), 'visible' => Yii::app()->user->isGuest],
                        ['label' => "My account " . '(<b>' . Yii::app()->user->name . ')</b>' . " <span class='caret'></span>", 'url' => '#',
                            'linkOptions' => [
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown',
                            ],
                            'itemOptions' => [
                                'class' => 'dropdown'
                            ],
                            'visible' => !Yii::app()->user->isGuest,
                            'items' => [
                                ['label' => 'My cart (' . Order::getNrCartProducts() . ')', 'url' => ['order/mine']],
                                ['label' => 'My invoices (' . Order::userInvoices() . ')', 'url' => ['invoice/mine']],
                                ['label' => 'Logout',
                                    'url' => array('site/logout'),
                                    'linkOptions' => [
                                        'class' => 'bg-danger'
                                    ],
                                    'confirm' => 'Are you sure?'
                                ],
                            ],
                        ],
                    ],
                    'htmlOptions' => [
                        'class' => 'nav navbar-nav pull-right'
                    ],
                    'submenuHtmlOptions' => ['class' => 'dropdown-menu'],
                    'activeCssClass' => 'active',
                    'encodeLabel' => false
                )); ?>
            </div>
        </div>
    </nav>

    <?php $this->widget('zii.widgets.CBreadcrumbs', array(
        'links' => $this->breadcrumbs,
        'htmlOptions' => [
            'class' => 'breadcrumb'
        ],
        'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
        'inactiveLinkTemplate' => '<li class="active">{label}</li>',
        'separator' => ''
    )); ?>
    <?php echo $content; ?>
</div>
</body>
</html>