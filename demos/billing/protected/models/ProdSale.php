<?php

/**
 * This is the model class for table "{{prod_sale}}".
 *
 * The followings are the available columns in table '{{prod_sale}}':
 * @property integer $idps
 * @property integer $idprod
 * @property integer $idsale
 *
 * The followings are the available model relations:
 * @property Products $idprod0
 * @property Sales $idsale0
 */
class ProdSale extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{prod_sale}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('idprod, idsale', 'numerical', 'integerOnly' => true),
            ['idprod, idsale', 'required'],
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idps, idprod, idsale', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'idprod0' => array(self::BELONGS_TO, 'Product', 'idprod'),
            'idsale0' => array(self::BELONGS_TO, 'Sales', 'idsale'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idps' => 'ID',
            'idprod' => 'Product',
            'idsale' => 'Sale',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idps', $this->idps);
        $criteria->compare('idprod', $this->idprod);
        $criteria->compare('idsale', $this->idsale);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ProdSale the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
