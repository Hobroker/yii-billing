<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */

$this->breadcrumbs=array(
	'Prod Sales'=>array('index'),
	$model->idps=>array('view','id'=>$model->idps),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProdSale', 'url'=>array('index')),
	array('label'=>'Create ProdSale', 'url'=>array('create')),
	array('label'=>'View ProdSale', 'url'=>array('view', 'id'=>$model->idps)),
	array('label'=>'Manage ProdSale', 'url'=>array('admin')),
);
?>

<h2>Update ProdSale <?php echo $model->idps; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>