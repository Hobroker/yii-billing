<?php
/* @var $this OrganisationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Organisations',
);

$this->menu = array(
    array('label' => 'Create Organisations', 'url' => array('create')),
    array('label' => 'Manage Organisations', 'url' => array('admin')),
);
?>

<h2>Organisations</h2>

<div class="row">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )); ?>
</div>
