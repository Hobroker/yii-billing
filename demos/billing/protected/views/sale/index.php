<?php
/* @var $this SaleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Sales',
);

$this->menu = array(
    array('label' => 'Create Sale', 'url' => array('create')),
    array('label' => 'Manage Sales', 'url' => array('admin')),
);
?>

<h2>Sales</h2>
<div class="container-fluid">
    <div class="col-md-12">
        <h3>Active</h3>
    </div>
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
        'summaryText'=>'',
    )); ?>
    <div class="col-md-12">
        <h3>Inactive</h3>
    </div>
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => new CActiveDataProvider('Sales', [
            'criteria' => [
                'condition' => 'enddate < now()::DATE'
            ]
        ]),
        'itemView' => '_view',
        'summaryText'=>'',
    )); ?>

</div>