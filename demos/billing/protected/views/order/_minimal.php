<?php
/* @var $this OrderController */
/* @var $data Order */
?>
<div class="col-md-6">
    <div class="thumbnail well order-checkout">
        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('nameprod')); ?>:</b>
        <?= Product::model()->productManufacturer($data->idprod) ?>
        <?php echo CHtml::encode($data->idprod0->nameprod); ?>
        <br/>

        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('priceprod')); ?>:</b>
        <?php echo CHtml::encode($data->idprod0->priceprod); ?> MDL
        <br/>

        <b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
        <?php echo CHtml::encode(Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->date, 'yyyy-MM-dd'), 'medium', null)); ?>
        <br/>

        <?php if (Product::productSale($data->idprod) != 0): ?>
            <b><?php echo CHtml::encode(Sales::model()->getAttributeLabel('sale')); ?>:</b>
            <?php echo Product::productSale($data->idprod); ?>%
            <br/>
        <?php endif; ?>

        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('priceprod')); ?>:</b>
        <?php if (Product::productSale($data->idprod) != 0): ?>
            <?php echo Product::productPriceWithSale($data->idprod); ?> MDL
            <strike><?php echo CHtml::encode($data->idprod0->priceprod); ?> MDL</strike>
        <?php else: ?>
            <?php echo CHtml::encode($data->idprod0->priceprod); ?> MDL
        <?php endif; ?>
        <br/>
    </div>
</div>