<?php
/* @var $this ManufacturerController */
/* @var $model Manufacturer */

$this->breadcrumbs=array(
	'Manufacturers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Manufacturer', 'url'=>array('index')),
	array('label'=>'Create Manufacturer', 'url'=>array('create')),
	array('label'=>'Update Manufacturer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Manufacturer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manufacturer', 'url'=>array('admin')),
);
?>

<h2><?php echo $model->name; ?></h2>

<?php
$dataProvider = new CActiveDataProvider('Product', [
	'criteria' => [
		'condition' => 'idmanufacturer=:idmanufacturer',
		'params'=>[
			':idmanufacturer'=>$model->id
		],
		'order'=>'nameprod ASC'
	]
]);
$this->widget('zii.widgets.CListView', array(
	'dataProvider' => $dataProvider,
	'itemView' => '../product/_view',
	'htmlOptions' => [
		'class' => 'row'
	],
	'summaryCssClass' => 'container-fluid text-right'
)); ?>
