<?php
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/7/16
 * Time: 2:02 PM
 */
$this->breadcrumbs = array(
    'I-RBAC' => array('index'),
    !isset($_GET['name']) ? 'Create task' : "Edit task",
);
?>
<?php if (isset($_GET['name'])): ?>
    <?= CHtml::link('Create task', ['irbac/task'], ['class' => 'btn btn-default']) ?>
    <h2>Edit task <b><?= $_GET["name"] ?></b>
        <small><?php
            $text = Yii::app()->authManager->getAuthItem($_GET["name"])->description;
            if (strlen($text))
                echo "(" . Yii::app()->authManager->getAuthItem($_GET["name"])->description . ")";
            ?>
        </small>
    </h2>
<?php else: ?>
    <h2>Create task</h2>
<?php endif; ?>
<div class="container-fluid">
    <?php $form = $this->beginWidget(
        'IActiveForm',
        [
            'htmlOptions' => array(
                'onsubmit' => 'return roleSubmit()',
            )
        ]
    ); ?>
    <?php if (empty($_GET['name'])): ?>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?= IHtml::label('Name ' . $error, 'name', ['encode' => false]) ?>
                    <input type="text" id="name" name="name" class="form-control"
                           value="<?= isset($data['name']) ? $data['name'] : ""; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= IHtml::label('Description', 'description') ?>
                    <input type="text" name="description" id="description" class="form-control">
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <?= IHtml::label('Bizrule', 'bizrule') ?>
                <input type="text" name="bizrule" id="bizrule" class="form-control" value="<?=isset($_GET['name'])?$item = Authitem::model()->findByPk($_GET['name'])->bizrule:""?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= IHtml::label('All operations', '') ?>
                <?php
                $this->widget('zii.widgets.grid.CGridView',
                    [
                        'afterAjaxUpdate' => 'function(){ selectRowsEvent(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => [
                            [
                                'class' => 'ICheckBoxColumn',
                                'selectableRows' => 2,
                                'checkBoxHtmlOptions' => [
                                    'class' => 'row-checkbox',
                                ],
                            ],
                            [
                                'name' => 'name',
                                'htmlOptions' => [
                                    'class' => 'select-name'
                                ]
                            ],
                            'description',
                        ],
                        'enablePagination' => true,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover table-condensed selectable',
                        'pager' => [
                            'class' => 'ILinkPager',
                        ],
                        'htmlOptions' => [
                            'class' => 'table-responsive'
                        ],
                        'pagerCssClass' => 'default',
                    ])
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="emptySpace"></div>
                <?= IHtml::label('Assigned operations', 'operations') ?>
                <?php
                $controllers = [];
                $actions = [];
                foreach (glob('./protected/controllers/*Controller.php') as $filename) {
                    $name = explode('/', $filename);
                    $controllers[] = str_replace("Controller.php", "", end($name));
                }
                ?>

                <div class="form-group">
                    <?php
                    $items = [];
                    if (isset($_GET['name'])) {
                        $myOperations = Authitemchild::model()->findAll([
                            'condition' => 'parent=:parent',
                            'params' => [
                                ':parent' => $_GET['name']
                            ]
                        ]);
                        foreach ($myOperations as $operation) {
                            if (!isset($items[explode(":", $operation->child)[0]]))
                                $items[explode(":", $operation->child)[0]] = [];
                            $items[explode(":", $operation->child)[0]][] = explode(":", $operation->child)[1];
                        }
                    }
                    echo "<select id='operationsTo' name='operations[]' class='form-control destination' multiple style='height: 536px'>";
                    foreach ($controllers as $controller) {
                        echo "<optgroup " . (!isset($items[$controller]) ? "style='display:none'" : "") . " label='$controller'>";
                        if (isset($items[$controller]))
                            foreach ($items[$controller] as $item) {
                                echo "<option value='$controller:$item'>$item</option>";
                            }
                        echo "</optgroup>";
                    }
                    ?>
                    </select>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?php
                            echo IHtml::button('REMOVE', [
                                'class' => 'btn btn-danger btn-block',
                                'onclick' => "removeOperation()"
                            ]);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <?php echo IHtml::submitButton(isset($_GET['name']) ? 'Update' : 'Create', [
                'name' => isset($_GET['name']) ? 'update' : 'create',
            ]); ?>
        </div>
    </div>
</div>
<?php $this->endWidget(); ?>
</div>
