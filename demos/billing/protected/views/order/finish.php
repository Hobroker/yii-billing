<?php
/* @var $this OrderController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Orders',
);

$this->menu = array(
    array('label' => 'Create Order', 'url' => array('create')),
    array('label' => 'Manage Order', 'url' => array('admin')),
);
?>
<h1>Get invoice</h1>
<h4>Invoice isn't approved yet! Wait until the manager will approve it.</h4>


<?php $this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_minimal',
)); ?>
<h4 class="right2">
    Total: <?= Product::productSumByUser(); ?> MDL
    <br>
    <br>
    <?= CHtml::button('Print',
        [
            'submit' => ['/'],
            'class' => 'right2'
        ]
    ); ?>
</h4>