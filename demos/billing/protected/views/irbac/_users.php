<?php
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/7/16
 * Time: 2:02 PM
 */
$this->breadcrumbs = array(
    'I-RBAC' => array('index'),
    'Assign role',
);
?>
<h2>Assign role</h2>

<div class="container-fluid col-md-4">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <div class="row">
        <div class="form-group">
            <?= IHtml::label('User', 'user') ?>
            <?php
            $users = User::model()->findAll();
            foreach ($users as $user) {
                $arr[$user->id] = $user->username;
            }
            echo IHtml::dropDownList('user', null, $arr);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?= IHtml::label('Role', 'role') ?>
            <?php
            $roles = Yii::app()->authManager->getRoles();
            foreach ($roles as $role) {
                if ($role->name != "")
                    $arr[$role->name] = $role->name;
            }
            echo IHtml::dropDownList('role', null, $arr, array('empty' => '(Select a role)'));
            ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Create', [
            'name' => 'create'
        ]); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>