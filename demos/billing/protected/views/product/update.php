<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	$model->idprod=>array('view','id'=>$model->idprod),
	'Update',
);

$this->menu=array(
	array('label'=>'List Products', 'url'=>array('index')),
	array('label'=>'Create Product', 'url'=>array('create')),
	array('label'=>'View Product', 'url'=>array('view', 'id'=>$model->idprod)),
	array('label'=>'Manage Products', 'url'=>array('admin')),
);
?>

<h2>Update Product <?php echo $model->idprod; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>