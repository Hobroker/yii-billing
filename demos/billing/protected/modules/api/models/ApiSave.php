<?php

/**
 * This is the model class for table "api_saves".
 *
 * The followings are the available columns in table 'api_saves':
 * @property string $id
 * @property string $module
 * @property string $model
 * @property string $action
 * @property string $url
 * @property string $params
 */
class ApiSave extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'api_saves';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('module, model, action, url', 'length', 'max' => 255),
            array('params', 'length', 'max' => 10240),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, module, model, action, url, params', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'module' => 'Module',
            'model' => 'Model',
            'action' => 'Action',
            'url' => 'Url',
            'params' => 'Parameters',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('module', $this->module, true);
        $criteria->compare('model', $this->model, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('params', $this->params, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ApiSave the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function getAllActions()
    {
        $ignore = [
            'Export',
            'Import',
            'Administration',
            'Index'
        ];
        $controller = 'DefaultController.php';
        $path = Yii::app()->basePath . "/modules/Api/controllers";
        $actions = [];
        if (file_exists($path . DIRECTORY_SEPARATOR . $controller)) {
            $source = file_get_contents($path . DIRECTORY_SEPARATOR . $controller);
            preg_match_all('/function (?:(actions)|action(\w+))\(/i', $source, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                if (!empty($match[1])) {
                    $actionsMethod = '';
                    $braces = 0;
                    $pos = stripos($source, 'function actions()') + strlen('function actions()');
                    while (($c = $source[++$pos]) !== '{') ;
                    do {
                        $c = $source[$pos++];
                        if ($c === '{')
                            $braces++;
                        elseif ($c === '}')
                            $braces--;
                        $actionsMethod .= $c;
                    } while ($braces > 0);

                    preg_match_all('/([\'"])(\w+)\1.*?class/si', $actionsMethod, $classes, PREG_SET_ORDER);

                    foreach ($classes as $class)
                        if (!in_array(ucfirst($class[2]), $ignore))
                            $actions[] = ucfirst($class[2]);
                } else
                    if (!in_array(ucfirst($match[2]), $ignore))
                        $actions[] = ucfirst($match[2]);
            }//foreach
        }//file_exists
        return $actions;
    }
}
