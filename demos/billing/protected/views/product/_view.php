<?php
/* @var $this ProductController */
/* @var $data Product/Order */
$idprod = isset($data->idprod) ? $data->idprod : $data->idprod0->idprod;
$nameprod = isset($data->nameprod) ? $data->nameprod : $data->idprod0->nameprod;
$idcateg = isset($data->idcateg) ? $data->idcateg : $data->idprod0->idcateg;
$priceprod = isset($data->priceprod) ? $data->priceprod : $data->idprod0->priceprod;
$idmanufacturer = isset($data->idmanufacturer) ? $data->idmanufacturer : $data->idprod0->idmanufacturer;
$namecateg = isset($data->idcateg0) ? $data->idcateg0->namecateg : $data->idprod0->idcateg0->namecateg;
$idmanufacturer = isset($data->idmanufacturer0) ? $data->idmanufacturer0->name : $data->idprod0->idmanufacturer0->name;

$small = isset($small) && $small;
?>

<div class="<?= $small ? '' : 'col-sm-4'; ?>">
    <div class="thumbnail <?= $small ? "" : "product" ?>">
        <div class="caption">
            <h4><?= CHtml::link(CHtml::encode($nameprod), ['product/view', 'id' => $idprod]); ?></h4>
            <h4 class="<?=$small?"":"text-right"?>">
                <?php if (Product::productSale($idprod) != 0): ?>
                    <span class="text-danger"><s><?php echo CHtml::encode($priceprod); ?> MDL</s></span>
                    <?php echo Product::productPriceWithSale($idprod); ?> MDL
                <?php else: ?>
                    <?php echo CHtml::encode($priceprod); ?> MDL
                <?php endif; ?>
            </h4>
            <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('idcateg')); ?>:</b>
            <?php echo CHtml::encode($namecateg); ?>
            <br/>

            <?php if (Product::productSale($idprod) != 0): ?>
                <b><?php echo CHtml::encode(Sales::model()->getAttributeLabel('sale')); ?>:</b>
                <?php echo Product::productSale($idprod); ?>%
                <br/>
            <?php endif; ?>

            <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('idmanufacturer')); ?>:</b>
            <?php echo $idmanufacturer; ?>
            <br/>

            <div class="text-right">
                <?= isset($buy) && !$buy ? "" : CHtml::button('Buy',
                    [
                        'submit' => array('product/buy'),
                        'params' => [
                            'idprod' => $idprod
                        ],
                        'confirm' => 'Buy this product?',
                        'class' => 'btn btn-primary btn-md text-right btn-bottom'
                    ]
                ); ?>
            </div>
        </div>
    </div>
</div>