<?php

/**
 * Created by PhpStorm.
 * User: urechean
 * Date: 25.05.2015
 * Time: 16:18
 */
class GetDocument
{

    public static function getList()
    {
        $models = array();

        if (isset(Yii::app()->modules['Documents'])) {
            $modelsDir = Yii::getPathOfAlias('application.modules.Documents.models');
            if (is_dir($modelsDir)) {
                foreach (scandir($modelsDir) as $key => $value) {
                    if (in_array($value, array('.', '..')) || is_dir($modelsDir . '/' . $value) || substr($value, -4) !== '.php' || substr($value, -11) == 'Version.php' || substr($value, -11) == 'History.php')
                        continue;
                    else {
                        $model_name = substr($value, 0, -4);
                        if (!in_array($model_name, array('GeneralClasificator', 'TableCodeClasificator', 'LimitAcces'))) {
                            try {
                                $models[] = array('name' => $model_name);
                            } catch (Exception $e) {
                                Response::sendResponse(500,
                                    ObjectEncoded::getObjectEncoded(
                                        'Document',
                                        array(
                                            "status" => "ERROR",
                                            "message" => $e->getMessage()
                                        )
                                    )
                                );
                            }
                        }
                    }
                }
            }
        }
        return $models;
    }

    public static function getStructure($model_name)
    {
        $arr = array();
        $structure = array();
        $relations = array();

        $model_attr = $model_name::model()->attributes;
        $model_attr_arr = $model_name::model()->formAttributes();

        foreach ($model_attr as $key => $attr) {
            $column = $model_name::model()->tableSchema->columns[$key];
            $visible = isset($model_attr_arr[$column->name]) ? true : false;
            $structure[] = array(
                "name" => $column->name,
                "allowNull" => $column->allowNull,
                "dbType" => $column->dbType,
                "defaultValue" => $column->defaultValue,
                "size" => $column->size,
                "precision" => $column->precision,
                "visible" => $visible,
            );
        }
        $model_rel = $model_name::model()->relations();


        foreach ($model_rel as $key => $related) {
            $visible = isset($model_attr_arr[$related[2]]) ? true : false;
            $description = array();
            $description['type'] = $related[0];
            $description['model'] = $related[1];
            $description['field'] = $related[2];
            $description['visible'] = $visible;
            if ($visible && $model_attr_arr[$related[2]]["type"] === "Clasificator")
                $description['model_field_view'] = $model_attr_arr[$related[2]]["attr_cls"];
            else
                $description['model_field_view'] = "";
            $relations[] = $description;
        }

        $use_as = $model_name::model()->useAs();

        $arr['use_as'] = $use_as;
        $arr['Data_type'] = $structure;
        $arr['Relations'] = $relations;
        return $arr;
    }

    public static function getData($model_name)
    {
        $model = new $model_name;
        $dataProviderPost = $model->search();
        $dataProviderPost->setPagination(
            array(
                'pageSize' => 999999,
                'pageVar' => 'page',
            )
        );
        $models = $dataProviderPost->getData();
        $arr = array();
        $criteria = new CDbCriteria();
        $permitedIds = [];
        foreach ($models as $element)
            if (RightsList::checkDocumentRight($element->document_id, 1)) // 1 is right to view doc
                $permitedIds[] = $element->document_id;

        $criteria->addInCondition('document_id', $permitedIds, 'AND');
        $models = $model_name::model()->findAll($criteria);
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr_elements["document_name"] = Document::model()->findByPk($arr_elements["document_id"])->title . "." .
                Document::model()->findByPk($arr_elements["document_id"])->fileFormat->extension;
            $arr[] = $arr_elements;
        }
        return $arr;
    }

    public static function getDataByPk($model_name, $id)
    {
        $models = $model_name::model()->findByPK($id);
        if (!RightsList::checkDocumentRight($models->document_id, 1)) // 1 is right to read doc
            Response::sendResponse(403);
        $arr = array();
        foreach ($models->attributes as $key => $element)
            $arr[$key] = $element;
        $arr["document_name"] = Document::model()->findByPk($models->document_id)->title . "." .
            Document::model()->findByPk($models->document_id)->fileFormat->extension;
        return $arr;
    }


    public static function getDataByAttributes($model_name)
    {
        $criteria = new CDbCriteria;

        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    $key_val = 97;
                foreach ($body_params as $key => $body_param) {
                    $elements = array();

                    foreach ($body_param as $param_name => $param_value) {
                        $elements[$param_name] = $param_value;
                    }

                    $criteria->addCondition("t." . $elements["column"] . " " . $elements["condition"] . " :" . chr($key_val));
                    $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $elements["value"]));
                    $key_val = $key_val + 1;
                }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        $documents = $model_name::model()->findAll($criteria);

        $permitedIds = [];
        foreach ($documents as $element)
            if (RightsList::checkDocumentRight($element->document_id, 1)) // 1 is right to view doc
                $permitedIds[] = $element->document_id;
        $criteria->addInCondition('document_id', $permitedIds, 'AND');
        $models = $model_name::model()->findAll($criteria);
        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr_elements["document_name"] = Document::model()->findByPk($arr_elements["document_id"])->title . "." .
                Document::model()->findByPk($arr_elements["document_id"])->fileFormat->extension;
            $arr[] = $arr_elements;
        }
        return $arr;
    }

} 
