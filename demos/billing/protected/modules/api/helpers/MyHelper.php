<?php

/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 28/04/16
 * Time: 12:39
 */
class MyHelper
{
    private static $ignore = ['create_user_id', 'create_datetime', 'update_user_id', 'update_datetime', 'id'];

    public static function decodeJSON($body = '[{}]')
    {
        if (strlen($body) && !(is_string($body) && is_array(json_decode($body, true)) && (json_last_error() == JSON_ERROR_NONE)))
            MyHelper::sendError("Invalid JSON!");
        $body_params = json_decode($body);
        $arr = [];
        if (isset($body_params) && !empty($body_params))
            foreach ($body_params as $key => $body_param)
                foreach ($body_param as $param_name => $param_value)
                    $arr[$param_name] = $param_value;
        return $arr;
    }

    public static function getAllModules()
    {
        $arr = [];
        foreach (glob('./protected/modules/*') as $filename)
            if (basename($filename) != 'gii')
                $arr[] = basename($filename);
        return $arr;
    }

    public static function getAllModels($module = "")
    {
        $arr = [];
        if ($module == "")
            return $arr;
        foreach (glob('./protected/modules/' . $module . '/models/*.php') as $filename) {
            $arr[] = str_replace(".php", "", basename($filename));
        }
        return $arr;
    }

    public static function sendError($message = "")
    {
        Response::sendResponse(500,
            ObjectEncoded::getObjectEncoded(
                'Document',
                array(
                    "status" => "ERROR",
                    "message" => $message
                )
            )
        );
    }

    public static function send404($message = "")
    {
        Response::sendResponse(404,
            ObjectEncoded::getObjectEncoded(
                'Document',
                array(
                    "status" => "ERROR",
                    "message" => $message
                )
            )
        );
    }

    public static function putExample($q = [])
    {
        if (isset($_POST['example']))
            Response::sendResponse(200,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "example" => [$q]
                    )
                )
            );
    }

    public static function sendOk($message = [])
    {
        Response::sendResponse(200,
            ObjectEncoded::getObjectEncoded(
                'Document',
                array(
                    "status" => "OK",
                    "response" => $message
                )
            )
        );
    }

    public static function validPk($pk = 0)
    {
        $pk = strval($pk);
        if (!ctype_digit($pk) || $pk > 9223372036854775807)
            MyHelper::sendError("Invalid Primary Key `$pk`!");
        return true;
    }

    public static function getColumns($model)
    {
        $columns = [];
        $model_attr = $model::model()->attributes;
        foreach ($model_attr as $key => $attr)
            $columns[] = $model::model()->tableSchema->columns[$key]->name;
        return $columns;
    }

    public static function getColumnRules($model, $col)
    {
        $model_attr = $model::model()->attributes;
        foreach ($model_attr as $key => $attr) {
            $q = $model::model()->tableSchema->columns[$key];
            if ($q->name == $col && !in_array($col, self::$ignore)) {
                return [
                    'column' => $q->name,
                    'allowNull' => $q->allowNull,
                    'dbType' => $q->dbType,
                    'size' => $q->size
                ];
            }
        }

        return ['error'];
    }

    public static function getColumnsNotNull($model)
    {
        $arr = [];
        $model_attr = $model::model()->attributes;
        foreach ($model_attr as $key => $attr) {
            $q = $model::model()->tableSchema->columns[$key];
            if ($q->name == 'id') continue;
            if (!$q->allowNull) {
                $arr[] = $q->name;
            }
        }

        return $arr;
    }

    public static function is_col_ok($model, $key)
    {
        return MyHelper::getColumnRules($model, $key);
    }

    public static function getTemplatePdf($template_id, $state = 'uploaded_doc')
    {
        $template = DocumentsTemplatesPdf::model()->findByPk((int)$template_id);
        $template_file = Storage::model()->findByPk(Storage::TEMPLATES_STORAGE_ID)->path . $template->saved_name;
        if (is_file($template_file)) {
            $filePath = Storage::model()->findByPk(Storage::APP_STORAGE_ID)->path . md5($state) . '_' . $template->saved_name;
            if (Yii::app()->user->hasState($state)) {
                $uploaded_doc = Yii::app()->user->getState($state);
                if (isset($uploaded_doc['path']) && file_exists($uploaded_doc['path']))
                    @unlink($uploaded_doc['path']);
            }
            $uploaded_doc = array('path' => $filePath, 'type' => 'template_pdf', 'template_id' => $template->id);
            HelpersStorage::MoveFile($template_file, $filePath);
            Yii::app()->user->setState($state, $uploaded_doc);
            return $filePath;
        }
        MyHelper::sendError("No template found!");
    }

    public static function getTemplate($template_id, $state = 'uploaded_doc')
    {
        $template = DocumentsTemplates::model()->findByPk((int)$template_id);
        if (is_null($template))
            MyHelper::sendError("No template found!");
        $template_file = Storage::model()->findByPk(Storage::TEMPLATES_STORAGE_ID)->path . $template->saved_name;
        if (is_file($template_file)) {
            $filePath = Storage::model()->findByPk(Storage::APP_STORAGE_ID)->path . md5($state) . '_' . $template->saved_name;
            if (Yii::app()->user->hasState($state)) {
                $uploaded_doc = Yii::app()->user->getState($state);
                if (isset($uploaded_doc['path']) && file_exists($uploaded_doc['path']))
                    @unlink($uploaded_doc['path']);
            }
            $uploaded_doc = array('path' => $filePath, 'type' => 'template');
            HelpersStorage::MoveFile($template_file, $filePath);
            Yii::app()->user->setState($state, $uploaded_doc);
            return $filePath;
        } else {
            MyHelper::sendError("No template found!");
        }
    }

    public static function getTemplateSpecial($state = 'uploaded_doc')
    {
        $file_name = 'file.png';
        $special_file = Yii::getPathOfAlias('application.modules.Documents.data') . DIRECTORY_SEPARATOR . $file_name;
        if (is_file($special_file)) {
            $filePath = Storage::model()->findByPk(Storage::APP_STORAGE_ID)->path . time() . '_' . rand() . '.' . pathinfo($special_file, PATHINFO_EXTENSION);
            //setam in cache imaginele incarcate pentru user
            if (Yii::app()->user->hasState($state)) {
                $uploaded_doc = Yii::app()->user->getState($state);
                if (isset($uploaded_doc['path']) && file_exists($uploaded_doc['path']))
                    @unlink($uploaded_doc['path']);
            }
            $uploaded_doc = array('path' => $filePath, 'type' => 'special');
            HelpersStorage::MoveFile($special_file, $filePath);
            Yii::app()->user->setState($state, $uploaded_doc);
            return $filePath;
        }
        MyHelper::sendError("No template found!");
    }

    public static function getTemplateFile($state = 'uploaded_doc')
    {
        //die(var_dump($_FILES));
        $targetDir = Storage::model()->findByPk(Storage::APP_STORAGE_ID)->path;//'./storage/plupload';
        if (isset($_FILES['uploaded_file'])) {
            $image = CUploadedFile::getInstanceByName('uploaded_file');
            //$prev_name = $image->getName();
            $file_name = md5(time() . rand()) . '_' . time() . '.' . $image->extensionName;
            $filePath = $targetDir . $file_name;
            $image->saveAs($filePath);

            if (Yii::app()->user->hasState($state)) {
                $uploaded_doc = Yii::app()->user->getState($state);
                if (isset($uploaded_doc['path']) && file_exists($uploaded_doc['path']))
                    @unlink($uploaded_doc['path']);
            }
            $uploaded_doc = array('path' => $filePath, 'type' => 'file');
            Yii::app()->user->setState($state, $uploaded_doc);

            return $filePath;
        }
        MyHelper::sendError("No file specified!");
    }

    public static function getStructure($model)
    {
        $arr = array();
        $structure = array();

        if (!method_exists($model::model(), 'formAttributes')) {
            return $arr;
        }
        $model_attr = $model::model()->attributes;
        $model_attr_arr = $model::model()->formAttributes();
        foreach ($model_attr as $key => $attr) {
            $column = $model::model()->tableSchema->columns[$key];
            $visible = isset($model_attr_arr[$column->name]) ? true : false;
            $structure[] = array(
                "name" => $column->name,
                "allowNull" => $column->allowNull,
                "dbType" => $column->dbType,
                "defaultValue" => $column->defaultValue,
                "size" => $column->size,
                "precision" => $column->precision,
                "visible" => $visible,
            );
        }

        return $structure;
    }

    public static function getRelations($model)
    {
        $relations = [];
        $model_rel = $model::model()->relations();
        foreach ($model_rel as $key => $related) {
            $visible = isset($model_attr_arr[$related[2]]) ? true : false;
            $description = array();
            $description['type'] = $related[0];
            $description['model'] = $related[1];
            $description['field'] = $related[2];
            $description['visible'] = $visible;
            if ($visible && $model_attr_arr[$related[2]]["type"] === "Clasificator")
                $description['model_field_view'] = $model_attr_arr[$related[2]]["attr_cls"];
            elseif ($visible && $model_attr_arr[$related[2]]["type"] === "DropDownList")
                $description['model_field_view'] = $model_attr_arr[$related[2]]["from_attr"];
            else
                $description['model_field_view'] = "";
            $relations[] = $description;
        }
        return $relations;
    }
}