<?php
/* @var $this SaleController */
/* @var $model Sales */

$this->breadcrumbs=array(
	'Sales'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Sales', 'url'=>array('index')),
	array('label'=>'Manage Sales', 'url'=>array('admin')),
);
?>

<h2>Create a sale</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>