<?php

/**
 * Created by PhpStorm.
 * User: urechean
 * Date: 25.05.2015
 * Time: 16:18
 */
class GetClassificator
{

    public static function getList()
    {
        if (!isset($_GET['login']))
            Response::sendResponse(203,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => "Parameter <b>login</b> is missing !"
                    )
                )
            );
        else
            $login = strtolower($_GET['login']);

        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), true);
        $user_api = User::model()->find($criteria);

        $roles = Yii::app()->db->createCommand()
            ->select('itemname')
            ->from('authassignment')
            ->where('userid=:id', array(':id' => $user_api->id))
            ->queryAll();

        $arr_role = array();
        foreach ($roles as $role)
            $arr_role[] = $role["itemname"];

        $criteria = new CDbCriteria();
        $criteria->addInCondition('rule', $arr_role);

        $arr_models = LimitAcces::model()->findAll($criteria);

        $models = array();

        if ($login != "sa") {
            foreach ($arr_models as $arr_model)
                $models[] = array('id' => $arr_model->id, 'name' => $arr_model->name);
        } else
            if (isset(Yii::app()->modules['Clasificator'])) {
                $modelsDir = Yii::getPathOfAlias('application.modules.Clasificator.models');
                if (is_dir($modelsDir)) {
                    $i = 0;
                    foreach (scandir($modelsDir) as $key => $value) {
                        if (in_array($value, array('.', '..')) || is_dir($modelsDir . '/' . $value) || substr($value, -4) !== '.php')
                            continue;
                        else {
                            $model_name = substr($value, 0, -4);
                            try {
                                $models[] = array('id' => $i, 'name' => $model_name);
                                $i++;
                            } catch (Exception $e) {
                                Response::sendResponse(500,
                                    ObjectEncoded::getObjectEncoded(
                                        'Document',
                                        array(
                                            "status" => "ERROR",
                                            "message" => $e->getMessage()
                                        )
                                    )
                                );
                            }
                        }
                    }
                }
            }
        return $models;
    }

    public static function getListByAttributes()
    {
        if (!isset($_GET['login']))
            Response::sendResponse(203,
                ObjectEncoded::getObjectEncoded(
                    'Document',
                    array(
                        "status" => "ERROR",
                        "message" => "Parameter <b>login</b> is missing !"
                    )
                )
            );
        else
            $login = strtolower($_GET['login']);

        $criteria = new CDbCriteria;
        $criteria->compare('upper(username)', strtoupper($login), true);
        $user_api = User::model()->find($criteria);

        $roles = Yii::app()->db->createCommand()
            ->select('itemname')
            ->from('authassignment')
            ->where('userid=:id', array(':id' => $user_api->id))
            ->queryAll();

        $arr_role = array();
        foreach ($roles as $role)
            $arr_role[] = $role["itemname"];

        $criteria = new CDbCriteria();
        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        $criteria->addInCondition('rule', $arr_role);
        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    $key_val = 97;
                foreach ($body_params as $key => $body_param) {
                    $elements = array();

                    foreach ($body_param as $param_name => $param_value) {
                        $elements[$param_name] = $param_value;
                    }

                    $criteria->addCondition("t." . $elements["column"] . " " . $elements["condition"] . " :" . chr($key_val));
                    $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $elements["value"]));
                    $key_val = $key_val + 1;
                }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }
        $arr_models = LimitAcces::model()->findAll($criteria);
        $arr_role = array();
        $models = array();

        if ($login != "sa") {
            foreach ($arr_models as $arr_model)
                $models[] = array('id' => $arr_model->id, 'name' => $arr_model->name);
        } else
            if (isset(Yii::app()->modules['Clasificator'])) {
                $modelsDir = Yii::getPathOfAlias('application.modules.Clasificator.models');
                if (is_dir($modelsDir)) {
                    $dir_data = scandir($modelsDir);
                    $i = 0;
                    foreach (scandir($modelsDir) as $key => $value) {
                        if (in_array($value, array('.', '..')) || is_dir($modelsDir . '/' . $value) || substr($value, -4) !== '.php')
                            continue;
                        else {
                            $model_name = substr($value, 0, -4);
                            try {
                                $models[] = array('id' => $i, 'name' => $model_name);
                                $i++;
                            } catch (Exception $e) {
                                Response::sendResponse(500,
                                    ObjectEncoded::getObjectEncoded(
                                        'Document',
                                        array(
                                            "status" => "ERROR",
                                            "message" => $e->getMessage()
                                        )
                                    )
                                );
                            }
                        }
                    }
                }
            }
        return $models;
    }

    public static function getStructure($model_name)
    {
        $arr = array();

        $model_attr = $model_name::model()->attributes;
        foreach ($model_attr as $key => $attr) {
            $column = $model_name::model()->tableSchema->columns[$key];
            $arr[] = array(
                "name" => $column->name,
                "allowNull" => $column->allowNull,
                "dbType" => $column->dbType,
                "defaultValue" => $column->defaultValue,
                "size" => $column->size,
                "precision" => $column->precision
            );
        }

        return $arr;
    }

    public static function getData($model_name)
    {
        $model = new $model_name;
        $dataProviderPost = $model->search();
        $dataProviderPost->setPagination(
            array(
                'pageSize' => 999999,
                'pageVar' => 'page',
            )
        );
        $models = $dataProviderPost->getData();

        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr[] = $arr_elements;
        }
        return $arr;
    }

    public static function getDataByPk($model_name, $id)
    {
        $models = $model_name::model()->findByPK($id);
        $arr = array();
        foreach ($models->attributes as $key => $element)
            $arr[$key] = $element;
        return $arr;
    }


    public static function getDataByAttributes($model_name)
    {
        $criteria = new CDbCriteria;

        $body = file_get_contents("php://input");
        $content_type = false;
        if (isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }

        switch ($content_type) {
            case "application/json":
                $body_params = json_decode($body);

                if (isset($body_params) && !empty($body_params))
                    $key_val = 97;
                foreach ($body_params as $key => $body_param) {
                    $elements = array();

                    foreach ($body_param as $param_name => $param_value) {
                        $elements[$param_name] = $param_value;
                    }

                    $criteria->addCondition("t." . $elements["column"] . " " . $elements["condition"] . " :" . chr($key_val));
                    $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $elements["value"]));
                    $key_val = $key_val + 1;
                }
                break;
            default:
                Response::sendResponse(500,
                    ObjectEncoded::getObjectEncoded(
                        'Document',
                        array(
                            "status" => "ERROR",
                            "message" => 'Content type <b>' . $content_type . '</b> is not accept'
                        )
                    )
                );
                break;
        }

        $models = $model_name::model()->findAll($criteria);
        $arr = array();
        foreach ($models as $elements) {
            $arr_elements = array();
            foreach ($elements->attributes as $key => $element)
                $arr_elements[$key] = $element;
            $arr[] = $arr_elements;
        }
        return $arr;
    }

} 