<?php

/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 28/04/16
 * Time: 09:33
 */
class Universal
{
    public static function getModuleList()
    {
        MyHelper::putExample([
            'list' => 'active'
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['list']))
            $list = strtolower($data['list']);
        else
            $list = "";
        $arr = [];
        if ($list == "active") {
            $modules = Yii::app()->getModules();
            foreach ($modules as $key => $module)
                if ($key != 'gii')
                    $arr[] = [
                        'active' => true,
                        'name' => $key
                    ];
        } elseif ($list == "inactive") {
            $arr0 = [];
            $allModules = MyHelper::getAllModules();
            $activeModules = Yii::app()->getModules();
            foreach ($activeModules as $key => $module)
                $arr0[] = $key;
            foreach ($allModules as $module)
                if (!in_array($module, $arr0))
                    $arr[] = [
                        'active' => false,
                        'name' => $module
                    ];
        } elseif (in_array($list, ['all', ''])) {
            $arr0 = [];
            $modules = Yii::app()->getModules();
            foreach ($modules as $key => $module)
                $arr0[] = $key;
            foreach (glob('./protected/modules/*') as $filename)
                $arr[] = [
                    'active' => in_array(basename($filename), $arr0),
                    'name' => basename($filename)
                ];
        } else
            MyHelper::sendError("Invalid `list` value!");
        return $arr;
    }

    public static function getModelList()
    {
        MyHelper::putExample([
            'module' => 'Clasificator'
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (!in_array($module, MyHelper::getAllModules()))
                MyHelper::sendError("Module `$module` doesn't exist!");
        } else
            MyHelper::sendError("Module name not specified!");
        $arr = [];
        $models = MyHelper::getAllModels($module);
        foreach ($models as $filename) {
            try {
                $model = str_replace(".php", "", basename($filename));
                $arr[] = [
                    'name' => $model,
                    'Data_type' => MyHelper::getStructure($model),
                    'Relations' => MyHelper::getRelations($model),
                ];
                if($module == "Documents") {
                    $arr[count($arr) - 1]['use_as'] = $model::model()->useAs();
                }
            } catch (Exception $e) {

            }
        }
        return $arr;
    }

    public static function getModelStructure()
    {
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                $model = $data['model'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        return MyHelper::getStructure($model);
    }

    public static function getModelData()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect'
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (isset($data['pk']))
                    $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $columns = MyHelper::getColumns($model);
        $arr = [];

        if (isset($pk)) {
            foreach ($pk as $id) {
                if (!MyHelper::validPk($id))
                    MyHelper::sendError("");
                $data = $model::model()->findByPk($id);
                if (is_null($data))
                    $arr[] = ['error' => "Invalid Primary Key `$id`"];
                else {
                    $arr[] = [];
                    foreach ($columns as $column) {
                        $arr[count($arr) - 1][$column] = $data->$column;
                        if ($column == 'document_id') {
                            $arr[count($arr) - 1]['model_name'] = Document::model()->findByPk($item->document_id)->type->instance_model_name;
                            $arr[count($arr) - 1]['document_name'] = Document::model()->findByPk($item->document_id)->title;
                        }
                    }
                }
            }
        } else {
            $data = $model::model()->findAll();
            foreach ($data as $item) {
                $arr[] = [];
                foreach ($columns as $column) {
                    $arr[count($arr) - 1][$column] = $item->$column;
                    if ($column == 'document_id') {
                        $arr[count($arr) - 1]['model_name'] = Document::model()->findByPk($item->document_id)->type->instance_model_name;
                        $arr[count($arr) - 1]['document_name'] = Document::model()->findByPk($item->document_id)->title;
                    }
                }
            }
        }
        return $arr;
    }

    public static function getModelDataByAttributes()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'criteria' => [
                [
                    'column' => 'name',
                    'value' => 'test',
                    'condition' => '='
                ]
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
//        die(var_dump($data));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (isset($data['criteria']))
                    $in = $data['criteria'];
                else
                    MyHelper::sendError("Attributes not specified!");
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        $columns = MyHelper::getColumns($model);
        $criteria = new CDbCriteria;
        $arr = [];
        $key_val = 97;
        foreach ($in as $item) {
            if (empty($item->column) || !is_string($item->column))
                MyHelper::sendError("Column not specified!");
            elseif (!in_array($item->column, MyHelper::getColumns($model)))
                MyHelper::sendError("Column `$item->column` doesn't exist!");
            elseif (empty($item->condition))
                MyHelper::sendError("Condition not specified!");
            elseif (!isset($item->value))
                MyHelper::sendError("Value not specified!");
            $criteria->addCondition("t." . $item->column . " " . $item->condition . " :" . chr($key_val));
            $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $item->value));
            $key_val++;
        }

        try {
            $data = $model::model()->findAll($criteria);
            foreach ($data as $item) {
                $arr[] = [];
                foreach ($columns as $column) {
                    $arr[count($arr) - 1][$column] = $item->$column;
                    if ($column == 'document_id') {
                        $arr[count($arr) - 1]['model_name'] = Document::model()->findByPk($item->document_id)->type->instance_model_name;
                        $arr[count($arr) - 1]['document_name'] = Document::model()->findByPk($item->document_id)->title;
                    }
                }
            }
        } catch (Exception $e) {
            MyHelper::sendError("Check model structure!");
        }
        return $arr;
    }

    public static function putModelData()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'data' => [
                [
                    'name' => 'test'
                ],
                [
                    'name' => 'test2'
                ]
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        $arr = [];
        foreach ($from as $row) {
            $to = new $data['model'];
            $inCols = [];
            foreach ($row as $key => $value) {
                if (in_array($key, MyHelper::getColumns($model)))
                    $to->$key = $value;
                else
                    MyHelper::sendError("Column `$key` doesn't exist!");
                $inCols[] = $key;
            }
            if ($to->save())
                $arr[] = [
                    'id' => $to->id,
                    'insert' => true
                ];
            else {
                $arr[] = [
                    "insert" => false,
                    "error" => "Check model structure!"
                ];
            }
        }
        return $arr;
    }

    public static function deleteDataByPk()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'pk' => [1, 2]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['pk']))
                    MyHelper::sendError("Primary Key not specified!");
                $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        $arr = [];
        foreach ($pk as $item) {
            if (MyHelper::validPk($item))
                $to = $model::model()->findByPk($item);
            if (!is_null($to))
                if ($to->delete()) {
                    $arr[] = [
                        'id' => $item,
                        'delete' => true
                    ];
                    continue;
                }
            $arr[] = [
                'id' => $item,
                'delete' => false,
                'message' => "Inexistent PK!"
            ];
        }
        return $arr;
    }

    public static function deleteDataByAttributes()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'criteria' => [
                'column' => 'name',
                'value' => 'test',
                'condition' => '='
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (isset($data['criteria']))
                    $in = $data['criteria'];
                else
                    MyHelper::sendError("Attributes not specified!");
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        $criteria = new CDbCriteria;
        $arr = [];
        $key_val = 97;
        foreach ($in as $item) {
            if (empty($item->column) || !is_string($item->column))
                MyHelper::sendError("Column not specified!");
            elseif (!in_array($item->column, MyHelper::getColumns($model)))
                MyHelper::sendError("Column `$item->column` doesn't exist!");
            elseif (empty($item->condition))
                MyHelper::sendError("Condition not specified!");
            elseif (empty($item->value))
                MyHelper::sendError("Value not specified!");
            $criteria->addCondition("t." . $item->column . " " . $item->condition . " :" . chr($key_val));
            $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $item->value));
            $key_val++;
        }
        try {
            $data = $model::model()->findAll($criteria);
            foreach ($data as $item) {
                $arr[] = [
                    'id' => $item->id,
                    'delete' => $item->delete()
                ];
            }
        } catch (Exception $e) {
            MyHelper::sendError("Check model structure!");
        }
        return $arr;
    }

    public static function updateDataByPk()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'data' => [
                [
                    'pk' => 1,
                    'name' => 'test'
                ],
                [
                    'pk' => 2,
                    'name' => 'test2'
                ]
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $arr = [];
        foreach ($from as $item) {
            if (empty($item->pk)) {
                $arr[] = [
                    'update' => false,
                    'error' => 'Pk not specified!'
                ];
                continue;
            }
            if (MyHelper::validPk($item->pk))
                $to = $model::model()->findByPk($item->pk);
            if (!is_null($to)) {
                foreach ($item as $key => $val) {
                    if ($key != 'pk') {
                        $to->{$key} = $val;
                    }
                }
                $arr[] = [
                    'id' => $item->pk,
                    'update' => $to->update()
                ];
                continue;
            }
            $arr[] = [
                'id' => $item->pk,
                'update' => false
            ];
        }
        return $arr;
    }

    public static function updateDataByAttributes()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'data' => [
                'name' => 'test'
            ],
            'criteria' => [
                'column' => 'name',
                'value' => 'test',
                'condition' => '='
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (isset($data['criteria']))
                    $in = $data['criteria'];
                else
                    MyHelper::sendError("Attributes not specified!");
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");

        $criteria = new CDbCriteria;
        $arr = [];
        $key_val = 97;
        foreach ($in as $item) {
            if (empty($item->column) || !is_string($item->column))
                MyHelper::sendError("Column not specified!");
            elseif (!in_array($item->column, MyHelper::getColumns($model)))
                MyHelper::sendError("Column `$item->column` doesn't exist!");
            elseif (empty($item->condition))
                MyHelper::sendError("Condition not specified!");
            elseif (empty($item->value))
                MyHelper::sendError("Value not specified!");
            $criteria->addCondition("t." . $item->column . " " . $item->condition . " :" . chr($key_val));
            $criteria->params = array_merge($criteria->params, array(':' . chr($key_val) => $item->value));
            $key_val++;
        }
        try {
            $data = $model::model()->findAll($criteria);
            foreach ($data as $item) {
                foreach ($from as $key => $val) {
                    $item->{$key} = $val;
                }
                $arr[] = [
                    'id' => $item->id,
                    'updated' => $item->update()
                ];
            }
        } catch (Exception $e) {
            MyHelper::sendError("Check model structure!");
        }
        return $arr;
    }

    public static function putPdf()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (isset($data['template']))
                    $templateName = $data['template'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = new $modelName;
        $arr = [];
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));  //die(var_dump($dt));
        $dt_templates = DocumentsTypesTemplatesPdfLink::model()->findAllByAttributes(array('type_id' => $dt->id));
        $in = [];
        foreach ($dt_templates as $dt_template)
            $in[] = $dt_template->template_id;
        $templates = DocumentsTemplatesPdf::model()->findAllByPk($in);
        $out = [];
        foreach ($templates as $t)
            $out[$t->id] = $t->name;
        if (isset($templateName)) {
            if (!in_array($templateName, $out))
                MyHelper::sendError("Invalid template!");
            $template_id = array_search($templateName, $out);
        } else {
            $templateName = reset($out);
            $template_id = key($out);
        }
        if (count($model->useAs()) == 1 && in_array("template_pdf", $model->useAs())) {
            foreach ($from as $key => $value)
                if (in_array($key, MyHelper::getColumns($model)))
                    $model->$key = $value;
                else
                    MyHelper::sendError("Column `$key` doesn't exist!");
            $model->document_id = MyHelper::getTemplatePdf($template_id);
            $code = DocumentsTemplatesPdf::model()->findByPk($template_id)->file_content;
            $newCode = HelperTemplates::replaceTokensPdf($code, $model);
            $pdf = Yii::createComponent('application.modules.Documents.extensions.tcpdf.ETcPdf', 'P', 'mm', 'A4', true, 'ISO-8859-1', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->SetFont('dejavusans', 'N', 10);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);
            $pdf->AddPage();
            $pdf_data = str_replace('&nbsp;', ' ', $newCode);
            $pdf->writeHTML($pdf_data, true, false, false, false, '');
            $pdf->Output($model->document_id, "F");
            if (isset($model->document_id) && !is_numeric($model->document_id)) {
                $model->document_id = HelperSystem::remoteCreateDocument($modelName, $model->document_id);
            }
            if ($model->save()) {
                $arr[] = [
                    'save' => true,
                    'template' => $templateName,
                    'id' => $model->id
                ];
                HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($model->document_id));
                Document::model()->updateByPk($model->document_id, array("instance_id" => $model->id));
            } else {
                $arr[] = [
                    'save' => false,
                    'validate' => $model->validate(),
                    'data' => $from
                ];
            }
        } else
            MyHelper::sendError("Check model structure!");
        return $arr;
    }

    public static function putDoc()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (isset($data['template']))
                    $templateName = $data['template'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = new $modelName;
        $arr = [];
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        $dt_templates = DocumentsTypesTemplatesLink::model()->findAllByAttributes(array('type_id' => $dt->id));
        $in = [];
        foreach ($dt_templates as $dt_template)
            $in[] = $dt_template->template_id;
        $templates = DocumentsTemplates::model()->findAllByPk($in);
        $out = [];
        foreach ($templates as $t)
            $out[$t->id] = $t->name;
        if (isset($templateName)) {
            if (!in_array($templateName, $out))
                MyHelper::sendError("Invalid template!");
            $template_id = array_search($templateName, $out);
        } else {
            $templateName = reset($out);
            $template_id = key($out);
        }
        $model->document_id = MyHelper::getTemplate($template_id);
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($model)))
                $model->$key = $value;
            else
                MyHelper::sendError("Column `$key` doesn't exist!");
        HelperTemplates::replaceTokens($model->document_id, $model);
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteCreateDocument($modelName, $model->document_id);
        }
        if ($model->save()) {
            $arr[] = [
                'save' => true,
                'template' => $templateName,
                'id' => $model->id
            ];
            HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($model->document_id));
            Document::model()->updateByPk($model->document_id, array("instance_id" => $model->id));
        } else {
            $arr[] = [
                'save' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function putFile()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ]
        ]);
        if (empty($_POST['0']))
            MyHelper::sendError("Empty JSON!");
        $data = MyHelper::decodeJSON($_POST['0']);
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = new $modelName;
        $arr = [];
        $extensions = array();
        $max_file_size = 1;
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        if (!is_null($dt)) {
            $cat = DocumentCategory::model()->findByPk($dt->category_id);
            if (!is_null($cat)) {
                $ext_models = FileFormat::model()->findAllByPk(explode(',', $cat->extensions));
                if (!empty($ext_models))
                    foreach ($ext_models as $ext_model)
                        $extensions[] = '.' . $ext_model->extension;
                $max_file_size = $cat->file_size;
            }
        }
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($model)))
                $model->$key = $value;
            else
                MyHelper::sendError("Column `$key` doesn't exist!");
        $model->document_id = MyHelper::getTemplateFile();
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteCreateDocument($modelName, $model->document_id);
        }
        if ($model->save()) {
            $arr[] = [
                'save' => true,
                'id' => $model->id
            ];
            HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($model->document_id));
            Document::model()->updateByPk($model->document_id, array("instance_id" => $model->id));
        } else {
            $arr[] = [
                'save' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function putSpecial()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ]
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = new $modelName;
        $arr = [];
        $extensions = array();
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        if (!is_null($dt)) {
            $cat = DocumentCategory::model()->findByPk($dt->category_id);
            if (!is_null($cat)) {
                $ext_models = FileFormat::model()->findAllByPk(explode(',', $cat->extensions));
                if (!empty($ext_models))
                    foreach ($ext_models as $ext_model)
                        $extensions[] = '.' . $ext_model->extension;
            }
        }
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($model)))
                $model->$key = $value;
            else
                MyHelper::sendError("Column `$key` doesn't exist!");
        $model->document_id = MyHelper::getTemplateSpecial();
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteCreateDocument($modelName, $model->document_id);
        }
        if ($model->save()) {
            $arr[] = [
                'save' => true,
                'id' => $model->id
            ];
            HelpersStorage::MoveFileToOperativ(Document::model()->findByPk($model->document_id));
            Document::model()->updateByPk($model->document_id, array("instance_id" => $model->id));
        } else {
            $arr[] = [
                'save' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function updatePdfByPk()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ],
            'pk' => 12
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (isset($data['template']))
                    $templateName = $data['template'];
                if (empty($data['pk']))
                    MyHelper::sendError("Primary Key not specified!");
                $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = $modelName::model()->findByPk($pk);
        if (!RightsList::checkDocumentRight($model->document_id, 2)) // 2 is the right to write doc
            MyHelper::sendError("No rights!");
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($model)))
                $model->$key = $value;
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));  //die(var_dump($dt));
        $dt_templates = DocumentsTypesTemplatesPdfLink::model()->findAllByAttributes(array('type_id' => $dt->id));
        $in = [];
        foreach ($dt_templates as $dt_template)
            $in[] = $dt_template->template_id;
        $templates = DocumentsTemplatesPdf::model()->findAllByPk($in);
        $out = [];
        foreach ($templates as $t)
            $out[$t->id] = $t->name;
        if (isset($templateName)) {
            if (!in_array($templateName, $out))
                MyHelper::sendError("Invalid template!");
            $template_id = array_search($templateName, $out);
        } else {
            $templateName = reset($out);
            $template_id = key($out);
        }
        $model->document_id = MyHelper::getTemplatePdf($template_id);
        if (is_null($model))
            MyHelper::sendError("Inexistent Primary Key!");
        $code = DocumentsTemplatesPdf::model()->findByPk($template_id)->file_content;
        $newCode = HelperTemplates::replaceTokensPdf($code, $model);
        $pdf = Yii::createComponent('application.modules.Documents.extensions.tcpdf.ETcPdf', 'P', 'mm', 'A4', true, 'ISO-8859-1', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->SetFont('dejavusans', 'N', 10);
        $pdf->SetPrintHeader(false);
        $pdf->SetPrintFooter(false);
        $pdf->AddPage();
        $pdf_data = str_replace('&nbsp;', ' ', $newCode);
        $pdf->writeHTML($pdf_data, true, false, false, false, '');
        $pdf->Output($model->document_id, "F");
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteUpdateDocument($modelName::model()->findByPk($model->id)->document_id, $model->document_id);
        }
        if ($model->update()) {
            $arr[] = [
                'update' => true,
                'template' => $templateName,
                'id' => $model->id
            ];
        } else {
            $arr[] = [
                'update' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function updateDocByPk()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ],
            'pk' => 12
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (isset($data['template']))
                    $templateName = $data['template'];
                if (empty($data['pk']))
                    MyHelper::sendError("Primary Key not specified!");
                $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = $modelName::model()->findByPk($pk);
        if (is_null($model))
            MyHelper::sendError("Inexistent Primary Key!");
        $arr = [];
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        $dt_templates = DocumentsTypesTemplatesLink::model()->findAllByAttributes(array('type_id' => $dt->id));
        $in = [];
        foreach ($dt_templates as $dt_template)
            $in[] = $dt_template->template_id;
        $templates = DocumentsTemplates::model()->findAllByPk($in);
        $out = [];
        foreach ($templates as $t)
            $out[$t->id] = $t->name;
        if (isset($templateName)) {
            if (!in_array($templateName, $out))
                MyHelper::sendError("Invalid template!");
            $template_id = array_search($templateName, $out);
        } else {
            $templateName = reset($out);
            $template_id = key($out);
        }
        $model->document_id = MyHelper::getTemplate($template_id);
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($model)))
                $model->$key = $value;
        HelperTemplates::replaceTokens($model->document_id, $model);
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteUpdateDocument($modelName::model()->findByPk($model->id)->document_id, $model->document_id);
        }
        if ($model->update()) {
            $arr[] = [
                'update' => true,
                'template' => $templateName,
                'id' => $model->id
            ];
        } else {
            $arr[] = [
                'update' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function updateFile()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ],
            'pk' => 12
        ]);
        $data = MyHelper::decodeJSON($_POST['0']);
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (empty($data['pk']))
                    MyHelper::sendError("Primary Key not specified!");
                $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = $modelName::model()->findByPk($pk);
        if (is_null($model))
            MyHelper::sendError("Inexistent Primary Key!");
        $arr = [];
        $extensions = array();
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        if (!is_null($dt)) {
            $cat = DocumentCategory::model()->findByPk($dt->category_id);
            if (!is_null($cat)) {
                $ext_models = FileFormat::model()->findAllByPk(explode(',', $cat->extensions));
                if (!empty($ext_models))
                    foreach ($ext_models as $ext_model)
                        $extensions[] = '.' . $ext_model->extension;
            }
        }
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($modelName)))
                $model->$key = $value;
        if (isset($_FILES['file'])) {
            $model->document_id = MyHelper::getTemplateFile();
        }
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteUpdateDocument($modelName::model()->findByPk($model->id)->document_id, $model->document_id);
        }
        if ($model->update()) {
            $arr[] = [
                'update' => true,
                'id' => $model->id
            ];
        } else {
            $arr[] = [
                'update' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function updateSpecial()
    {
        MyHelper::putExample([
            'module' => 'Documents',
            'model' => 'Test',
            'data' => [
                'name' => 'test'
            ],
            'pk' => 12
        ]);
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (empty($data['model']))
                    MyHelper::sendError("Model not specified!");
                $model = $data['model'];
                if (!in_array($model, MyHelper::getAllModels($module)))
                    MyHelper::sendError("Model `$model` doesn't exist in module `$module`");
                if (empty($data['data']))
                    MyHelper::sendError("Data not specified!");
                else
                    $from = $data['data'];
                if (empty($data['pk']))
                    MyHelper::sendError("Primary Key not specified!");
                $pk = $data['pk'];
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $modelName = $model;
        $model = $modelName::model()->findByPk($pk);
        if (is_null($model))
            MyHelper::sendError("Inexistent Primary Key!");
        $arr = [];
        $extensions = array();
        $dt = DocumentType::model()->findByAttributes(array('instance_model_name' => $modelName));
        if (!is_null($dt)) {
            $cat = DocumentCategory::model()->findByPk($dt->category_id);
            if (!is_null($cat)) {
                $ext_models = FileFormat::model()->findAllByPk(explode(',', $cat->extensions));
                if (!empty($ext_models))
                    foreach ($ext_models as $ext_model)
                        $extensions[] = '.' . $ext_model->extension;
            }
        }
        foreach ($from as $key => $value)
            if (in_array($key, MyHelper::getColumns($modelName)))
                $model->$key = $value;
        if (isset($model->document_id) && !is_numeric($model->document_id)) {
            $model->document_id = HelperSystem::remoteUpdateDocument($modelName::model()->findByPk($model->id)->document_id, $model->document_id);
        }
        if ($model->update()) {
            $arr[] = [
                'update' => true,
                'id' => $model->id
            ];
        } else {
            $arr[] = [
                'update' => false,
                'validate' => $model->validate(),
                'data' => $from
            ];
        }
        return $arr;
    }

    public static function getModelListByAttributes()
    {
        MyHelper::putExample([
            'module' => 'Clasificator',
            'model' => 'Tipsubiect',
            'criteria' => [
                'column' => 'name',
                'value' => ["test", "test2"],
                'condition' => '!='
            ]
        ]);
        $supported_conditions = ["!="];
        $data = MyHelper::decodeJSON(file_get_contents("php://input"));
        if (isset($data['module'])) {
            $module = $data['module'];
            if (in_array($module, MyHelper::getAllModules())) {
                if (isset($data['criteria'])) {
                    $in = $data['criteria'];
                    if (!in_array($in->condition, $supported_conditions))
                        MyHelper::sendError("Unsupported condition!");
                } else
                    MyHelper::sendError("Attributes not specified!");
            } else
                MyHelper::sendError("Module `$module` doesn't exist!`");
        } else
            MyHelper::sendError("Module not specified!");
        $arr = [];
        $models = MyHelper::getAllModels($module);
        foreach ($models as $model) {
            if ($in->condition == "!=")
                if (in_array($model, $in->value))
                    continue;
            $arr[] = [
                'name' => $model,
                'Data_type' => MyHelper::getStructure($model),
                'Relations' => MyHelper::getRelations($model),
            ];
        }
        return $arr;
    }
}