<?php
/* @var $this InvoiceController */
/* @var $model Invoice */

$this->breadcrumbs = array(
    'Invoices' => array('index'),
    $model->idinv,
);

$this->menu = array(
    array('label' => 'List Invoice', 'url' => array('index')),
    array('label' => 'Create Invoice', 'url' => array('create')),
    array('label' => 'Update Invoice', 'url' => array('update', 'id' => $model->idinv)),
    array('label' => 'Delete Invoice', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idinv), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Invoice', 'url' => array('admin')),
);
?>

<h2>Invoice #<?php echo $model->idinv; ?>
    <small><i><?= $model->aprobat ? "approved" : "unapproved" ?></i></small>
</h2>
<h4 class="text-success">Total: <b><?= Invoice::getTotalPriceByInvoice($model->idinv) . Product::$currency ?></b></h4>
<h4>Products: </h4>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('Order', [
        'criteria' => [
            'condition' => 't.idinv=:idinv',
            'params' => [
                ':idinv' => $model->idinv
            ],
            'order' => 'idprod0.nameprod ASC',
            'with' => [
                'idprod0'
            ],
//            'together' => true
        ]
    ]),
    'itemView' => '../product/_view',
    'htmlOptions' => [
        'class' => 'row'
    ],
    'viewData' => [
        'buy' => false
    ],
    'summaryCssClass' => 'container-fluid text-right'
)); ?>
