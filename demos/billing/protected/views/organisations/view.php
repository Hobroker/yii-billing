<?php
/* @var $this OrganisationsController */
/* @var $model Organisations */

$this->breadcrumbs=array(
	'Organisations'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Organisations', 'url'=>array('index')),
	array('label'=>'Create Organisations', 'url'=>array('create')),
	array('label'=>'Update Organisations', 'url'=>array('update', 'id'=>$model->idOrg)),
	array('label'=>'Delete Organisations', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->idOrg),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Organisations', 'url'=>array('admin')),
);
?>

<h1>View Organisations #<?php echo $model->idOrg; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'idOrg',
		'name',
		'address',
	),
)); ?>
