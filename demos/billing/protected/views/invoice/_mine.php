<?php
/* @var $this InvoiceController */
/* @var $data Invoice */
$approved = Invoice::isApproved($data->idinv);
?>

<div class="col-md-6">
    <div class="well<?= !$data->aprobat ? ' bg-danger' : '' ?>">
        <?php if ($approved): ?>
            <i>Invoice approved</i>
            <? //= CHtml::link('Print', ['']) ?>
        <?php else: ?>
            <i>Invoice is not approved yet!</i>
        <?php endif; ?>
        <br/>

        <b><?php echo CHtml::encode($data->getAttributeLabel('idinv')); ?>:</b>
        <?php echo CHtml::link(CHtml::encode($data->idinv), array('view', 'id' => $data->idinv)); ?>
        <br/>

        <b>Total price:</b>
        <?php echo Invoice::getTotalPriceByInvoice($data->idinv); ?> MDL
        <br/>

        <?=$approved?CHtml::link("Export to PDF", ['export', 'id' => $data->idinv]):""; ?>
    </div>
</div>