<?php
/* @var $this OrderController */
/* @var $data Order */
?>

<div class="col-md-6">
    <div class="thumbnail well order-checkout">

        <b>User:</b>
        <?= $data->iduser0->fname . " " . $data->iduser0->lname . " (" . $data->iduser0->email . ")" ?>
        <br/>

        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('nameprod')); ?>:</b>
        <?= Product::model()->productManufacturer($data->idprod) ?>
        <?= CHtml::encode($data->idprod0->nameprod); ?>
        <br/>

        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('priceprod')); ?>:</b>
        <?php echo CHtml::encode($data->idprod0->priceprod) . Product::$currency; ?>
        <br/>

        <b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
        <?php echo CHtml::encode(Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->date, 'yyyy-MM-dd'), 'medium', null)); ?>
        <br/>

        <b><?php echo CHtml::encode(Product::model()->getAttributeLabel('priceprod')); ?>:</b>
        <?php if (Product::productSale($data->idprod) != 0): ?>
            <?php echo Product::productPriceWithSale($data->idprod) . Product::$currency; ?>
            <strike><?php echo CHtml::encode($data->idprod0->priceprod); ?> MDL</strike>
            (-<?php echo Product::productSale($data->idprod) . Sales::$unit; ?>)
        <?php else: ?>
            <?php echo CHtml::encode($data->idprod0->priceprod) . Product::$currency; ?>
        <?php endif; ?>
        <br/>

    </div>
</div>
