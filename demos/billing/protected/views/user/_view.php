<?php
/* @var $this UserController */
/* @var $data User */
?>
<div class="col-md-4 col-sm-6">
    <h4 class="small-block">
        <a href="<?= Yii::app()->createUrl('user/' . $data->id); ?>"
           class="thumbnail well no-underline">
            <?php echo CHtml::encode($data->username); ?>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
                <?= $data->id; ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
                <?php echo CHtml::encode($data->email); ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('profile')); ?>:</b>
                <?php echo CHtml::encode($data->profile); ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('fname')); ?>:</b>
                <?php echo CHtml::encode($data->fname); ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('lname')); ?>:</b>
                <?php echo CHtml::encode($data->lname); ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('lname')); ?>:</b>
                <?php echo CHtml::encode($data->lname); ?>
            </small>
            <small>
                <b><?php echo CHtml::encode($data->getAttributeLabel('idorg')); ?>:</b>
                <?php echo CHtml::encode($data->idorg0->name); ?>
            </small>
        </a>
    </h4>
</div>