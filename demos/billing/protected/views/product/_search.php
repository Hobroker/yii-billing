<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'idprod'); ?>
		<?php echo $form->textField($model,'idprod'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idcateg'); ?>
		<?php echo $form->textField($model,'idcateg'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nameprod'); ?>
		<?php echo $form->textField($model,'nameprod',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'priceprod'); ?>
		<?php echo $form->textField($model,'priceprod',array('size'=>9,'maxlength'=>9)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'idmanufacturer'); ?>
		<?php echo $form->textField($model,'idmanufacturer'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->