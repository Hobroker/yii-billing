<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'api-form',
        'enableAjaxValidation' => false,
    )); ?>

    <div class="row">
        <label for="select_module">Module</label>
        <?php
        $modules = MyHelper::getAllModules();
        $modules = array_combine($modules, $modules);
        $this->widget('ext.select2.ESelect2', array(
            'name' => 'Api[module]',
            'data' => $modules,
            'htmlOptions' => [
                'style' => 'display: block',
                'placeholder' => 'Select a module',
                'id' => 'select_module',
                'class' => 'select_bby',
                'name' => 'module'
            ]
        ));
        ?>
    </div>

    <div class="row">
        <label for="select_model">Model</label>
        <?php
        $this->widget('ext.select2.ESelect2', array(
            'name' => 'Api[model]',
            'data' => [],
            'htmlOptions' => [
                'style' => 'display: block',
                'placeholder' => 'Select a model',
                'id' => 'select_model',
                'class' => 'select_bby',
                'name' => 'model'
            ]
        ));
        ?>
    </div>

    <div class="row">
        <label for="select_action">Action</label>
        <?php
        $actions = ApiSave::getAllActions();
        $actions = array_combine($actions, $actions);
        $this->widget('ext.select2.ESelect2', array(
            'name' => 'Api[action]',
            'data' => $actions,
            'htmlOptions' => [
                'style' => 'display: block',
                'placeholder' => 'Select action',
                'id' => 'select_action',
                'class' => 'select_bby',
                'name' => 'action'
            ]
        ));
        ?>
    </div>

    <div class="row">
        <label for="cmd">Url</label>
        <input type="text" class="form-control" id="url" name="Api[url]" readonly>
    </div>

    <div class="row">
        <label for="cmd">Parameters</label>
        <div class="col-md-6">
            <pre id="paramsv" style="min-height: 34px"></pre>
            <input type="hidden" name="Api[params]" id="params">
        </div>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton(isset($model) ? 'Update' : 'Create'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script src=""></script>
<script>
    var url = <?=CJSON::encode(Yii::app()->createAbsoluteUrl("Api/default"))?>;
    var old = null;
    <?php if(isset($model)) {?>
    old = {
        module: '<?=$model->module?>',
        model: '<?=$model->model?>',
        action: '<?=$model->action?>',
    };
    <?php }?>
    $(document).ready(function () {
        $('#select_module').on("change", function () {
            setModels($(this).select2().val())
        });
        $('select.select_bby').on("change", setParams);
        if (null != old) {
            $("#select_module").select2("val", old.module);
            setModels(old.module);
            $("#select_action").select2("val", old.action);
            setParams()
        }
    });

    function setParams() {
        q = ['select_module', 'select_model', 'select_action']
        for (i = 0; i < q.length; i++) {
            if ($("#" + q[i]).val() == undefined || $("#" + q[i]).val() == "")
                return;
        }
        var module = $("#select_module option:selected").val(),
            model = $("#select_model option:selected").val(),
            action = $("#select_action option:selected").val(),
            jsn = [];
        $("#url").val(url + '/' + action);
        $.ajax({
            type: 'POST',
            url: url + "/" + action,
            data: {
                'example': 'x'
            },
            dataType: "text",
            success: function (data, status) {
                if (status == "success") {
                    data = JSON.parse(data);
                    if (data.example != undefined) {
                        data.example['0'].module = module;
                        data.example['0'].model = model;
                        jsn = data.example;

                        $("#paramsv").text(JSON.stringify(jsn, null, 4));
                        $("#params").val(JSON.stringify(jsn));
                        if (action.toLowerCase() == 'putfile') {
                            $("#paramsv").text('0=' + $("#paramsv").text())
                        }
                    }
                }
            },
            error: function (e) {
                console.log(e)
            }
        });
    }
    function setModels(module) {
        $.ajax({
            type: 'POST',
            url: url + "/getModels",
            data: JSON.stringify([{"module": module}]),
            dataType: "text",
            success: function (data, status) {
                if (status == "success") {
                    data = JSON.parse(data);
                    var select = $('#select_model');
                    select.empty();
                    for (var i = 0; i < data.response.length; i++) {
                        var option = $('<option/>').attr('value', data.response[i].name).text(data.response[i].name);
                        if (null != old)
                            if (data.response[i].name == old.model)
                                option.prop("selected", "selected");
                        select.append(option).select2();
                    }
                }
            }
        });
    }

</script>
<style>
    .hb-selected {
        background-color: #E57373 !important;
    }
</style>