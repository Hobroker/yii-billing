<?php
/* @var $this ProductController */
/* @var $model Product */

$this->breadcrumbs=array(
	'Products'=>array('index'),
	'Create',
);

$this->menu=array(
	['label' => 'Create Product', 'url' => array('create')],
	array('label'=>'List Products', 'url'=>array('index')),
	array('label'=>'Manage Products', 'url'=>array('admin')),
);
?>

<h2>Create Product</h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>