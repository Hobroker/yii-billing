<?php

/**
 * This is the model class for table "{{sales}}".
 *
 * The followings are the available columns in table '{{sales}}':
 * @property integer $idsale
 * @property integer $valuesale
 * @property string $startdate
 * @property string $enddate
 *
 * The followings are the available model relations:
 * @property ProdSale[] $prodSales
 */
class Sales extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    
    public static $unit = '%';
    
    public function tableName()
    {
        return '{{sales}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('valuesale', 'numerical', 'integerOnly' => true),
            array('startdate, enddate', 'safe'),
            array('startdate, enddate, valuesale', 'required'),
            array('enddate', 'compare', 'compareAttribute' => 'startdate', 'operator' => '>', 'message' => 'Start Date must be less than End Date'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('idsale, valuesale, startdate, enddate', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'prodSales' => array(self::HAS_MANY, 'ProdSale', 'idsale'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'idsale' => 'ID',
            'valuesale' => 'Value',
            'startdate' => 'Start Date',
            'enddate' => 'End Date',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('idsale', $this->idsale);
        $criteria->compare('valuesale', $this->valuesale);
        $criteria->compare('startdate', $this->startdate, true);
        $criteria->compare('enddate', $this->enddate, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Sales the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function prettyDate($date) {
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($date, 'yyyy-MM-dd'),'medium',null);
    }

    public static function getProductsWithSale($id) {
        $productSales = ProdSale::model()->findAll([
            'condition'=>'idsale=:idsale',
            'params'=>[
                ':idsale'=>$id
            ]
        ]);
        return $productSales;
    }
    
}
