<?php
$this->menu = array(
    array('label' => 'Index', 'icon' => 'list', 'url' => array("Api/apiSave/index")),
    array('label' => 'Create', 'icon' => 'plus', 'url' => array("Api/apiSave/create")),
);
?>
<?= CHtml::link("Index", ['index']); ?>
    <h1>Update API
        <small>#<?= $model->id ?></small>
    </h1>
<?php
$this->renderPartial("_form", ['update' => true, 'model' => $model]);
?>