<?php
/* @var $this ManufacturerController */
/* @var $data Manufacturer */
?>

<div class="col-md-4 col-sm-6">
    <h4><?php
        echo CHtml::link(
            $data->name . '<span class="pull-right">(' . Manufacturer::getNrProductsByManufacturer($data->id) . ')</span>',
            ['view', 'id' => $data->id],
            [
                'class' => 'thumbnail well no-underline'
            ]);
        ?>
    </h4>
</div>