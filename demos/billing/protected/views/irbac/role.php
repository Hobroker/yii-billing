<?php
$auth = Yii::app()->authManager;
$this->breadcrumbs = array(
    'I-RBAC' => array('index'),
    !isset($_GET['name']) ? 'Create role' : "Edit role",
);
?>
<?php if (isset($_GET['name'])): ?>
    <?= CHtml::link('Create role', ['irbac/role'], ['class' => 'btn btn-default']) ?>
    <h2>Edit role <b><?= $_GET["name"] ?></b></h2>
<?php else: ?>
    <h2>Create role</h2>
<?php endif; ?>
<div class="container-fluid">
    <?php $form = $this->beginWidget(
        'IActiveForm',
        [
            'htmlOptions' => array(
                'onsubmit' => 'return taskSubmit()',
            )
        ]
    ); ?>
    <div class="row">
        <?php if (empty($_GET['name'])): ?>
            <div class="col-md-6">
                <div class="form-group">
                    <?= IHtml::label('Name ' . $error, 'name', ['encode' => false]) ?>
                    <input type="text" id="name" name="name" class="form-control"
                           value="<?= isset($data['name']) ? $data['name'] : ""; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?= IHtml::label('Description', 'description') ?>
                    <input type="text" name="description" id="description" class="form-control"
                           value="<?= isset($data['description']) ? $data['description'] : ""; ?>">
                </div>
            </div>
        <?php endif; ?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <?= IHtml::label('All tasks', '') ?>
                <?php
                $this->widget('zii.widgets.grid.CGridView',
                    [
                        'afterAjaxUpdate' => 'function(){ selectRowsEvent(); }',
                        'dataProvider' => $model->search(),
                        'filter' => $model,
                        'columns' => [
                            [
                                'class' => 'ICheckBoxColumn',
                                'selectableRows' => 2,
                                'checkBoxHtmlOptions' => [
                                    'class' => 'row-checkbox',
                                ],
                            ],
                            [
                                'name' => 'name',
                                'type' => 'raw',
                                'value' => 'CHtml::link("$data->name", ["irbac/task/$data->name"])',
                                'htmlOptions' => [
                                    'class' => 'select-name'
                                ]
                            ],
                            'description',
                        ],
                        'enablePagination' => true,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover table-condensed selectable',
                        'pager' => [
                            'class' => 'ILinkPager',
                        ],
                        'htmlOptions' => [
                            'class' => 'table-responsive'
                        ],
                        'pagerCssClass' => 'default',
                    ])
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="emptySpace"></div>
                <?= IHtml::label('Assigned tasks', 'operations') ?>
                <?php
                $controllers = [];
                $actions = [];
                foreach (glob('./protected/controllers/*Controller.php') as $filename) {
                    $name = explode('/', $filename);
                    $controllers[] = str_replace("Controller.php", "", end($name));
                }
                ?>
                <select id='tasksTo' name='tasks[]' class='form-control destination' multiple style='height: 350px'>
                    <?php
                    if (isset($_GET['name'])) {
                        $tasks = Authitemchild::model()->findAll([
                            'condition' => 'parent=:parent',
                            'params' => [
                                ':parent' => $_GET['name']
                            ]
                        ]);
                        foreach ($tasks as $task) {
                            echo "<option value='$task->child'>$task->child</option>";
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3 col-md-offset-9">
                    <div class="form-group">
                        <?= CHtml::link('Create task', ['irbac/task/'], [
                            'class' => 'btn btn-default btn-block',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?php
                        echo IHtml::button('Remove', [
                            'class' => 'btn btn-danger btn-block',
                            'onclick' => "removeTasks()"
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-6">
                    <div class="form-group">
                        <?= CHtml::link('Go to task', ['irbac/task/'], [
                            'class' => 'btn btn-default btn-block taskRedirect',
                            'source' => 'From'
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <?php echo IHtml::submitButton(isset($_GET['name']) ? 'Update' : 'Create', [
                    'name' => isset($_GET['name']) ? 'update' : 'create',
                ]); ?>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
