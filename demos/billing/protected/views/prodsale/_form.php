<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */
/* @var $form CActiveForm */
?>

<div class="container-fluid col-md-4">

    <?php $form = $this->beginWidget('IActiveForm', array(
        'id' => 'prod-sale-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'idprod'); ?>
            <?= IHtml::activeDropDownList(
                $model, 'idprod',
                CHtml::listData(Product::model()->findAll(), 'idprod', 'nameprod'),
                [
                    'prompt' => 'Select a product',
                    'class' => 'form-control'
                ]
            ); ?>
            <?php echo $form->error($model, 'idprod'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'idsale'); ?>
            <?= CHtml::activeDropDownList($model, 'idsale', CHtml::listData(Sales::model()->findAll(), 'idsale', function ($sale) {
                return $sale->valuesale . '%' . ' - (' . IHtml::idate($sale->startdate) . ' - ' . IHtml::idate($sale->enddate) . ')';
            }), [
                'class' => 'form-control',
                'prompt' => 'Select a sale'
            ]) ?>
            <?php echo $form->error($model, 'idsale'); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="form-group">
            <?php echo IHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->