<?php
/* @var $this ProductController */
/* @var $model Product */
/* @var $form CActiveForm */
?>

<div class="container-fluid col-md-4">

    <?php $form = $this->beginWidget('IActiveForm', array(
        'id' => 'product-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'idcateg'); ?>
            <?= CHtml::activeDropDownList($model, 'idcateg', CHtml::listData(Categories::model()->findAll(), 'idcateg', 'namecateg'), array('class' => 'form-control', 'prompt' => 'Select a category')); ?>
            <?php echo $form->error($model, 'idcateg'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'idmanufacturer'); ?>
            <?= CHtml::activeDropDownList($model, 'idmanufacturer', CHtml::listData(Manufacturer::model()->findAll(), 'id', 'name'), array('class' => 'form-control', 'prompt' => 'Select a manufacturer')); ?>
            <?php echo $form->error($model, 'idmanufacturer'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'nameprod'); ?>
            <?php echo $form->textField($model, 'nameprod', array('size' => 60, 'maxlength' => 100)); ?>
            <?php echo $form->error($model, 'nameprod'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'priceprod'); ?>
            <div class="input-group">
                <?php echo $form->numberField($model, 'priceprod', array('min' => 1, 'max' => 9999999)); ?>
                <div class="input-group-addon"><?= Product::$currency ?></div>
            </div>
            <?php echo $form->error($model, 'priceprod'); ?>
        </div>
    </div>

    <div class="row buttons">
        <div class="form-group">
            <?php echo IHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->