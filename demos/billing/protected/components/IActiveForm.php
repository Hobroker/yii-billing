<?php

class IActiveForm extends CActiveForm
{
    private $bs_inputClasses = ['class' => 'form-control'];
    private $bs_BtnClasses = ['class' => 'btn btn-primary btn-block'];

    public function textField($model, $attribute, $htmlOptions = [])
    {
        return CHtml::activeTextField($model, $attribute, array_merge($this->bs_inputClasses, $htmlOptions));
    }

    public function passwordField($model, $attribute, $htmlOptions = [])
    {
        return CHtml::activePasswordField($model, $attribute, array_merge($this->bs_inputClasses, $htmlOptions));
    }

    public function textArea($model, $attribute, $htmlOptions = [])
    {
        return CHtml::activeTextArea($model, $attribute, array_merge($this->bs_inputClasses, $htmlOptions));
    }

    public function submitButton($title, $htmlOptions = [])
    {
        echo CHtml::submitButton($title, array_merge($this->bs_BtnClasses, $htmlOptions));
    }

    public function labelEx($model, $attribute, $htmlOptions = array())
    {
        return IHtml::activeLabelEx($model, $attribute, $htmlOptions);
    }

    public function numberField($model, $attribute, $htmlOptions = array())
    {
        return CHtml::activeNumberField($model, $attribute, array_merge($this->bs_inputClasses, $htmlOptions));
    }

    public function activeDropDownList($model, $attribute, $data, $htmlOptions = array())
    {
        self::resolveNameID($model, $attribute, $htmlOptions);
        $selection = self::resolveValue($model, $attribute);
        $options = "\n" . self::listOptions($selection, $data, $htmlOptions);
        self::clientChange('change', $htmlOptions);

        if ($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);

        $hidden = '';
        if (!empty($htmlOptions['multiple'])) {
            if (substr($htmlOptions['name'], -2) !== '[]')
                $htmlOptions['name'] .= '[]';

            if (isset($htmlOptions['unselectValue'])) {
                $hiddenOptions = isset($htmlOptions['id']) ? array('id' => self::ID_PREFIX . $htmlOptions['id']) : array('id' => false);
                $hidden = self::hiddenField(substr($htmlOptions['name'], 0, -2), $htmlOptions['unselectValue'], $hiddenOptions);
                unset($htmlOptions['unselectValue']);
            }
        }
        return $hidden . self::tag('select', array_merge(['class' => 'form-control'], $htmlOptions), $options);
    }
}