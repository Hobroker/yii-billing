-- noinspection SqlNoDataSourceInspectionForFile

drop table if exists cl_test_cl_api;
CREATE TABLE cl_test_cl_api
(
  id bigserial unique not null,
  cimp1 character varying(150),
  cimp2 integer,
  cimp3 bigint,
  cimp4 numeric,
  cimp5 double precision,
  cimp8 text,
  cimp9 character varying(100),
  cimp10 text,
  cimp11 character varying(100),
  cimp12 integer,
  cimp13 text,
  cimp14 date,
  cimp15 timestamp without time zone,
  create_user_id bigint,
  create_datetime timestamp without time zone DEFAULT now(),
  update_user_id bigint,
  update_datetime timestamp without time zone,
  primary key ("id")
);

drop table if exists api_saves;
drop table if exists cl_api_saves;
CREATE TABLE api_saves (
  id bigserial unique not null,
  module character varying(255),
  model character varying(255),
  action character varying(255),
  url character varying(255),
  params character varying(10240),
  primary key ("id")
);

insert into "cl_test_cl_api" ("cimp1", "cimp2", "cimp3", "cimp4", "cimp5", "cimp8", "cimp9", "cimp10","cimp11", "cimp12", "cimp13", "cimp14", "cimp15", "create_user_id", "create_datetime","update_user_id", "update_datetime") VALUES
('TestText',123,123,22,22,'Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test Test ','test','test2','test1',1,'test1','1988-11-11','1988-11-11 00:00:00',1, current_timestamp, NULL, NULL);

