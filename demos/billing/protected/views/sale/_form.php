<?php
/* @var $this SaleController */
/* @var $model Sales */
/* @var $form CActiveForm */
?>

<div class="container-fluid col-md-4">

    <?php $form = $this->beginWidget('IActiveForm', array(
        'id' => 'sales-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'valuesale'); ?>
            <div class="input-group">
                <?php echo $form->numberField($model,
                    'valuesale',
                    [
                        'min' => 1,
                        'max' => 100,
                        'style' => 'z-index: 0'
                    ]); ?>
                <div class="input-group-addon"><?= Sales::$unit ?></div>
            </div>
            <?php echo $form->error($model, 'valuesale'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'startdate'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', [
                'model' => $model,
                'attribute' => 'startdate',
                'options' => [
                    'dateFormat' => 'yy-mm-dd'
                ],
                'htmlOptions' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
            <?php echo $form->error($model, 'startdate'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'enddate'); ?>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker', [
                'model' => $model,
                'attribute' => 'enddate',
                'options' => [
                    'dateFormat' => 'yy-mm-dd'
                ],
                'htmlOptions' => [
                    'class' => 'form-control'
                ]
            ]);
            ?>
            <?php echo $form->error($model, 'enddate'); ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo IHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->