<?php

class DefaultController extends Controller
{
    Const APPLICATION_ID = 'ASCCPE';
    public $layout = 'main';
    private $format = 'json';

    public function __construct($ar1, $ar2)
    {
        parent::__construct($ar1, $ar2);
        //Yii::app()->clientScript->registerCssFile( $this->module->assetsUrl . '/assets/css/custom.css');
    }

    public function beforeAction() {
        require_once(dirname(__FILE__) . '/../helpers/MyHelper.php');
        require_once(dirname(__FILE__) . '/../helpers/Universal.php');
        require_once(dirname(__FILE__) . '/../helpers/Response.php');
        return true;
    }

    public function actionExport()
    {
        $this->render('export', array(
            "server_url" => Yii::app()->BaseUrl . "/Api/default"
        ));
    }

    public function actionImport()
    {
        $this->render('import', array(
            "server_url" => Yii::app()->BaseUrl . "/Api/default"
        ));
    }

    public function actionAdministration()
    {
        $this->render('administration');
    }

    public function actionIndex()
    {
        $this->render('administration');
    }

    public function actionValidateUser()
    {
//        LoginCheck::checkAuth();
        MyHelper::sendOk(LoginCheck::checkUser());
    }


    public function createMultilanguageReturnUrl($lang = 'ro')
    {
        $arr = (count($_GET) > 0) ? $_GET : array();
        $arr['language'] = $lang;
        $url = isset($this->module) ? (DIRECTORY_SEPARATOR . $this->module->getName() . DIRECTORY_SEPARATOR) : '';
        $url .= $this->getId() . DIRECTORY_SEPARATOR;
        $url .= $this->getAction()->getId();
        $url = $this->createUrl($url, $arr);
        return $url;
    }

    // Igor edit //

    public function actionGetModules()
    {
        
        MyHelper::sendOk(Universal::getModuleList());
    }

    public function actionGetModels()
    {
//        [{"module":"Clasificator"}]
        MyHelper::sendOk(Universal::getModelList());
    }

    public function actionGetModelsByAttributes()
    {
//        [{"module":"Clasificator"}]
        MyHelper::sendOk(Universal::getModelListByAttributes());
    }

    public function actionGetStructure()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect"}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::getModelStructure());
    }

    public function actionGetData()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect"}]
//        [{"module":"Clasificator", "model":"Tipsubiect", "pk":[17, 10, 12]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::getModelData());
    }

    public function actionGetDataByPk()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect"}]
//        [{"module":"Clasificator", "model":"Tipsubiect", "pk":[17, 10, 12]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::getModelData());
    }

    public function actionGetDataByAttributes()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect", "criteria":[{"column":"name","condition":"like","value":"%test%"}, {"column":"id","condition":"=","value":"18"}]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::getModelDataByAttributes());
    }

    public function actionDeleteDataByPk()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect", "pk":[5, 1]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::deleteDataByPk());
    }

    public function actionDeleteDataByAttributes()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect", "criteria":[{"column":"name","condition":"like","value":"%test%"}, {"column":"id","condition":"=","value":"18"}]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::deleteDataByAttributes());
    }

    public function actionPutData()
    {
//        [{"module":"Clasificator", "model":"Tipsubiect", "data":[{"name":"test1"}, {"name":"test2"}]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::putModelData());
    }

    public function actionUpdateDataByPk() {
//        [{"module":"Clasificator", "model":"Tipsubiect", "data":[{"pk":11, "name":"test1_update"}, {"pk":12, "name":"test2_up"}]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updateDataByPk());
    }

    public function actionUpdateDataByAttributes() {
//        [{"module":"Clasificator", "model":"Tipsubiect", "data":{"name":"test"}, "criteria":[{"column":"name","condition":"like","value":"%Y%"}]}]
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updateDataByAttributes());
    }

    public function actionPutPdf() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::putPdf());
    }

    public function actionPutDoc() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::putDoc());
    }

    public function actionPutFile() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::putFile());
    }

    public function actionPutSpecial() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::putSpecial());
    }

    public function actionUpdatePdfByPk() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updatePdfByPk());
    }

    public function actionUpdateDocByPk() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updateDocByPk());
    }

    public function actionUpdateFileByPk() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updateFile());
    }

    public function actionUpdateSpecialByPk() {
        LoginCheck::checkAuth();
        MyHelper::sendOk(Universal::updateSpecial());
    }

    public function actionTest() {

    }
}