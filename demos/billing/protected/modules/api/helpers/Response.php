<?php

class Response
{
    /**
     * Sends the API response
     *
     * @param int $status
     * @param string $body
     * @param string $content_type
     * @access private
     * @return void
     */
    public static function sendResponse($status = 200, $body = '', $content_type = 'application/json')
    {
        $status_header = 'HTTP/1.1 ' . $status . ' ' . ErrorClassificator::getStatusCodeMessage($status);
        // set the status
        header($status_header);
        // set the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if ($body != '') {
            // send the body
            echo $body;
            exit;
        } // we need to create the body if none is passed
        else {
            // create some body messages
            $message["Error"] = ErrorClassificator::getStatusCodeMessage($status);

            echo ObjectEncoded::getObjectEncoded('
                    Document',
                array(
                    "status" => "ERROR",
                    "message" => $message
                )
            );
            exit;
        }
    } // }}}
} 