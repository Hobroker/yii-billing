<?php
$this->pageTitle = Yii::app()->name . ' - Login';
$this->breadcrumbs = array(
    'Login',
);
?>
<div class="col-md-4 col-md-offset-4">
    <?= isset($_GET['new']) ? '' : "" ?>
    <?php if (isset($_GET['new'])): ?>
        <p class="bg-success">Successfully registered!</p>
    <?php endif; ?>
    <h2>Login</h2>
    <p>Please fill out the following form with your login credentials:</p>
    <?php $form = $this->beginWidget('IActiveForm', array(
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array(
            'class' => 'form',
        )
    )); ?>

    <p>Fields with <span class="text-danger">*</span> are required.</p>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'username'); ?>
        <?php echo $form->textField($model, 'username'); ?>
        <?php echo $form->error($model, 'username'); ?>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'password'); ?>
        <?php echo $form->passwordField($model, 'password'); ?>
        <?php echo $form->error($model, 'password'); ?>
    </div>
    <div class="checkbox">
        <label>
            <?php echo $form->checkBox($model, 'rememberMe'); ?>
            <?php echo $form->label($model, 'rememberMe'); ?>
            <?php echo $form->error($model, 'rememberMe'); ?>
        </label>
    </div>
    <div class="form-group">
        <?php echo CHtml::submitButton('Login', ['class' => 'btn btn-primary btn-block']); ?>
    </div>
    <div class="form-group">
        <p>Don't have an account? <?=CHtml::link('Register', ['user/register'])?></p>
    </div>
    <?php $this->endWidget(); ?>
</div>
