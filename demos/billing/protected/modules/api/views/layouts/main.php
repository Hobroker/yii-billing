<?php $this->beginContent(); ?>

<?php
$this->widget('application.components.widgets.usertheme.AvantModuleMenu', array(
    'label' => 'Module Menu',
    'icon' => 'more',
    'items' => array(
        array('label' => 'Administration', 'url' => array('/Api/default/administration')),
        array('label' => 'Import', 'url' => array('/Api/default/import')),
        array('label' => 'Export', 'url' => array('/Api/default/export')),
    )));
?>

<div id="content">
    <?php
    echo $content;
    ?>
</div>


<?php $this->endContent(); ?>
