$(document).ready(function () {
    selectRowsEvent();
    $('.taskRedirect').click(function (e) {
        var a = $('#tasks' + $(this).attr('source') + ' option:selected');
        if (a.size() == 1)
            $(this).attr('href', $(this).attr('href') + "/" + a[0].value)
        else {
            alert("Select one task!");
            e.preventDefault();
        }
    });
    $('.roleRedirect').click(function (e) {
        var a = $('#roles' + $(this).attr('source') + ' option:selected');
        if (a.size() == 1)
            $(this).attr('href', $(this).attr('href') + "/" + a[0].value)
        else {
            alert("Select one role!");
            e.preventDefault();
        }
    });
    $('#operationsTo option').dblclick(function () {
        removeItem($(this).val());
    });
    $('#tasksTo option').dblclick(function () {
        removeItem($(this).val());
    });
    $('#rolesTo option').dblclick(function () {
        removeItem($(this).val());
    });
});

function clearOperationControllers(where) {
    $(where + " > optgroup").each(function () {
        if (!$(this).children('option').length)
            $(this).hide()
    })
}

function removeOneOperation(full) {
    $("#operationsTo option[value='" + full + "']").remove();
    checkAssignments();
    clearOperationControllers("#operationsTo");
}

function addOneOperation(full) {
    var c = full.split(':')[0],
        a = full.split(':')[1]

    $('#operationsTo > optgroup').each(function () {
        var controller = $(this).attr('label');
        if (controller != c) return;
        $(this).show();
        if (!operationExists(full)) {
            var option = $("<option></option>");
            option.attr('value', full);
            option.text(a);
            option.dblclick(function () {
                $(this).remove();
            })
            $(this).append(option);
        }
    })
}

function removeOneRole(full) {
    $("#rolesTo option[value='" + full + "']").remove()
}

function removeOneTask(full) {
    $("#tasksTo option[value='" + full + "']").remove()
}

function addOneRole(what) {
    if (roleExists(what)) return
    var option = $("<option></option>");
    option.attr("value", what)
    option.text(what);
    option.dblclick(function () {
        $(this).remove();
    })
    $('#rolesTo').append(option);
}

function removeRole() {
    $('#rolesTo > option:selected').each(function () {
        $(this).remove();
    })
}

function roleExists(name) {
    return $("#rolesTo option[value='" + name + "']").length > 0;
}
function taskExists(name) {
    return $("#tasksTo option[value='" + name + "']").length > 0;
}

function operationExists(name) {
    return $("#operationsTo option[value='" + name + "']").length > 0;
}

function assignSubmit() {
    $('#rolesTo > option').each(function () {
        $(this).prop('selected', true);
    })
}

function roleSubmit() {
    $('#operationsTo > optgroup').each(function () {
        $(this).children('option').each(function () {
            $(this).prop('selected', true)
        })
    });
}

function removeOperation() {
    $('#operationsTo > optgroup').each(function () {
        $(this).children('option:selected').each(function () {
            removeItem($(this).val())
        })
    });
    $('#operationsTo > optgroup').each(function () {
        if ($(this).children().length == 0)
            $(this).hide()
    });
    checkAssignments()
}

function removeTasks() {
    $('#tasksTo > option:selected').each(function () {
        removeItem($(this).val())
    })
}

function addOneTask(what) {
    if (taskExists(what)) return;
    var option = $("<option></option>");
    option.attr("value", what);
    option.text(what);
    option.dblclick(function () {
        $(this).remove();
    });
    $('#tasksTo').append(option);
}
function taskSubmit() {
    $('#tasksTo > option').each(function () {
        $(this).prop('selected', true)
    });
}
function selectRowsEvent() {
    $(".selectable tr").click(function (e) {
        var a = $(this).find('.row-checkbox');
        if (!a.is(':checked')) {
            a.prop('checked', true);
            addItem($(this).find('td:eq(1)').text())
        } else {
            a.prop('checked', false);
            removeItem($(this).find('td:eq(1)').text());
        }
    });
    $(".selectable td.checkbox-column input").click(function (e) {
        e.stopPropagation();
        var a = $(this);
        if (a.is(':checked')) {
            a.prop('checked', true);
            addItem($(this).parent().parent().find('td:eq(1)').text())
        } else {
            a.prop('checked', false);
            removeItem($(this).parent().parent().find('td:eq(1)').text());
        }
    });
    $(".selectable th.checkbox-column input").click(function (e) {
        if ($(this).is(":checked")) {
            $(".selectable tbody input[type=checkbox]").each(function () {
                $(this).prop("checked", true);
                addItem($(this).parent().parent().find('td:eq(1)').text())
            })
        } else {
            $(".selectable tbody input[type=checkbox]").each(function () {
                $(this).prop("checked", false);
                removeItem($(this).parent().parent().find('td:eq(1)').text());
            })
        }
    });
    checkAssignments()
}

function checkAssignments() {
    if ($("#operationsTo").length)
        select = "operations";
    else if ($("#tasksTo").length)
        select = "tasks";
    else if ($("#rolesTo").length)
        select = "roles";
    $(".selectable tbody tr").each(function () {
        var left = $(this).find('td:eq(1)').text();
        var leftPanel = $(this);
        leftPanel.find(".checkbox-column input").prop("checked", false);
        $("#" + select + "To option").each(function () {
            var right = $(this).val();
            if (left == right) {
                leftPanel.find(".checkbox-column input").prop("checked", true);
            }
        });
    })
}
function addItem(what) {
    if (what == "") return
    if ($("#operationsTo").length)
        addOneOperation(what);
    else if ($("#tasksTo").length)
        addOneTask(what);
    else if ($("#rolesTo").length)
        addOneRole(what);
}
function removeItem(what) {
    if ($("#operationsTo").length)
        removeOneOperation(what);
    else if ($("#tasksTo").length)
        removeOneTask(what);
    else if ($("#rolesTo").length)
        removeOneRole(what);
    checkAssignments()
}