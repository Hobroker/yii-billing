<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Validate User</h3>
                    <p>This method verifies if user is valid.</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Enter username and password in Basic Authentication Box</p>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>
                            {"status":"OK","response":{"id":"1000000000002","status":"true","name_surname":"Demo
                            Demo","last_login":"05.23.16 10:49"}}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>
                            {"status":"ERROR","message":"Unauthorised"}
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get Module List</h3>
                    <p>This method returns list of modules.</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>
                            [{"list":"all"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri opționali:</p>
                    <ol>
                        <li>
                            list
                            <ul>
                                <li>all - toate modulele (active sau inactive)</li>
                                <li>active - modulele active</li>
                                <li>inactive - modulele inactive</li>
                            </ul>
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>
                            {"status":"OK","response":[{"active":true,"name":"User"},{"active":true,"name":"Api"},{"active":true,"name":"Clasificator"}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Parametrul list este incorect.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Invalid `list` value!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getModules
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"list":"active"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get model list</h3>
                    <p>This method returns all models by model.</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module":"Clasificator"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module":"Clasificator"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            <i class="fa fa-info-sign"></i>
                            {"status":"OK","response":[{"name":"ApiSave"},{"name":"TestClApi"}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getModels
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module":"Clasificator"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get model structure</h3>
                    <p>Returnează structura modelului</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module":"Clasificator", "model":"Tipsubiect"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[{"name":"id","allowNull":false,"dbType":"bigint","defaultValue":null,"size":null,"precision":null,"visible":false},{"name":"name","allowNull":true,"dbType":"character
                            varying(20)","defaultValue":null,"size":20,"precision":20,"visible":true},{"name":"create_user_id","allowNull":true,"dbType":"bigint","defaultValue":null,"size":null,"precision":null,"visible":false},{"name":"create_datetime","allowNull":true,"dbType":"timestamp
                            without time
                            zone","defaultValue":null,"size":null,"precision":null,"visible":false},{"name":"update_user_id","allowNull":true,"dbType":"bigint","defaultValue":null,"size":null,"precision":null,"visible":false},{"name":"update_datetime","allowNull":true,"dbType":"timestamp
                            without time zone","defaultValue":null,"size":null,"precision":null,"visible":false}]}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getStructure
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module":"Clasificator", "model":"Tipsubiect"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get model data</h3>
                    <p>Returnează datele din model</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module":"Clasificator", "model":"Tipsubiect"}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[
                            {"id":3,"name":null,"price":55,"col":null,"create_user_id":1000000000002,"create_datetime":"2016-04-29
                            15:55:58","update_user_id":1000000000002,"update_datetime":"2016-04-29 15:55:58"},
                            {"id":4,"name":null,"price":55,"col":null,"create_user_id":1000000000002,"create_datetime":"2016-04-29
                            15:56:15","update_user_id":1000000000002,"update_datetime":"2016-04-29 15:56:15"}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getData
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module":"Clasificator", "model":"Tipsubiect"}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get model data by pk</h3>
                    <p>Returnează datele din model după cheia primară</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Magic", "pk": [3,5]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>pk</i> - cheile primare într-un array
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[
                            {"id":3,"name":null,"price":55,"col":null,"create_user_id":1000000000002,"create_datetime":"2016-04-29
                            15:55:58","update_user_id":1000000000002,"update_datetime":"2016-04-29 15:55:58"},
                            {"id":4,"name":null,"price":55,"col":null,"create_user_id":1000000000002,"create_datetime":"2016-04-29
                            15:56:15","update_user_id":1000000000002,"update_datetime":"2016-04-29 15:56:15"}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată este incorectă
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Invalid Primary Key `12q`!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Cheia primară specificată nu este găsită în modul. Vor fi returnate doar datele cu cheia primară validă
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-warning">
                                    {"status":"OK","response":[{"error":"Invalid Primary Key `234`"}]}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getDataByPk
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Magic", "pk": [3,5]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<div class="col-md-12">
    <h4 class="timeline-month"><span>Api method</span></h4>
    <ul class="timeline">
        <li class="timeline-info">
            <div class="timeline-icon"><i class="fa fa-mobile"></i></div>
            <div class="timeline-body">
                <div class="timeline-header">
                    <span class="author">Posted by <a href="#">Igor Leahu</a></span>
                </div>
                <div class="timeline-content">
                    <h3>Get model data by attributes</h3>
                    <p>Returnează datele din model după atribute</p>
                </div>
                <hr/>
                <div class="timeline-content">
                    <p>Input data with method="post"</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            [{"module": "Clasificator", "model": "Tipsubiect", "criteria": [{"column":"name", "value":"test", "condition":"="}]}]
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Parametri necesari:</p>
                    <ol>
                        <li>
                            <i>module</i> - numele modulului
                        </li>
                        <li>
                            <i>model</i> - numele modelului
                        </li>
                        <li>
                            <i>criteria</i> - atributele
                            <ul>
                                <li>
                                    <i>column</i> – numele coloanei
                                </li>
                                <li>
                                    <i>value</i> – valoarea cu care se compară <i>column</i>
                                </li>
                                <li>
                                    <i>condition</i> – condiția de comparare. Spre exemplu like, =, >=, ...
                                </li>
                            </ul>
                        </li>
                    </ol>
                </div>
                <div class="timeline-content">
                    <p>Output succes data</p>
                </div>
                <div class="timeline-footer">
                    <div class="tab-content">
                        <div class="alert alert-info">
                            {"status":"OK","response":[
                            {"id":13,"name":"test","create_user_id":1000000000002,"create_datetime":"2016-04-29 10:00:32","update_user_id":1000000000002,"update_datetime":"2016-04-29 10:00:32"},
                            {"id":14,"name":"test","create_user_id":1000000000002,"create_datetime":"2016-04-29 10:00:32","update_user_id":1000000000002,"update_datetime":"2016-04-29 10:00:32"}
                        </div>
                    </div>
                </div>
                <div class="timeline-content">
                    <p>Output error data</p>
                </div>
                <ol>
                    <li>
                        Nu este specificat parametrul module.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    "status":"ERROR","message":"Module name not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modulul specificat nu există
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Module `Test` doesn't exist!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este specificat parametrul model
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model not specified!"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Modelul specificat nu există în modul
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Model `Test` doesn't exist in module `Clasificator`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu sunt specificate atributele
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Attributes not specified!"}
                                </div>
                            </div>
                        </div>
                        <ul>
                            <li>
                                Nu este specificată coloana (column) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Column not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată valoarea (value) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Value not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                Nu este specificată condiția (condition) în parametrul criteria
                                <div class="timeline-footer">
                                    <div class="tab-content">
                                        <div class="alert alert-danger">
                                            {"status":"ERROR","message":"Condition not specified!"}
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Coloana specificată în atribute (column) nu există.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Column `testcol` doesn't exist!`"}
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        Nu este respectată structura modelului. Verificați structura lui.
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-danger">
                                    {"status":"ERROR","message":"Check model structure!"}
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
                <div class="timeline-content">
                    <p>Example</p>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Address
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    <?= $server_url; ?>/getDataByAttributes
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 text-right">
                        Input data
                    </div>
                    <div class="col-md-10">
                        <div class="timeline-footer">
                            <div class="tab-content">
                                <div class="alert alert-info">
                                    [{"module": "Clasificator", "model": "Tipsubiect", "criteria": [{"column":"name", "value":"test", "condition":"="}]}]
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>