<?php

/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/13/16
 * Time: 12:17 PM
 */
class ICheckBoxColumn extends CCheckBoxColumn
{
    public function getDataCellContent($row)
    {
        return CHtml::checkBox('z', false, ['class'=>'row-checkbox']);
    }
}