<?php
/* @var $this ProdsaleController */
/* @var $model ProdSale */

$this->breadcrumbs = array(
    'Prod Sales' => array('index'),
    $model->idps,
);

$this->menu = array(
    array('label' => 'List Product Sales', 'url' => array('index')),
    array('label' => 'Create Product Sale', 'url' => array('create')),
    array('label' => 'Update Product Sale', 'url' => array('update', 'id' => $model->idps)),
    array('label' => 'Delete Product Sale', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idps), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Product Sales', 'url' => array('admin')),
);
?>

<h1>View Product Sale #<?php echo $model->idps; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'idps',
        [
            'name' => 'Product',
            'value' => isset($model->idprod0->nameprod) ? CHtml::encode($model->idprod0->nameprod) : "No product"
        ],
        [
            'name' => 'Sale',
            'value' => isset($model->idsale0->valuesale) ? CHtml::encode($model->idsale0->valuesale . '%') : "No product"
        ],
        [
            'name' => 'Period',
            'value' => CHtml::encode(Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->idsale0->startdate, 'yyyy-MM-dd'), 'medium', null) . ' - ' . Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->idsale0->enddate, 'yyyy-MM-dd'), 'medium', null))
        ]
    ),
)); ?>
