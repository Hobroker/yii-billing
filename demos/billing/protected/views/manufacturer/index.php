<?php
/* @var $this ManufacturerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Manufacturers',
);

$this->menu = array(
    array('label' => 'Create Manufacturer', 'url' => array('create')),
    array('label' => 'Manage Manufacturer', 'url' => array('admin')),
);
?>

<h2>Manufacturers</h2>

<div class="row">
    <?php $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_view',
    )); ?>

</div>