<?php
/* @var $this SaleController */
/* @var $model Sales */

$this->breadcrumbs=array(
	'Sales'=>array('index'),
	$model->idsale=>array('view','id'=>$model->idsale),
	'Update',
);

$this->menu=array(
	array('label'=>'List Sales', 'url'=>array('index')),
	array('label'=>'Create Sales', 'url'=>array('create')),
	array('label'=>'View Sales', 'url'=>array('view', 'id'=>$model->idsale)),
	array('label'=>'Manage Sales', 'url'=>array('admin')),
);
?>

<h2>Update Sales <?php echo $model->idsale; ?></h2>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>