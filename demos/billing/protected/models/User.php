<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $profile
 * @property string $fname
 * @property string $lname
 * @property string $tel
 * @property integer $idorg
 *
 * The followings are the available model relations:
 * @property Post[] $posts
 * @property Orders[] $orders
 * @property Organisations $idorg0
 */
class User extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{user}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, username, password', 'required'),
            array('idorg', 'numerical', 'integerOnly' => true),
            array('username, password, email, fname, lname', 'length', 'max' => 128),
            array('tel', 'length', 'max' => 32),
            array('profile', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, username, email, profile, fname, lname, tel, idorg', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'posts' => array(self::HAS_MANY, 'Post', 'author_id'),
            'orders' => array(self::HAS_MANY, 'Orders', 'iduser'),
            'idorg0' => array(self::BELONGS_TO, 'Organisations', 'idorg'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'profile' => 'Profile',
            'fname' => 'First name',
            'lname' => 'Last name',
            'tel' => 'Telephone number',
            'idorg' => 'Organisation'
        );
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return CPasswordHelper::verifyPassword($password, $this->password);
    }

    /**
     * Generates the password hash.
     * @param string password
     * @return string hash
     */
    public function hashPassword($password)
    {
        return CPasswordHelper::hashPassword($password);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('profile', $this->profile, true);
        $criteria->compare('fname', $this->fname, true);
        $criteria->compare('lname', $this->lname, true);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('idorg', $this->idorg);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function getUserFLname($id)
    {
        $user = User::model()->findByPk($id);
        return $user->fname . " " . $user->lname;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function sendEmail($to, $subject, $body, $attachment = null)
    {
        require_once('protected/extensions/mailer/EMailer.php');
        $mailer = new EMailer;
        $mailer->IsSMTP();
        $mailer->IsHTML(true);
        $mailer->SMTPAuth = true;
        $mailer->SMTPSecure = "ssl";
        $mailer->Host = "smtp.gmail.com";
        $mailer->Port = 465;
        $mailer->Username = "stajor.it@gmail.com";
        $mailer->Password = "123qweASDZXC";
        $mailer->From = "noreply@yii.billing.com";
        $mailer->FromName = "Yii Billing";

        $mailer->AddAddress($to);
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        $mailer->AddAttachment($attachment);

        if ($mailer->Send()) {
            echo "Message sent successfully!";
            return true;
        } else {
            echo "Fail to send your message!";
            return false;
        }
    }
}
