<?php
/* @var $this SaleController */
/* @var $model Sales */

$this->breadcrumbs = array(
    'Sales' => array('index'),
    $model->idsale,
);

$this->menu = array(
    array('label' => 'List Sales', 'url' => array('index')),
    array('label' => 'Create Sales', 'url' => array('create')),
    array('label' => 'Update Sales', 'url' => array('update', 'id' => $model->idsale)),
    array('label' => 'Delete Sales', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->idsale), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Sales', 'url' => array('admin')),
);
?>

<h2><?= $model->valuesale . Sales::$unit ?> sale</h2>
<p>
    <b><?= $model->getAttributeLabel('startdate') . ": "?></b>
    <?= IHtml::idate($model->startdate) ?>
</p>
<p>
    <b><?=$model->getAttributeLabel('enddate') . ": "?></b>
    <?= IHtml::idate($model->enddate) ?>
</p>
<h3>Products with this sale:</h3>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => new CActiveDataProvider('Product', [
        'criteria' => [
            'condition' => '"prodSales".idsale=:idsale',
            'params'=>[
                ':idsale'=>$model->idsale
            ],
            'with' => [
                'prodSales'
            ],
            "together" => true,
            'order' => 'nameprod ASC'
        ]
    ]),
    'itemView' => '../product/_view',
    'htmlOptions' => [
        'class' => 'row'
    ],
    'summaryCssClass' => 'container-fluid text-right',
)); ?>
