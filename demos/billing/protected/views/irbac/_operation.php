<?php
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 4/7/16
 * Time: 2:02 PM
 */
$this->breadcrumbs = array(
    'I-RBAC' => array('index'),
    'Create operation',
);
?>
<h2>Create operation</h2>

<div class="container-fluid col-md-4">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <div class="row">
        <div class="form-group">
            <?=IHtml::label("Name", 'name')?>
            <?php echo IHtml::textField('name'); ?>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            <?=IHtml::label("Description", 'description')?>
            <?php echo IHtml::textArea('description'); ?>
        </div>
    </div>

    <div class="row buttons">
        <?php echo IHtml::submitButton('Create', [
            'name' => 'operation'
        ]); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>