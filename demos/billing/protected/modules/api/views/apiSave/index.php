<?php
/**
 * Created by PhpStorm.
 * User: hobroker
 * Date: 5/4/16
 * Time: 9:39 AM
 */
$this->menu = array(
    array('label' => 'Create', 'icon' => 'plus', 'url' => array("Api/apiSave/create")),
);
?>
<h1>Saved API</h1>
<?php $this->widget('application.components.widgets.usertheme.AvantGridView', array(
    'id' => 'save-grid',
    'dataProvider' => new CActiveDataProvider('ApiSave'),
//    'hide_btns'=>true,
    'columns' => array(
//        'id',
        'module',
        'model',
        'action',
        'url',
//        [
//            'name' => 'params',
//            'value' => '"<pre>".$data->params."</pre>"',
//            'type' => 'raw'
//        ],
    ),
)); ?>